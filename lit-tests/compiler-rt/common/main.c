#define CHAR_CONST

#if defined(MAIN_WITH_ARGS)
extern int __real_main(int argc, char **argv);
#elif defined(MAIN_WITH_ARGS_CONST)
extern int __real_main(int argc, const char **argv);
#undef CHAR_CONST
#define CHAR_CONST const
#else
extern int __real_main();
#endif

int __wrap_main(int argc, CHAR_CONST char **argv) {
  int res;
#if defined(MAIN_WITH_ARGS) || defined(MAIN_WITH_ARGS_CONST)
  res = __real_main(argc, argv);
#else
  res = __real_main();
#endif
  return res;
}
