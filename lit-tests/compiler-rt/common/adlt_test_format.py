"""
@file adlt_test_format.py

@brief Describes a custom format for building and running lit tests for ADLT.
"""

import os
import sys
import lit.util
import re
from lit.formats import ShTest
import lit.TestRunner as TR

SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))
ROOT_DIR = f"{SCRIPT_DIR}/../../../"

sys.path.append(f"{ROOT_DIR}/tools")
from tools import shell
from config import Config


def get_output_from_cmd(cmd: str) -> tuple[str, str]:
    """
    Finding output file and defining its type in the input command.
    """
    output = None
    output_type = "executable"

    while cmd:
        arg = cmd.pop(0)
        if arg == "-shared" or arg == "-dynamiclib":
            output_type = "shared"
        elif arg == "-c":
            output_type = "object"
        elif arg == "-o":
            output = cmd.pop(0)

    return output, output_type


class AdltTestFormat(ShTest):
    """
    AdltTestFormat class.

    AdltTestFormat class describes the format for building and running lit tests using ADLT.

    Args:
        unsupportedList (list): List with unsupported tests.
    """

    def __init__(self, unsupportedList: list = []) -> None:
        super().__init__()
        self.unsupportedList = unsupportedList
        self.execute_external = False

    def execute(
        self, test: lit.Test.Test, litConfig: lit.LitConfig.LitConfig
    ) -> lit.Test.Result:
        """
        Execute a test from the suite.
        """
        return self.parse_command(
            test,
            litConfig,
            self.execute_external,
            self.extra_substitutions,
            self.preamble_commands,
        )

    def parse_command(
        self,
        test: lit.Test.Test,
        litConfig: lit.LitConfig.LitConfig,
        useExternalSh: bool,
        extra_substitutions: list = [],
        preamble_commands: list = [],
    ) -> lit.Test.Result:
        """
        Parsing lit commands from source files, variable substitution.

        These steps are taken from the original `ShTest``,
        but this method adds a check for a custom list of unsupported tests. And also call expand_for_adlt function.
        """
        if test.config.unsupported:
            return lit.Test.Result(lit.Test.UNSUPPORTED, "Test is unsupported")

        srcFile = test.getSourcePath()

        for testName in self.unsupportedList:
            if srcFile.endswith(testName):
                return lit.Test.Result(lit.Test.UNSUPPORTED, "Test is unsupported")

        script = list(preamble_commands)
        parsed = TR.parseIntegratedTestScript(test, require_script=not script)

        if isinstance(parsed, lit.Test.Result):
            return parsed
        script += parsed

        if litConfig.noExecute:
            return lit.Test.Result(lit.Test.PASS)

        tmpDir, tmpBase = TR.getTempPaths(test)

        substitutions = list(extra_substitutions)
        substitutions += TR.getDefaultSubstitutions(
            test, tmpDir, tmpBase, normalize_slashes=useExternalSh
        )

        conditions = {feature: True for feature in test.config.available_features}

        script = TR.applySubstitutions(
            script,
            substitutions,
            conditions,
            recursion_limit=test.config.recursiveExpansionLimit,
        )

        script = self.expand_for_adlt(test, script)

        return TR._runShTest(test, litConfig, useExternalSh, script, tmpBase)

    def expand_for_adlt(self, test: lit.Test.Test, script: list) -> list:
        """
        Adding additional commands (builds, linking, etc.) to the original command to run tests using ADLT.
        """
        tmpDir, tmpBase = TR.getTempPaths(test)
        testExt = tmpBase.split(".")[-1]
        srcFile = test.getSourcePath()

        defines = ""

        if testExt == "c":
            defines += " -DC_EXTERN"

        # We need to know which signature of the main function
        # is used in the test to set the correct define for wrapper.
        with open(srcFile) as f:
            data = f.read()
            if "int argc" in data:
                if "const char *argv" in data:
                    defines += " -DMAIN_WITH_ARGS_CONST"
                else:
                    defines += " -DMAIN_WITH_ARGS"

        # Generate an environment for QEMU.
        if not os.path.exists(f"{tmpDir}/lib"):
            shell(
                f"rm -rf {tmpDir}/lib {tmpDir}/lib_orig {tmpDir}/obj {tmpDir}/toolchain"
            )
            shell(f"python3 {ROOT_DIR}/tools/gen_qemu_env.py -od {tmpDir}")

        # To store output files of different commands.
        outputHistory = {}

        # For mapping old input files to new ones.
        inputsMapping = {}

        # To store ADLT input libraries.
        inputLibs = []

        # Updating the script commands.
        for i in range(len(script)):
            # Sometimes, a RUN script may contain multiple commands separated by `&&`,
            # so we need to check each one.
            runCmd = script[i].split("&&")

            for j in range(len(runCmd)):
                # Add a space at the end for correct replacement.
                cmd = f"{runCmd[j]} "

                # Some tests contain brackets in commands, it is advisable to remove them so as not to break the structure.
                for br in ["(", "{", "}", ")"]:
                    cmd = cmd.replace(f" {br} ", " ")

                if not "clang" in cmd:
                    runCmd[j] = cmd
                    continue

                # Commands not related to compilation - let's separate them.
                clangPos = re.search(r"\S+\/clang", cmd)
                leftCmd, cmd = cmd[: clangPos.start()], cmd[clangPos.start() :]

                outputName, outputType = get_output_from_cmd(cmd.split(" "))
                if outputName is None:
                    continue

                outputHistory[outputName] = (outputType, cmd)

                # If the command contains `-c`, we extend it with an additional option.
                if outputType == "object":
                    runCmd[j] = f"{cmd} -fPIC"
                    continue

                # If the command contains `-shared', we extend it with additional options required for ADLT compatibility.
                elif outputType == "shared":
                    runCmd[j] = (
                        f"{cmd} -Wl,--emit-relocs,--no-relax -Wl,-soname,{outputName} "
                    )
                    continue

                # If the command expects a binary file, the processing is more complicated.
                elif outputType == "executable":
                    outCmd = []

                    # Get a list of all input files (*.cpp, *.c, *.o, *.so, *.m) from the command.
                    cmdWithoutOutput = cmd.replace(f" {outputName} ", " ")
                    inputFiles = [
                        s.strip()
                        for s in re.findall(
                            r" \/\S+\.(?:cpp|o|so|c|m)", cmdWithoutOutput
                        )
                    ]

                    # Now we need to process each file separately.
                    for inFile in inputFiles:
                        name = inFile.split("/")[-1]

                        # If it's a library, we'll remember it for ADLT.
                        if name.endswith(".so"):
                            inputLibs.append(inFile)
                            continue

                        # If it's an object file, we need to do some additional linking.
                        elif name.endswith(".o"):
                            # For linking, we will reuse the command that was for compiling to an object file.
                            _, compCmd = outputHistory[inFile]
                            compCmd += " "

                            srcFile = re.findall(r" \S+\.(?:c|cpp|m) ", compCmd)[
                                0
                            ].strip()

                            linkOut = f"{inFile}.so"
                            linkCmd = compCmd.replace(
                                f" -o {inFile} ", f" -o {linkOut} "
                            )
                            linkCmd = re.sub(r"\-x (c\+\+|c)", " ", linkCmd)
                            linkCmd = linkCmd.replace(f" {srcFile} ", f" {inFile} ")
                            linkCmd = f"{linkCmd} -fPIC -Wl,--emit-relocs -shared -Wl,-soname,{linkOut}"
                            linkCmd = linkCmd.replace(" -c ", " ")

                            outCmd.append(linkCmd)
                            inputLibs.append(linkOut)
                            inputsMapping[inFile] = linkOut
                            continue

                        # If it's C/C++ source, we need to compile manually.
                        elif (
                            name.endswith(".cpp")
                            or name.endswith(".c")
                            or name.endswith(".m")
                        ):
                            newCmd = f"{cmd} "
                            # Remove all unnecessary files from command.
                            for f in inputFiles:
                                if f != inFile:
                                    newCmd = newCmd.replace(f" {f} ", " ")

                            # Add compilation step with `-c`.
                            compileOut = f"{tmpDir}/{name}.o"
                            compileCmd = newCmd.replace(
                                f"{inFile} ", f"{inFile} -fPIC -c "
                            )
                            compileCmd = compileCmd.replace(
                                f" {outputName}", f" {compileOut}"
                            )
                            outCmd.append(compileCmd)

                            # Add object file linking step.
                            linkOut = f"{compileOut}.so"
                            linkCmd = re.sub(r"\-x (c\+\+|c)", " ", newCmd)
                            linkCmd = linkCmd.replace(f" {outputName}", f" {linkOut}")
                            linkCmd = linkCmd.replace(
                                f"{inFile} ",
                                f" -fPIC -Wl,--emit-relocs,--no-relax -shared {compileOut} -Wl,-soname,{linkOut} ",
                            )
                            outCmd.append(linkCmd)
                            inputLibs.append(linkOut)
                            inputsMapping[inFile] = linkOut
                        else:
                            raise RuntimeError(
                                f"The file type isn't supported: {inFile}"
                            )

                    # In the output command update the input files.
                    for oldFile, newFile in inputsMapping.items():
                        cmd = cmd.replace(f" {oldFile} ", f" {newFile} ")

                    # Add the lld call with --adlt.
                    adltInputLibs = " ".join(inputLibs)
                    adltOut = f"{outputName}.adlt"
                    adltCmd = (
                        f"ld.lld --adlt {adltInputLibs} -o {adltOut} --emit-relocs"
                    )
                    outCmd.append(adltCmd)

                    # For library we need to call adlt_makelinks.py, to set the links.
                    # It's mandatory to pass environment variables, since llvm-lit clears them.
                    makelinksCmd = f"env SDK_ROOT={Config.SDK_ROOT} LLVM={Config.LLVM} python3 {ROOT_DIR}/tools/adlt_makelinks.py -i {adltInputLibs} -od {tmpDir}/lib -o {adltOut}"
                    outCmd.append(makelinksCmd)

                    # Add wrapper for main.
                    mainOut = f"{outputName}.main.o"
                    mainCmd = cmd.replace("clang++", "clang")
                    mainCmd = mainCmd.replace(f" -o {outputName} ", " ")
                    mainCmd = re.sub(r" \/\S+\.(?:cpp|o|so|c|m)", " ", mainCmd)
                    mainCmd = f"{mainCmd} -x c -std=c99 {defines} -o {mainOut} -c {SCRIPT_DIR}/main.c"
                    outCmd.append(mainCmd)

                    cmd = cmd.replace(
                        " -o ",
                        f" -Wl,--wrap=main --rtlib=compiler-rt -Wl,--emit-relocs {mainOut} -o ",
                    )
                    cmd = re.sub(r"\-x (c\+\+|c)", " ", cmd)
                    outCmd.append(cmd)

                    # Combine all the commands into one.
                    runCmd[j] = " && ".join(outCmd)

                    if len(leftCmd) > 0:
                        runCmd[j] = f"{leftCmd} {runCmd[j]}"

            # If there were multiple commands, then update.
            script[i] = " && ".join(runCmd)

        return script
