#!/usr/bin/python3
"""
@file qemu.py

@brief Wrapper for running tests with qemu-aarch64-static.
"""
import sys
import os
import subprocess

runFile = sys.argv[1]
workingDir = os.path.dirname(runFile)

envArgs = f"LD_LIBRARY_PATH={workingDir}/lib MUSL_LOG_DISABLE_CONNECT=1"

cmd = (
    f"{envArgs} qemu-aarch64-static -L {workingDir} -E -LD_LIBRARY_PATH={workingDir} "
    + " ".join(sys.argv[1:])
)

result = subprocess.run(cmd, shell=True, capture_output=True, text=True, timeout=600)

sys.stdout.write(result.stdout)
sys.stderr.write(result.stderr)
sys.stdout.flush()
sys.stderr.flush()

retcode = result.returncode
sys.exit(result.returncode)
