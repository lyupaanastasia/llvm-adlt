"""
@file lit.site.cfg.py

@brief The script describes the settings for running asan lit based tests with ADLT.
"""

import os
import sys

SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))
ROOT_DIR = f"{SCRIPT_DIR}/../../../"
ASAN_DIR = f"{ROOT_DIR}/out/build/lit-tests/compiler-rt/test/asan/AARCH64OHOSConfig/"

sys.path.append(f"{SCRIPT_DIR}/../common")

from adlt_test_format import AdltTestFormat

unsupported = [
    # qemu: uncaught target signal 5 (Trace/breakpoint trap): main function returns nothing in the test, which causes an error.
    "TestCases/Linux/aligned_delete_test.cpp",
    "TestCases/Linux/asan_default_suppressions.cpp",
    "TestCases/Linux/interception_readdir_r_test.cpp",
    "TestCases/Linux/thread_local_quarantine_size_kb.cpp",
    "TestCases/Posix/unpoison-alternate-stack.cpp"
    "TestCases/asan_and_llvm_coverage_test.cpp",
    "TestCases/handle_noreturn_bug.cpp",
    "TestCases/intercept-rethrow-exception.cpp",
    "TestCases/invalid-pointer-pairs.cpp",
    "TestCases/malloc_fill.cpp",
    "TestCases/realloc.cpp",

    # Compilation error, clang is used instead of clang++.
    # "error: ISO C++ forbids forward references to 'enum' types"
    "TestCases/Linux/ptrace.cpp",

    # The test does not contain C/C++ code, it is empty and designed for compilation only.
    "TestCases/default_ignorelist.cpp",

    # Very long test execution time:
    "TestCases/Linux/stress_dtls.c",

    # Tests not designed to use shared libraries:
    "TestCases/Linux/activation-options.cpp",
    "TestCases/Linux/globals-gc-sections-lld.cpp",
    "TestCases/Posix/coverage.cpp",
    "TestCases/coverage-trace-pc.cpp",
    "TestCases/global-underflow.cpp",
    "TestCases/initialization-bug.cpp",

    # Tests are not designed to use musl libc:
    "TestCases/Linux/init-order-dlopen.cpp",
    "TestCases/Linux/printf-fortify-1.c",
    "TestCases/Linux/printf-fortify-2.c",
    "TestCases/Linux/printf-fortify-3.c",
    "TestCases/Linux/printf-fortify-4.c",

    # No %run variable in lit-steps:
    "TestCases/Linux/dlopen-mixed-c-cxx.c",

    # Tests fail on arm64 host:
    "TestCases/Linux/function-sections-are-bad.cpp",
    "TestCases/Linux/initialization-bug-any-order.cpp",
    "TestCases/Posix/asan-sigbus.cpp",
    "TestCases/Posix/global-registration.c",
    "TestCases/Posix/handle_abort_on_error.cpp",
    "TestCases/Posix/print_cmdline.cpp",
    "TestCases/Posix/shared-lib-test.cpp",
    "TestCases/Posix/start-deactivated.cpp",
    "TestCases/asan_options-help.cpp",
    "TestCases/default_options.cpp",
    "TestCases/printf-2.c",
    "TestCases/sleep_for_debugger.c",
    "TestCases/strcasestr-1.c",
    "TestCases/strcasestr-2.c",
    "TestCases/strstr-1.c",
    "TestCases/strstr-2.c",

    # Problems in runtime (e.g. segfault) when using qemu.
    "TestCases/Linux/clone_test.cpp",
    "TestCases/Linux/interception_malloc_test.cpp",
    "TestCases/Linux/interception_test.cpp",
    "TestCases/Linux/quarantine_size_mb.cpp",
    "TestCases/Linux/segv_read_write.c",
    "TestCases/Posix/closed-fds.cpp",
    "TestCases/Posix/deep_call_stack.cpp",
    "TestCases/Posix/free_hook_realloc.cpp",
    "TestCases/Posix/halt_on_error-torture.cpp",
    "TestCases/Posix/handle_abort_on_error.cpp",
    "TestCases/Posix/no-fd.cpp",
    "TestCases/Posix/strndup_oob_test.cpp",
    "TestCases/Posix/strndup_oob_test2.cpp",
    "TestCases/coverage-disabled.cpp",
    "TestCases/debug_double_free.cpp",
    "TestCases/debug_report.cpp",
    "TestCases/exitcode.cpp",
    "TestCases/interception_failure_test.cpp",
    "TestCases/interface_test.cpp",
    "TestCases/log-path_test.cpp",
    "TestCases/malloc-no-intercept.c",
    "TestCases/null_deref.cpp",
    "TestCases/on_error_callback.cpp",
    "TestCases/replaceable_new_delete.cpp",
    "TestCases/suppressions-exec-relative-location.cpp",
    "TestCases/throw_invoke_test.cpp",
    "TestCases/verbose-log-path_test.cpp",
    "TestCases/global-location-nodebug.cpp",
]

config.available_features.add("ohos_family")
config.host_os = "Linux"

lit_config.load_config(
    config,
    f"{ASAN_DIR}/lit.site.cfg.py",
)

config.test_format = AdltTestFormat(unsupported)
config.test_exec_root = ASAN_DIR
config.lit_test_times = f"{ASAN_DIR}/lit_test_times.txt"
