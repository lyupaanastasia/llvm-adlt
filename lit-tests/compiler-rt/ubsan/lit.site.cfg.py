"""
@file lit.site.cfg.py

@brief The script describes the settings for running ubsan lit based tests with ADLT.
"""

import os
import sys

SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))
ROOT_DIR = f"{SCRIPT_DIR}/../../../"
UBSAN_DIR = f"{ROOT_DIR}/out/build/lit-tests/compiler-rt/test/ubsan/Standalone-aarch64"

sys.path.append(f"{SCRIPT_DIR}/../common")

from adlt_test_format import AdltTestFormat

unsupported = [
    # Requires changes to the source file:
    "TestCases/Integer/suppressions.cpp",

    # Not designed to use shared libraries:
    "TestCases/Misc/Linux/diag-stacktrace.cpp",
    "TestCases/Misc/Linux/ubsan_options.cpp",
    "TestCases/Misc/bounds.cpp",
    "TestCases/Misc/monitor.cpp",
]

config.available_features.add("ohos_family")
config.host_os = "Linux"

lit_config.load_config(
    config,
    f"{UBSAN_DIR}/lit.site.cfg.py",
)

config.test_format = AdltTestFormat(unsupported)
config.test_exec_root = UBSAN_DIR
config.lit_test_times = f"{UBSAN_DIR}/lit_test_times.txt"
