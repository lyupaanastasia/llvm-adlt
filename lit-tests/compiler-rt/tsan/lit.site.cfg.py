"""
@file lit.site.cfg.py

@brief The script describes the settings for running tsan lit based tests with ADLT.
"""

import os
import sys

SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))
ROOT_DIR = f"{SCRIPT_DIR}/../../../"
TSAN_DIR = f"{ROOT_DIR}/out/build/lit-tests/compiler-rt//test/tsan/AARCH64Config"

sys.path.append(f"{SCRIPT_DIR}/../common")

from adlt_test_format import AdltTestFormat

unsupported = [
    # Very long test execution time:
    "Linux/check_preinit.cpp",
    "Linux/fork_deadlock.cpp",
    "Linux/fork_multithreaded4.cpp",
    "Linux/user_fopen.cpp",
    "atexit2.cpp",
    "atexit4.cpp",
    "atexit5.cpp",
    "atomic_free.cpp",
    "atomic_norace2.cpp" "atomic_norace2.cpp",
    "atomic_race.cpp",
    "bench_acquire_only.cpp",
    "bench_local_mutex.cpp",
    "bench_threads.cpp",
    "bench_threads.cpp",
    "compare_exchange.cpp",
    "cond_destruction.cpp",
    "force_background_thread.cpp",
    "fork_atexit.cpp",
    "fork_multithreaded.cpp",
    "fork_multithreaded3.cpp",
    "free_race3.c",
    "signal_block.cpp",
    "signal_thread.cpp",
    "tls_race.cpp",
    "tls_race2.cpp",
    "unaligned_race.cpp",
    "vfork.cpp",

    # Tests not designed to use shared libraries:
    "Linux/user_malloc.cpp",
    "cxa_guard_acquire.cpp",
    "deadlock_detector_stress_test.cpp",
    "debugging.cpp",
    "default_options.cpp",
    "dlopen_ns.cpp",
    "java_symbolization.cpp",
    "java_symbolization_legacy.cpp",
    "large_malloc_meta.cpp",
    "lots_of_threads.c",

    # Problems in runtime (e.g. segfault) when using qemu:
    "Linux/clone_setns.cpp",
    "fiber_cleanup.cpp",
    "mmap_large.cpp",
    "mmap_lots.cpp",
    "mutex_cycle_long.c",
    "signal_handler_longjmp.cpp",
    "signal_reset.cpp",

    # Errors in linking. Need to fix sources to fix:
    "atexit.cpp",
    "global_race.cpp",
    "ignore_dlclose.cpp",
    "ignore_lib2.cpp",
    "libcxx_call_once.cpp",
]

config.available_features.add("ohos_family")
config.host_os = "Linux"

lit_config.load_config(
    config,
    f"{TSAN_DIR}/lit.site.cfg.py",
)

config.test_format = AdltTestFormat(unsupported)
config.test_exec_root = TSAN_DIR
config.lit_test_times = f"{TSAN_DIR}/lit_test_times.txt"
