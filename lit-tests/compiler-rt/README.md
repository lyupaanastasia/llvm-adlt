## Compiler-rt lit tests
---
This directory contains auxiliary scripts for running lit tests from compiler-rt using ADLT.

The main idea:
- CMake configuration occurs in the `llvm-adlt/out/build` directory.
- The lit tests are run using a special config `lit.site.cfg.py`. The custom config overrides the steps of building and running lit tests to be ADLT-compatible.


Contents:
- `common`:
   - `common/main.c` - is a wrapper used to call the original `main` from the test.
   - `common/adlt_test_format.py` - script with overriding steps for running lit tests.
- `cmake`:
   - `cmake/Platform/OHOS.cmake` - file with CMake configuration for building.
- `asan`:
   - `asan/lit.site.cfg.py` - configuration for running `asan` tests.
- `ubsan`:
   - `ubsan/lit.site.cfg.py` - configuration for running `ubsan` tests.
- `tsan`:
   - `tsan/lit.site.cfg.py` - configuration for running `tsan` tests.



To add a new test, you need to:
- Create new folder for test suite and create a new configuration file, e.g. `lit.site.cfg.py` inside.
- Inside the created file you need to override `test_format`. Optionally, you can pass a list of unsupported tests.
- In `llvm-adlt/tools/run_lit_sanitizers.py` update  list of supported tests.
