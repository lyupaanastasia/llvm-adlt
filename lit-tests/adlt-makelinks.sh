#!/bin/sh

# Ro be customized: llvm-readobj which understands adlt
READOBJ=llvm-readobj

in_manual() {
    tgt=$1
    set -f
    set ${MANUAL}
    while [ $# -gt 0 ]
    do
	if [ x"$tgt" = x"$1" ]; then
	    echo $2
	    return 0
	fi
	shift 2
    done
    return 1
}

in_skip() {
    for zz in ${SKIP} ; do
	if [ x"$zz" = x"$1" ]; then
	    return 0
	fi
    done
    return 1
}

if [ -z "$1" ] || [ -z "$2" ]; then
    cat <<EOF 1>& 2
    Usage: $(basename $0) adlt_libName dest_dir
    Purpose: create symlinks to adlt_libname in dest_dir"
    set 'SKIP="link_to_skip1 link_to_skip2" to skip some links
    set 'MANUAL="SRC MANUAL_LINK" to add a link MANUAL_LINK->SRC
    Examples:
       '$(basename $0) libadlt.so lib/alllinks'
       		   install all SONAMES from libadlt to lib/alllinks
       'SKIP=lib_a.so $(basename $0) libadlt.so lib/skip_lib_a'
       		   install all SONAMES except lib_a.so to lib/skip_lib_a
       'MANUAL="lib_a.so alias_a" $(basename $0) libadlt.so lib/manual'
       		   install all SONAMES PLUS 'alias_a -> lib_a.so' to lib/manual

EOF
    exit 1
fi

# Convert libadlt to abspath
TARGET=$(realpath $1)

# CD to dest dir (create if necessary)
mkdir -p $2
cd $2 || ( echo Cannot cd to dest_dir 1>& 2 ; exit 1;  )

# Get list of nDSO names in libadlt
LSLN=$(${READOBJ} --adlt-section ${TARGET} | grep soname\:|sed s/soname\:\ *//)

# make a link for each name absent in SKIP list
for i in $LSLN ; do
    # Skip if current name is in SKIP list
    if in_skip  $i; then continue; fi
    # make a link
    ln -sf ${TARGET} $i
    # If the given name (key) is in MANUAL, add corresponding link
    if [ -z "$MANUAL" ]; then continue; fi
    M=$(in_manual $i)
    if [ $? -eq 0 ]; then
	ln -sf $i ${M}
    fi
done
exit 0
