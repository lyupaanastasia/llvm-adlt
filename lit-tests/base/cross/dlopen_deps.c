// Prepare runtime env
// RUN: mkdir -p %build_rpath %rpath
// RUN: ln -sf %OHOS_SYSROOT/%OHOS_TARGET/usr/lib/libc.so %rpath/ld-musl-aarch64.so.1
// RUN: ln -sf %OHOS_LLVM_INSTALL/lib/%OHOS_TARGET/libc++.so %rpath/libc++.so

// Prepare debug env.
// RUN: cp %S/dso.script %t

// Build shared libraries to %build_rpath
// RUN: %clang -fPIC -shared -Wl,--emit-relocs -Wl,-soname,b.so -o %build_rpath/b.so %S/Inputs/b.c
// RUN: %clang -fPIC -shared -Wl,--emit-relocs -Wl,-soname,a.so -o %build_rpath/a.so %S/Inputs/a.c %build_rpath/b.so
// RUN: %clang -fPIC -shared -Wl,--emit-relocs -Wl,-soname,e.so -o %build_rpath/e.so %S/Inputs/e.c %build_rpath/a.so

// Link and layout libs with adlt, depending on config
// RUN: %adlt %build_rpath/a.so %build_rpath/b.so %build_rpath/e.so -o %build_rpath/libadlt.so
// RUN: %adlt_layout %build_rpath/libadlt.so %rpath

// Build executable
// RUN: %clang -o %t/exe %s

// Check runtime
// RUN: %run %t/exe 2>&1 | FileCheck %s


#include <dlfcn.h>
#include <stdio.h>
#include <stdlib.h>

#define RESOLVE_SYM(handle, symbol)                                            \
  int (*symbol)() = 0;                                                         \
  symbol = dlsym(handle, #symbol);                                             \
  if (symbol == 0) {                                                           \
    fprintf(stderr, "dlsym() failed for %s with %s\n", #symbol, dlerror());    \
    exit(EXIT_FAILURE);                                                        \
  }

int main() {
  const char *libname = "e.so";
  // init
  void *handle = dlopen(libname, RTLD_LAZY);
  if (!handle) {
    fprintf(stderr, "dlopen() failed for %s with %s\n", libname, dlerror());
    exit(EXIT_FAILURE);
  }
  RESOLVE_SYM(handle, f5);
  int res = f5(); // Equals 3
  printf("%d\n", res);
  // CHECK: 3
  dlclose(handle);
}
