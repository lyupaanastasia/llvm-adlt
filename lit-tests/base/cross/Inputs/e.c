#include <stdio.h>

extern int f1(void);
extern const char *msgA;

const char *strE = "e.c";
const char *msgE = "message from lib E!";

void printE(void) { puts("this is lib E!"); }

void printMessageEfromE(void) { printf("%s\n", msgE); }

void printMessageAfromE(void) { printf("%s\n", msgA); }

int f5(void) {
  return f1();
}
