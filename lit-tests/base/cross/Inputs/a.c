#include <stdio.h>

extern int f3(void);
extern const char *msgB;

const char *strA = "a.c";
const char *msgA = "message from lib A!";

void printA(void) { puts("this is lib A!"); }

void printMessageAfromA(void) { printf("%s\n", msgA); }

void printMessageBfromA(void) {
  printf("print msg B from %s: %s\n", strA, msgB);
}

int f1(void) { return f3(); }

int f4(void) { return 4; }
