#include <stdio.h>

extern int f4(void);
extern const char *msgA;

const char *strB = "b.c";
const char *msgB = "message from lib B!";

void printB(void) { puts("this is lib B!"); }

void printMessageBfromB(void) { printf("%s\n", msgB); }

void printMessageAfromB(void) {
  printf("print msg A from %s: %s\n", strB, msgA);
}

int f2(void) { return f4(); }

int f3(void) { return 3; }
