// Prepare runtime env
// RUN: mkdir -p %build_rpath %rpath
// RUN: ln -sf %OHOS_SYSROOT/%OHOS_TARGET/usr/lib/libc.so %rpath/ld-musl-aarch64.so.1
// RUN: ln -sf %OHOS_LLVM_INSTALL/lib/%OHOS_TARGET/libc++.so %rpath/libc++.so

// Prepare debug env.
// RUN: cp %S/dso.script %t

// Build shared libraries to %build_rpath
// RUN: %clang -fPIC -shared -Wl,--emit-relocs -Wl,-soname,a.so -o %build_rpath/a.so %S/Inputs/a.c
// RUN: %clang -fPIC -shared -Wl,--emit-relocs -Wl,-soname,b.so -o %build_rpath/b.so %S/Inputs/b.c
// RUN: %clang -fPIC -shared -Wl,--emit-relocs -Wl,-soname,e.so -o %build_rpath/e.so %S/Inputs/e.c

// Link and layout libs with adlt, depending on config
// RUN: %adlt %build_rpath/a.so %build_rpath/b.so %build_rpath/e.so -o %build_rpath/libadlt.so
// RUN: %adlt_layout %build_rpath/libadlt.so %rpath

// Build executable
// RUN: %clang %s %build_rpath/a.so %build_rpath/b.so %build_rpath/e.so -o %t/exe

// Check runtime
// RUN: %run %t/exe 2>&1 | FileCheck %s

#include <stdio.h>

int f1(void); // from a.so
int f2(void); // from b.so
int f5(void); // from e.so

void printA(void);
void printB(void);
void printE(void);

void printMessageAfromA(void);
void printMessageBfromB(void);
void printMessageEfromE(void);

void printMessageAfromB(void);
void printMessageBfromA(void);
void printMessageAfromE(void);

extern const char *strA;
extern const char *strB;
extern const char *strE;

extern const char *msgA;
extern const char *msgB;
extern const char *msgE;

int main() {
  // Checking cross GOT calls:
  puts(strA);
  puts(strB);
  puts(strE);
  // CHECK: a.c
  // CHECK: b.c
  // CHECK: e.c

  puts(msgA); // message from lib A!
  puts(msgB); // message from lib B!
  puts(msgE); // message from lib E!
  // CHECK: message from lib A!
  // CHECK: message from lib B!
  // CHECK: message from lib E!

  // Checking PLT calls:
  printA();
  printB();
  printE();
  // CHECK: this is lib A!
  // CHECK: this is lib B!
  // CHECK: this is lib E!

  // Checking cross PLT calls:
  printMessageAfromA();
  printMessageBfromB();
  printMessageEfromE();
  // CHECK: message from lib A!
  // CHECK: message from lib B!
  // CHECK: message from lib E!

  printMessageAfromB();
  printMessageBfromA();
  printMessageAfromE();
  // CHECK: message from lib A!
  // CHECK: message from lib B!
  // CHECK: message from lib A!

  // Checking sum of values from cross PLT calls:
  int sum = 0;
  const int countRuns = 1000000;

  // A.so calls B.so
  for (size_t i = 0; i < countRuns; i++)
    sum += f1();
  printf("%d\n", sum);
  // CHECK: 3000000

  // B.so calls A.so
  for (size_t i = 0; i < countRuns; i++)
    sum += f2();
  printf("%d\n", sum);
  // CHECK: 7000000

  // E.so calls A.so which calls B.so
  for (size_t i = 0; i < countRuns; i++)
    sum += f5();
  printf("%d\n", sum);
  // CHECK: 10000000
}
