// Only for AARCH64
// Prepare runtime env
// RUN: mkdir -p %build_rpath %rpath
// RUN: ln -sf %OHOS_SYSROOT/%OHOS_TARGET/usr/lib/libc.so %rpath/ld-musl-aarch64.so.1
// RUN: ln -sf %OHOS_LLVM_INSTALL/lib/%OHOS_TARGET/libc++.so %rpath/libc++.so

// Prepare debug env
// RUN: cp %S/aarch64-lldb.script %t

// Build shared libraries to %build_rpath
// RUN: %clang -fPIC -shared -Wl,--emit-relocs -Wl,-soname,a.so -o %build_rpath/a.so %S/Inputs/a.c
// RUN: %clang -fPIC -shared -Wl,--emit-relocs -Wl,-soname,b.so -o %build_rpath/b.so %S/Inputs/b.c

// Link and layout libs with adlt, depending on config
// RUN: %adlt %build_rpath/a.so %build_rpath/b.so -o %build_rpath/libadlt.so
// RUN: %adlt_layout %build_rpath/libadlt.so %rpath

// Build executable
// RUN: %clang %s %build_rpath/a.so %build_rpath/b.so -o %t/exe

// Start lldb with adlt lib
// RUN: export LD_LIBRARY_PATH=%t/run/lib
// RUN: %lldb -s %t/aarch64-lldb.script %t/exe -b 2>&1 | FileCheck %s

/* Expected runtime output. */
// CHECK: frame #0: 0x{{.*}} {{.?}}.so`testPrintA at a.c:4:3
// CHECK: frame #0: 0x{{.*}} {{.?}}.so`testPrintB at b.c:4:3
// CHECK: Process {{.*}} exited with status = 0 (0x00000000)



extern void testPrintA(void);
extern void testPrintB(void);

int main(int argc, char **argv) {
  testPrintA();
  testPrintB();
  return 0;
}
