// Prepare runtime env
// RUN: mkdir -p %build_rpath %rpath
// RUN: ln -sf %OHOS_SYSROOT/%OHOS_TARGET/usr/lib/libc.so %rpath/ld-musl-aarch64.so.1
// RUN: ln -sf %OHOS_LLVM_INSTALL/lib/%OHOS_TARGET/libc++.so %rpath/libc++.so

// Build shared libraries to %build_rpath
// RUN: %clangxx_cfi -shared %S/Inputs/libbase.cpp -Wl,-soname,libbase.so -o %build_rpath/libbase.so
// RUN: %clangxx_cfi -shared %S/Inputs/libuse.cpp %build_rpath/libbase.so -Wl,-soname,libuse.so -o %build_rpath/libuse.so

// Link and layout libs with adlt, depending on config
// RUN: %adlt %build_rpath/libbase.so %build_rpath/libuse.so -o %build_rpath/libadlt.so
// RUN: %adlt_layout %build_rpath/libadlt.so %rpath

// Build executable
// RUN: %clangxx_cfi %s %build_rpath/libbase.so -o %t/exe

// Check runtime
// RUN: %run %t/exe 2>&1 | FileCheck %s

#include <dlfcn.h>
#include <cstdio>

struct base_t;
void do_f(base_t *ptr);

int main() {
  using create_t = void (*)();
  auto *lib = dlopen("libuse.so", RTLD_NOW);
  auto *fn = (create_t)dlsym(lib, "create");
  fn();
  dlclose(lib);
  using do_f_t = void(*)(base_t *);
  do_f_t ptr = do_f;
  (*ptr)(nullptr);
  fprintf(stderr, "=OK=\n");
}

// CHECK-NOT: control flow integrity check {{.*}} failed
// CHECK: =OK=
// CHECK-NOT: control flow integrity check {{.*}} failed
