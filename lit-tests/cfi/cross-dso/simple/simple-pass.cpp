// Prepare runtime env
// RUN: mkdir -p %build_rpath %rpath
// RUN: ln -sf %OHOS_SYSROOT/%OHOS_TARGET/usr/lib/libc.so %rpath/ld-musl-aarch64.so.1
// RUN: ln -sf %OHOS_LLVM_INSTALL/lib/%OHOS_TARGET/libc++.so %rpath/libc++.so

// Build shared libraries with CFI to %build_rpath
// RUN: %clangxx_cfi -fPIC -shared -DSHARED_LIB1 %s -Wl,-soname,libpass1.so -o %build_rpath/libpass1.so
// RUN: %clangxx_cfi -fPIC -shared -DSHARED_LIB2 %s -Wl,-soname,libpass2.so -o %build_rpath/libpass2.so

// Link and layout libs with adlt, depending on config
// RUN: %adlt %build_rpath/libpass1.so %build_rpath/libpass2.so -o %build_rpath/libadlt.so
// RUN: %adlt_layout %build_rpath/libadlt.so %rpath

// Build executable with CFI
// RUN: %clangxx_cfi %s %build_rpath/libpass1.so %build_rpath/libpass2.so -o %t/exe

// Check runtime
// RUN: %run %t/exe 2>&1 | FileCheck %s

// Prepare runtime env
// RUN: rm -rf %t
// RUN: mkdir -p %build_rpath %rpath
// RUN: ln -sf %OHOS_SYSROOT/%OHOS_TARGET/usr/lib/libc.so %rpath/ld-musl-aarch64.so.1
// RUN: ln -sf %OHOS_LLVM_INSTALL/lib/%OHOS_TARGET/libc++.so %rpath/libc++.so

// Build shared libraries without CFI to %build_rpath
// RUN: %clangxx -fPIC -shared -DSHARED_LIB1 %s -Wl,-soname,libpass1.so -o %build_rpath/libpass1.so
// RUN: %clangxx -fPIC -shared -DSHARED_LIB2 %s -Wl,-soname,libpass2.so -o %build_rpath/libpass2.so

// Link and layout libs with adlt, depending on config
// RUN: %adlt %build_rpath/libpass1.so %build_rpath/libpass2.so -o %build_rpath/libadlt.so
// RUN: %adlt_layout %build_rpath/libadlt.so %rpath

// Build executable without CFI
// RUN: %clangxx %s %build_rpath/libpass1.so %build_rpath/libpass2.so -o %t/exe

// Check runtime
// RUN: %run %t/exe 2>&1 | FileCheck %s

#include <stdio.h>

struct PA {
  virtual void f();
};

PA *create_PB();
PA *create_PC();

int icall1();
int icall2();

#ifdef SHARED_LIB1

#include "utils.h"
struct PB : public PA {
  virtual void f();
};
void PB::f() {}

PA *create_PB() {
  create_derivers<PB>();
  return new PB();
}

int icall1() {
  return 1;
}

#elif defined SHARED_LIB2

#include "utils.h"
struct PC : public PA {
  virtual void f();
};
void PC::f() {}

PA *create_PC() {
  create_derivers<PC>();
  return new PC();
}

int icall2() {
  return 0;
}
#else

void PA::f() {}

int main() {
  // CHECK: cross-dso simple pass example vptr
  fprintf(stderr, "cross-dso simple pass example vptr\n");

  PA *b = create_PB();
  PA *c = create_PC();

  // CHECK: =1=
  fprintf(stderr, "=1=\n");
  // CHECK-NOT: control flow integrity check
  b->f(); // OK

  // CHECK: =2=
  fprintf(stderr, "=2=\n");
  // CHECK-NOT: control flow integrity check
  c->f(); // OK

  // CHECK: =3=
  fprintf(stderr, "=3=\n");

  // CHECK: cross-dso simple pass example icall
  fprintf(stderr, "cross-dso simple pass example icall\n");

  using func_t = int (*)();

  func_t arr[] = {icall1, icall2};
  int idx = 0;

  // CHECK: =1=
  fprintf(stderr, "=1=\n");
  // CHECK-NOT: control flow integrity check
  idx = arr[idx](); // OK, call icall1

  // CHECK: =2=
  fprintf(stderr, "=2=\n");
  // CHECK-NOT: control flow integrity check
  idx = arr[idx](); // OK, call icall2

  // CHECK: =3=
  fprintf(stderr, "=3=\n");
  // CHECK-NOT: control flow integrity check
  idx = arr[idx](); // OK, call icall1

  // CHECK: =4=
  fprintf(stderr, "=4=\n");
  // CHECK-NOT: control flow integrity check
}

#endif
