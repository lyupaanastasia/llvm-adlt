// Prepare runtime env
// RUN: mkdir -p %build_rpath %rpath
// RUN: ln -sf %OHOS_SYSROOT/%OHOS_TARGET/usr/lib/libc.so %rpath/ld-musl-aarch64.so.1
// RUN: ln -sf %OHOS_LLVM_INSTALL/lib/%OHOS_TARGET/libc++.so %rpath/libc++.so

// Build shared libraries with CFI to %build_rpath
// RUN: %clangxx_cfi -fPIC -shared -DSHARED_LIB1 %s -Wl,-soname,libfail1.so -o %build_rpath/libfail1.so
// RUN: %clangxx_cfi -fPIC -shared -DSHARED_LIB2 %s -Wl,-soname,libfail2.so -o %build_rpath/libfail2.so

// Link and layout libs with adlt, depending on config
// RUN: %adlt %build_rpath/libfail1.so %build_rpath/libfail2.so -o %build_rpath/libadlt.so
// RUN: %adlt_layout %build_rpath/libadlt.so %rpath

// Build executable with CFI
// RUN: %clangxx_cfi %s %build_rpath/libfail1.so %build_rpath/libfail2.so -o %t/exe

// Check runtime
// RUN: %run %t/exe 2>&1 | FileCheck --check-prefixes=CHECK,CFI %s

// Prepare runtime env
// RUN: rm -rf %t
// RUN: mkdir -p %build_rpath %rpath
// RUN: ln -sf %OHOS_SYSROOT/%OHOS_TARGET/usr/lib/libc.so %rpath/ld-musl-aarch64.so.1
// RUN: ln -sf %OHOS_LLVM_INSTALL/lib/%OHOS_TARGET/libc++.so %rpath/libc++.so

// Build shared libraries without CFI to %build_rpath
// RUN: %clangxx -fPIC -shared -DSHARED_LIB1 %s -Wl,-soname,libfail1.so -o %build_rpath/libfail1.so
// RUN: %clangxx -fPIC -shared -DSHARED_LIB2 %s -Wl,-soname,libfail2.so -o %build_rpath/libfail2.so

// Link and layout libs with adlt, depending on config
// RUN: %adlt %build_rpath/libfail1.so %build_rpath/libfail2.so -o %build_rpath/libadlt.so
// RUN: %adlt_layout %build_rpath/libadlt.so %rpath

// Build executable without CFI
// RUN: %clangxx %s %build_rpath/libfail1.so %build_rpath/libfail2.so -o %t/exe

// Check runtime
// RUN: %run %t/exe 2>&1 | FileCheck --check-prefixes=CHECK,NCFI %s

#include <stdio.h>

struct FA {
  virtual void f();
};

void *create_FB();
void *create_FC();
int icall1();
int icall2();

#ifdef SHARED_LIB1

#include "utils.h"
struct FB {
  virtual void f();
};
void FB::f() {}

void *create_FB() {
  create_derivers<FB>();
  return (void *)(new FB());
}

int icall1() {
  return 1;
}

#elif defined SHARED_LIB2

#include "utils.h"
struct FC {
  virtual void f();
};
void FC::f() {}

void *create_FC() {
  create_derivers<FC>();
  return (void *)(new FC());
}

int icall2() {
  return 0;
}

#else

void FA::f() {}

int main(int argc, char *argv[]) {
  fprintf(stderr, "cross-dso simple fail example\n");

  FA *a;
  void *pb = create_FB(), *pc = create_FC();

  fprintf(stderr, "=0=\n");

  // Test cast. BOOM.
  // CFI: runtime error: control flow integrity check for type 'FA' failed during cast to unrelated type
  // CFI: note: vtable is of type '{{(struct )?}}FB'
  // NCFI-NOT: control flow integrity check
  a = (FA*)pb;

  // CHECK: =1=
  fprintf(stderr, "=1=\n");

  // CFI: runtime error: control flow integrity check for type 'FA' failed during virtual call
  // CFI: note: vtable is of type '{{(struct )?}}FB'
  // NCFI-NOT: control flow integrity check
  a->f(); // UB here

  // CHECK: =2=
  fprintf(stderr, "=2=\n");

  // Test cast. BOOM.
  // CFI: runtime error: control flow integrity check for type 'FA' failed during cast to unrelated type
  // CFI: note: vtable is of type '{{(struct )?}}FC'
  // NCFI-NOT: control flow integrity check
  a = (FA*)pc;

  // CHECK: =3=
  fprintf(stderr, "=3=\n");

  // CFI: runtime error: control flow integrity check for type 'FA' failed during virtual call
  // CFI: note: vtable is of type '{{(struct )?}}FC'
  // NCFI-NOT: control flow integrity check
  a->f(); // UB here

  // CHECK: =4=
  fprintf(stderr, "=4=\n");

  using func_t = int (*)(int);
  func_t arr[] = {(func_t)icall1, (func_t)icall2};
  int idx = 0;

  // CFI: runtime error: control flow integrity check for type 'int (int)' failed during indirect function call
  // NCFI-NOT: control flow integrity check
  idx = arr[idx](idx);
  // CFI: runtime error: control flow integrity check for type 'int (int)' failed during indirect function call
  // NCFI-NOT: control flow integrity check
  idx = arr[idx](idx);
  // CFI: runtime error: control flow integrity check for type 'int (int)' failed during indirect function call
  // NCFI-NOT: control flow integrity check
  idx = arr[idx](idx);

  // CHECK: =4=
  fprintf(stderr, "=4=\n");
  // CHECK-NOT: control flow integrity check
}

#endif
