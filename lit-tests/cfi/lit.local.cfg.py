parent = config

cfi_flags = "-fsanitize=cfi -fsanitize-cfi-cross-dso -flto -fvisibility=default -fno-sanitize-trap=cfi -fsanitize-recover=cfi "
config.target_cflags = parent.target_cflags + cfi_flags
config.target_cxxflags = parent.target_cxxflags + cfi_flags
config.substitutions.append(("%clang_cfi ", f"{parent.clang} {config.target_cflags} "))
config.substitutions.append(("%clangxx_cfi ", f"{parent.clang}++ {config.target_cxxflags} "))
