/*
Prepare runtime env.
*/
// RUN: rm -rf %t && mkdir -p %build_rpath %rpath
// RUN: ln -sf %OHOS_SYSROOT/%OHOS_TARGET/usr/lib/libc.so %rpath/ld-musl-aarch64.so.1
// RUN: ln -sf %OHOS_LLVM_INSTALL/lib/%OHOS_TARGET/libc++.so %rpath/libc++.so

/*
Important env for GWP-ASAN.
*/
// RUN: mkdir -p %root_rpath/dev/__parameters__ && cp %S/Inputs/param_storage %root_rpath/dev/__parameters__

/*
Prepare debug env.
*/
// RUN: cp %S/double-free.script %t

/*
Create shared lib and link it to an app.
Check runtime.
*/
// RUN: %clang -shared -Wl,-soname,buggy.so -o %build_rpath/buggy.so %S/Inputs/buggy.c
// RUN: %adlt %build_rpath/buggy.so -o %build_rpath/libadlt.so
// RUN: %adlt_layout %build_rpath/libadlt.so %rpath
// RUN: %clang -o %t/a.out %build_rpath/buggy.so %s
// RUN: not %run %t/a.out 2>&1 | FileCheck %s

/*
Expected runtime output.
*/
// CHECK: *** GWP-ASan detected a memory error ***
// CHECK: Double Free at 0x[[#%x,]] (a 4-byte allocation) by thread {{.*}} here:
// CHECK:  #0 0x[[#%x,]]
// CHECK: {{.*}} was deallocated by thread {{.*}} here
// CHECK:  #0 0x[[#%x,]]
// CHECK: {{.*}} was allocated by thread {{.*}} here
// CHECK:  #0 0x[[#%x,]]

#include <stdbool.h>

__attribute__((weak)) bool may_init_gwp_asan(bool force_init);

extern void doubleFree();

int main() {
  /*
  These params are unavailable in qemu-aarch64-static:
  system("param set gwp_asan.sample.all true");
  system("param set gwp_asan.log.path default");
  */
  may_init_gwp_asan(true);

  doubleFree();
}
