#include <stdio.h>
#include <stdlib.h>

void doubleFree() {
  int* ptr = malloc(sizeof(int)) ;
  free(ptr);
  free(ptr);
  *ptr = 123;
  printf("change %d\n", *ptr);
}
