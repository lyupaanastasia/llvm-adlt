# -*- Python -*-

import os
import platform
import re
import subprocess
import locale
from pathlib import Path

import lit.formats
import lit.util

from lit.llvm import llvm_config

SDK_ROOT = Path(os.environ["SDK_ROOT"])
config.llvm_install = Path(os.environ["LLVM"])  # path to llvm-install

adlt_root = Path(os.path.dirname(__file__)) / ".."
ohos_target = "aarch64-linux-ohos"
ohos_llvm_install = config.llvm_install
ohos_sysroot_path = ohos_llvm_install / ".." / "sysroot"
ohos_clangrt = SDK_ROOT / "out" / "lib" / f"clangrt-{ohos_target}" / "lib" / ohos_target

config.name = config.name_suffix
config.test_format = lit.formats.ShTest()
config.excludes = ["Inputs", "Output", ".txt", ".script"]
config.suffixes = [".c", ".cpp", ".asm", ".test"]
config.test_source_root = os.path.dirname(__file__)

config.target_arch = "aarch64"
config.clang = ohos_llvm_install / "bin" / "clang"
config.lld = ohos_llvm_install / "bin" / "ld.lld"
config.lldb = ohos_llvm_install / "bin" / "lldb"

config.target_cflags = f"--sysroot={ohos_sysroot_path} --target={ohos_target} -g -O0 "
config.target_cflags += "-Wl,--emit-relocs,--no-relax " if config.use_adlt else ""
config.target_cxxflags = config.target_cflags + "-std=c++17 "
qemu = SDK_ROOT / "qemu-aarch64-static"

config.environment = os.environ
config.environment["MUSL_LOG_DISABLE_CONNECT"] = "1"

config.substitutions.append(("%SDK_ROOT", SDK_ROOT))
config.substitutions.append(("%OHOS_SYSROOT", ohos_sysroot_path))
config.substitutions.append(("%OHOS_LLVM_INSTALL", ohos_llvm_install))
config.substitutions.append(("%OHOS_TARGET", ohos_target))
config.substitutions.append(("%OHOS_CRT", ohos_clangrt))
config.substitutions.append(("%QEMU", qemu))

config.substitutions.append(("%clang ", f"{config.clang} {config.target_cflags} "))
config.substitutions.append(("%clangxx ", f"{config.clang}++ {config.target_cxxflags} "))
config.substitutions.append(("%lldb ", f"{config.lldb} "))
config.substitutions.append(("%adlt ", f"{config.lld} --adlt " if config.use_adlt else ": "))
config.substitutions.append(("%adlt_layout ", f"{config.test_source_root}/adlt-makelinks.sh " if config.use_adlt else ": "))
config.substitutions.append(("%run ", f"{qemu} -L %root_rpath "))
config.substitutions.append(("%build_rpath", "%rpath.adlt" if config.use_adlt else "%rpath"))
config.substitutions.append(("%rpath", "%root_rpath/lib"))
config.substitutions.append(("%root_rpath", "%t/run"))

if config.use_adlt:
    config.available_features.add('adlt')
