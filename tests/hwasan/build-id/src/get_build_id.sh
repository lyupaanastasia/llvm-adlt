#!/bin/bash

LIBRARY_FILE1="$1"
LIBRARY_FILE2="$2"
OUTPUT_FILE="build_id.txt"

rm -rf "$OUTPUT_FILE"

SDK_PARTLY="${SDK_PARTLY}"

# Check if SDK_PARTLY is set
if [ -n "$SDK_PARTLY" ]; then
    LLVM="$SDK_PARTLY"
    FUSE_LD="-fuse-ld=${LLVM}/bin/ld.lld"
else
    LLVM="${LLVM}"
fi

# Set LLVM_BIN variable
LLVM_BIN="${LLVM}/bin"

# Execute the readelf command and filter out warnings, then extract the Build ID
BUILD_ID1=$($LLVM_BIN/llvm-readelf -n "$LIBRARY_FILE1" 2>&1 | grep -a "Build ID:" | awk '{print $3}')
BUILD_ID2=$($LLVM_BIN/llvm-readelf -n "$LIBRARY_FILE2" 2>&1 | grep -a "Build ID:" | awk '{print $3}')

# Check if a Build ID was found
if [ -z "$BUILD_ID1" ]; then
    echo "No Build ID found in $LIBRARY_FILE1." > "$OUTPUT_FILE"
elif [ -z "$BUILD_ID2" ]; then
    echo "No Build ID found in $LIBRARY_FILE2." > "$OUTPUT_FILE"
else
    printf "%s\n%s" "$BUILD_ID1" "$BUILD_ID2" > "$OUTPUT_FILE"
fi
