#include "gmock/gmock.h"
#include "gtest/gtest.h"
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <pwd.h>

extern "C" {
int f_a(void);
int f_b(void);
};

// in our case BuildID is 16 characters
char buildID_liba[20];
char buildID_libb[20];
size_t readResult = 0;
const char *tmpDirectory = "/data/local/tmp/";

int readBuildIDs() {
  FILE *file;

  file = fopen("build_id.txt", "r");
  if (file == NULL) {
    perror("Error opening file");
    return EXIT_FAILURE;
  }

  if (fgets(buildID_liba, sizeof(buildID_liba), file) == NULL) {
    printf("Error reading build=id for liba or end of file reached\n");
    return EXIT_FAILURE;
  }

  if (fgets(buildID_libb, sizeof(buildID_libb), file) == NULL) {
    printf("Error reading build=id for libb or end of file reached\n");
    return EXIT_FAILURE;
  }

  fclose(file);

  buildID_liba[strcspn(buildID_liba, "\n")] = 0;
  buildID_libb[strcspn(buildID_libb, "\n")] = 0;

  return 0;
}

int checkTmpDir() {

  struct stat statbuf;
  if (stat(tmpDirectory, &statbuf) == 0) {
    // Check if it's a directory
    if (S_ISDIR(statbuf.st_mode)) {
      return 0;
    } else {
      // path exists but is not a directory
      return EXIT_FAILURE;
    }
  }
  // Directory does not exist or an error occurred
  return EXIT_FAILURE;
}

void createTmpDir() {
  if (checkTmpDir()) {
    // Path tmpDirectory does not exists

    char cmd[256];
    // Make a directory
    snprintf(cmd, sizeof(cmd), "sudo mkdir -p %s", tmpDirectory);

    int res = system(cmd);
    if (res == 0) {
      printf("Directory %s created successfully\n", tmpDirectory);
    } else {
      printf("Failed to create directory %s. Please check your permissions\n",
             tmpDirectory);
    }

    // Get the current user's username
    uid_t uid = getuid();
    struct passwd *pw = getpwuid(uid);
    if (pw == NULL) {
      printf("Failed to get user name\n");
      return;
    }

    // Change the ownership of the directory
    snprintf(cmd, sizeof(cmd), "sudo chown %s:%s %s", pw->pw_name, pw->pw_name,
             tmpDirectory);

    res = system(cmd);
    if (res == 0) {
      printf("Ownership of %s changed to user %s successfully\n", tmpDirectory,
             pw->pw_name);
    } else {
      printf(
          "Failed to change ownership of %s. Please check your permissions\n",
          tmpDirectory);
    }
  }
}

TEST(buildIdTest, testBuildIDsAvailability) {
  // We should be able to get the build-ids obtained
  // from the libraries built during the test configuration in cmake
  ASSERT_EQ(0, readResult);
}

// There's a problem with the temporary directory, see as well the comment
// https://rnd-gitlab-msc.huawei.com/rus-os-team/compilers/tasks/-/issues/2469#note_3319537)
TEST(buildIdTest, testTmpDir) {
  // The /data/local/tmp/ folder that exists on Android
  // but does not exist on Linux should be pre-created
  // as a temporary solution to run this test on Linux
  ASSERT_EQ(0, checkTmpDir());
}

// It's recommended by gtest to use
//  testing::GTEST_FLAG(death_test_style) = "threadsafe";
// but in the case with running in qemu this does not work.
// Gtest with “threadsafe” starts the child process not in Qemu,
// but in the host system and “execve: Exec format error” occurs

TEST(buildIdTest, testLibA_BuildID) {
  std::string buildID_liba_matcher = std::string("(.*BuildId: ") +
                                     std::string(buildID_liba) +
                                     std::string("*)");
  // We expect to exit with code 99 (standard completion code for HWAsan).
  EXPECT_EXIT(f_a(), testing::ExitedWithCode(99), buildID_liba_matcher.c_str());
}

// - orig test will be PASSED
// - adlt test will be FAILED for the B library for now, because HWAsan in the
// message only specifies the first build ID that matches the LibA build ID
TEST(buildIdTest, testLibB_BuildID) {
  std::string buildID_libb_matcher = std::string("(.*BuildId: ") +
                                     std::string(buildID_libb) +
                                     std::string("*)");
  // We expect to exit with code 99 (standard completion code for HWAsan).
  EXPECT_EXIT(f_b(), testing::ExitedWithCode(99), buildID_libb_matcher.c_str());
}

__attribute__((no_sanitize("hwaddress"))) int main(int argc, char **argv) {
  testing::InitGoogleMock(&argc, argv);

  createTmpDir();

  readResult = readBuildIDs();

  return RUN_ALL_TESTS();
}
