#include <pthread.h>

int globalA = 0;

void *a1(void *p) {
  globalA++;
  return NULL;
}

void *a2(void *p) {
  globalA--;
  return NULL;
}

void testA() {
  pthread_t t[2];
  pthread_create(&t[0], NULL, a1, NULL);
  pthread_create(&t[1], NULL, a2, NULL);
  pthread_join(t[0], NULL);
  pthread_join(t[1], NULL);
}
