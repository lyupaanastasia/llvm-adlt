#include <pthread.h>

int globalB = 0;
pthread_mutex_t lock;

void *b1(void *p) {
  pthread_mutex_lock(&lock);
  globalB++;
  pthread_mutex_unlock(&lock);
  return NULL;
}

void *b2(void *p) {
  pthread_mutex_lock(&lock);
  globalB--;
  pthread_mutex_unlock(&lock);
  return NULL;
}

void testB() {
  pthread_t t[2];
  pthread_mutex_init(&lock, NULL);
  pthread_create(&t[0], NULL, b1, NULL);
  pthread_create(&t[1], NULL, b2, NULL);
  pthread_join(t[0], NULL);
  pthread_join(t[1], NULL);
  pthread_mutex_destroy(&lock);
}
