#include "gtest/gtest.h"
#include <fstream>
#include <string>

extern void testA();
extern void testB();

TEST(tsan, DataRaceTwoLibs) {
  testA();
  testB();

  std::string filename = "tsan_report.log." + std::to_string(getpid());

  ASSERT_TRUE(!filename.empty()) << "TSan report not found!";

  std::ifstream reportFile(filename);
  ASSERT_TRUE(reportFile) << "Unable to open file: " << filename;

  // Read all data from the file.
  std::stringstream strStream;
  strStream << reportFile.rdbuf();
  std::string reportData = strStream.str();

  std::string libA = "libtsanDataRaceTwoLibs_a.so";
  std::string libB = "libtsanDataRaceTwoLibs_b.so";
  std::string errorMsg = "SUMMARY: ThreadSanitizer: data race";

  ASSERT_TRUE(!reportData.empty())
      << "Report file: " << filename << " is empty!";

  if (reportData.find(libA) == std::string::npos)
    FAIL() << "Failed to match a string in the report file: " << libA;

  if (reportData.find(errorMsg) == std::string::npos)
    FAIL() << "Failed to match a string in the report file: " << errorMsg;

  // We expect that there will be no mention of the 'libtsanDataRaceTwoLibs_b'
  // library in the report.
  if (reportData.find(libB) != std::string::npos)
    FAIL() << "Found a string we didn't expect to find: " << libB;

  SUCCEED();
}

GTEST_API_ int main(int argc, char **argv) {
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
