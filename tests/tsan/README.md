`qemu-aarch64-static` has a problem with running tests with the option: `-fsanitize=thread`. Launching any program causes an error like:
```
execve failed, errno 8.
```

To be able to run a test with thread sanitizer, it is necessary to use a script:
https://github.com/qemu/qemu/blob/master/scripts/qemu-binfmt-conf.sh

It is necessary to execute the command and specify the path to qemu, for example:
```shell
sudo qemu-binfmt-conf.sh --qemu-path $HOME/work/sdk/
```

You should then be able to run the binary without errors.
