#include "gtest/gtest.h"
#include <fstream>
#include <string>

int global;

extern int testDataRace();

TEST(tsan, DataRace) {
  int result = testDataRace();
  (void)result;

  std::string filename = "tsan_report.log." + std::to_string(getpid());

  ASSERT_TRUE(!filename.empty()) << "TSan report not found!";

  std::ifstream reportFile(filename);
  ASSERT_TRUE(reportFile) << "Unable to open file: " << filename;

  // Read all data from the file.
  std::stringstream strStream;
  strStream << reportFile.rdbuf();
  std::string reportData = strStream.str();

  ASSERT_TRUE(!reportData.empty())
      << "Report file: " << filename << " is empty!";

  std::string firstCase = "SUMMARY: ThreadSanitizer: data race";
  if (reportData.find(firstCase) == std::string::npos)
    FAIL() << "Failed to match a string in the report file: " << firstCase;

  SUCCEED();
}

GTEST_API_ int main(int argc, char **argv) {
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
