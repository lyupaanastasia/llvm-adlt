#include <pthread.h>

extern int global;

void *testThread(void *x) {
  global = 42;
  return x;
}

int testDataRace() {
  pthread_t t;
  pthread_create(&t, NULL, testThread, NULL);
  global = 43;
  pthread_join(t, NULL);
  return global;
}
