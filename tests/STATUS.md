## TODO

- RTTI - it might work already, because of working CFI tests...
- Add tests for .adlt section: check whether section exists in adlt-enabled builds and if it can be successfully parsed

### Base

| Feature | Desc | Status | Test name
| - | - | - | -
| GOT, PLT | Cross linking, PLT optimization | OK | base/cross
| HEAP | Checks memory alloc inside adlt | OK | base/heap
| Backtrace | Checks API from MUSL: dlopen, dlsym | OK | base/backtrace, base/backtrace2, base/dlopen-c, base/dlopen-cpp
| Relocs | Read here: [base/relocs/STATUS.md](base/relocs/STATUS.md) | TODO | base/relocs
| VA_ARGS | Check variadic arguments function support | OK | base/varargs
| OOP | Ctors,dtors,vtable (see notes below) | NOK | base/cdtors, base/oop

Notes:

- Currently base/cdtors test has some internal test cases failing due to musl limitations. More info in [base/cdtors/README.md](base/cdtors/README.md)

### CFI

Checks `__cfi_slowpath`, `__cfi_slowpath_diag`.
| Feature | Desc | Status | Test name
| - | - | - | -
| Call,Cast | Check control flow integrity | OK | cfi/call-cast
| Cross-DSO | Check cast, vtable | NOK | cfi/cross-dso/simple
| Cross-DSO | Uses API from MUSL: dlopen, dlsym | OK | cfi/cross-dso/ldso

### TLS

Notes:

- Checks `pthread`, `std::thread` API
- Same source code for all these tests, but different TLS model.
  - Except for the `local-exec` model - impossible to test from shared libraries.

| Feature | Desc | Status | Test name
| - | - | - | -
| emutls | Checks `__emutls_v`, `__emutls_t` support | OK | tls/emu
| Global-Dynamic | Checks TLS`GD` relocs-group | OK | tls/global-dynamic
| Local-Dynamic | Checks TLS`LD` relocs-group | OK | tls/local-dynamic
| Initial-Exec | Checks TLS`IE` relocs-group | OK | tls/initial-exec

### ld-musl

| Feature | Desc | Status | Test name
| - | - | - | -
| dlopen,dlsym | Checks API from MUSL | OK | ld-musl/dlopen
| so_mem_share | Read here: [ld-musl/so_mem_share/Readme.md](ld-musl/so_mem_share/Readme.md) | NOK | ld-musl/so_mem_share
