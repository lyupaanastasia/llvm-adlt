initproject(PROJECT_NAME ubsanIntegerOverflow EXECUTABLES main LIBS a)

add_library(${a} SHARED src/a.cpp)
target_compile_options(${a} PRIVATE "-fsanitize=undefined")
target_compile_options(${a} PRIVATE "-Wno-integer-overflow")
target_compile_options(${a} PRIVATE "-Wno-unused-variable")
target_link_options(${a} PRIVATE "-fsanitize=undefined")

add_custom_target(
  ${ADLT_PROJECT_LIB} ALL
  DEPENDS ${a}
  COMMAND python3 ${toolsDir}/build_adlt_lib.py -od ${CMAKE_CURRENT_BINARY_DIR}
          -i ${ADLT_LIB_CONTENTS} -o "${ADLT_PROJECT_LIB}")

add_adlt_test(TARGET ${main} SOURCES src/main.cpp LIBS ${a})
target_link_options(
  ${main}
  PRIVATE
  "-Wl,${LLVM}/lib/clang/15.0.4/lib/aarch64-linux-ohos/libclang_rt.ubsan_standalone.a"
)
