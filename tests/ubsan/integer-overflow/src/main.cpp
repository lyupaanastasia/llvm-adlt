#include "gtest/gtest.h"
#include <fstream>
#include <string>

extern void testIntegerOverflow();

TEST(ubsan, IntegerOverflow) {
  testIntegerOverflow();

  std::string filename = "ubsan_report.log." + std::to_string(getpid());

  ASSERT_TRUE(!filename.empty()) << "UBSan report not found!";

  std::ifstream reportFile(filename);
  ASSERT_TRUE(reportFile) << "Unable to open file: " << filename;

  // Read all data from the file.
  std::stringstream strStream;
  strStream << reportFile.rdbuf();
  std::string reportData = strStream.str();

  ASSERT_TRUE(!reportData.empty())
      << "Report file: " << filename << " is empty!";

  std::string firstCase =
      "runtime error: signed integer overflow: 2147483647 + 1";
  if (reportData.find(firstCase) == std::string::npos)
    FAIL() << "Failed to match a string in the report file: " << firstCase;

  std::string secondCase =
      "runtime error: signed integer overflow: -2147483648 - 1";
  if (reportData.find(secondCase) == std::string::npos)
    FAIL() << "Failed to match a string in the report file: " << secondCase;

  std::string libA = "/lib/libubsanIntegerOverflow_a.so";
  if (reportData.find(libA) == std::string::npos)
    FAIL() << "Failed to match a string in the report file: " << libA;

  SUCCEED();
}

GTEST_API_ int main(int argc, char **argv) {
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
