initproject(PROJECT_NAME ubsanIndexOutOfBounds EXECUTABLES main LIBS a b)

add_library(${a} SHARED src/a.cpp)
target_compile_options(${a} PRIVATE "-fsanitize=undefined")
target_compile_options(${a} PRIVATE "-O0")
target_link_options(${a} PRIVATE "-fsanitize=undefined")

add_library(${b} SHARED src/b.cpp)
target_compile_options(${b} PRIVATE "-fsanitize=undefined")
target_compile_options(${b} PRIVATE "-O0")
target_link_options(${b} PRIVATE "-fsanitize=undefined")

add_custom_target(
  ${ADLT_PROJECT_LIB} ALL
  DEPENDS ${a} ${b}
  COMMAND python3 ${toolsDir}/build_adlt_lib.py -od ${CMAKE_CURRENT_BINARY_DIR}
          -i ${ADLT_LIB_CONTENTS} -o "${ADLT_PROJECT_LIB}")

add_adlt_test(TARGET ${main} SOURCES src/main.cpp LIBS ${a} ${b})
target_link_options(
  ${main} PRIVATE
  "-Wl,${LLVM}/lib/clang/15.0.4/lib/aarch64-linux-ohos/libclang_rt.ubsan_standalone.a")
