#include "gtest/gtest.h"
#include <fstream>
#include <string>

extern int testA();
extern int testB();

TEST(ubsan, IndexOutOfBounds) {
  int result = testA();
  result = testB();
  (void)result;

  std::string filename = "ubsan_report.log." + std::to_string(getpid());

  ASSERT_TRUE(!filename.empty()) << "UBsan report not found!";

  std::ifstream reportFile(filename);
  ASSERT_TRUE(reportFile) << "Unable to open file: " << filename;

  // Read all data from the file.
  std::stringstream strStream;
  strStream << reportFile.rdbuf();

  std::string libA = "/lib/libubsanIndexOutOfBounds_a.so";
  std::string libB = "/lib/libubsanIndexOutOfBounds_b.so";
  std::string errorMsg =
      "runtime error: index 5 out of bounds for type 'int[4]'";

  bool libAErrorFound = false;
  bool libBErrorFound = false;

  // Let's look through the whole log and find the lines we need.
  bool skipLine = false;
  std::string line;
  while (std::getline(strStream, line)) {
    if (line.find("memory map") != std::string::npos)
      skipLine = !skipLine;
    if (skipLine)
      continue;

    // If we found an error related to 'b' library - mark it.
    // We expect that no error will be found.
    if (line.find(libB) != std::string::npos)
      libBErrorFound = true;

    // If we found the expected error message, we need to check the next
    // line after that error, which will reference the library.
    if (line.find(errorMsg) != std::string::npos) {
      std::string libLine;
      if (std::getline(strStream, libLine))
        if (libLine.find(libA) != std::string::npos)
          libAErrorFound = true;
    }
  }

  if (libAErrorFound && !libBErrorFound)
    SUCCEED();
  else
    FAIL() << "Error!\nFound libA error: " << libAErrorFound
           << "\nFound libB error: " << libBErrorFound;
}

GTEST_API_ int main(int argc, char **argv) {
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
