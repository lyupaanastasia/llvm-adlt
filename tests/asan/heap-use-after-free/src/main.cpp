#include "gtest/gtest.h"
#include <fstream>
#include <string>

extern int testHeapUseAfterFree();

TEST(asan, HeapUseAfterFree) {
  int result = testHeapUseAfterFree();
  (void)result;

  std::string filename = "asan_report.log." + std::to_string(getpid());

  ASSERT_TRUE(!filename.empty()) << "ASan report not found!";

  std::ifstream reportFile(filename);
  ASSERT_TRUE(reportFile) << "Unable to open file: " << filename;

  // Read all data from the file.
  std::stringstream strStream;
  strStream << reportFile.rdbuf();
  std::string reportData = strStream.str();

  ASSERT_TRUE(!reportData.empty())
      << "Report file: " << filename << " is empty!";

  std::string firstCase = "ERROR: AddressSanitizer: heap-use-after-free";
  if (reportData.find(firstCase) == std::string::npos)
    FAIL() << "Failed to match a string in the report file: " << firstCase;

  SUCCEED();
}

GTEST_API_ int main(int argc, char **argv) {
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
