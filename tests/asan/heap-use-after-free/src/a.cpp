int testHeapUseAfterFree() {
  unsigned size = 2;

  int *arr = new int[size];
  delete[] arr;

  return arr[0];
}
