initproject(PROJECT_NAME asanDoubleFree EXECUTABLES main LIBS a b)

add_library(${a} SHARED src/a.cpp)
target_compile_options(${a} PRIVATE "-fsanitize=address")
target_compile_options(${a} PRIVATE "-fsanitize-recover=address")
target_compile_options(${a} PRIVATE "-O0")
target_link_options(${a} PRIVATE "-fsanitize=address")

add_library(${b} SHARED src/b.cpp)
target_compile_options(${b} PRIVATE "-fsanitize=address")
target_compile_options(${b} PRIVATE "-fsanitize-recover=address")
target_compile_options(${b} PRIVATE "-O0")
target_link_options(${b} PRIVATE "-fsanitize=address")

add_custom_target(
  ${ADLT_PROJECT_LIB} ALL
  DEPENDS ${a} ${b}
  COMMAND python3 ${toolsDir}/build_adlt_lib.py -od ${CMAKE_CURRENT_BINARY_DIR}
          -i ${ADLT_LIB_CONTENTS} -o "${ADLT_PROJECT_LIB}")

add_adlt_test(TARGET ${main} SOURCES src/main.cpp LIBS ${a} ${b})
target_link_options(
  ${main} PRIVATE
  "-Wl,${LLVM}/lib/clang/15.0.4/lib/aarch64-linux-ohos/libclang_rt.asan.a")
