
#include "gtest/gtest.h"
#include <fstream>
#include <string>

extern void testDoubleFreeA();
extern void testFreeB();

TEST(asan, DoubleFree) {
  testDoubleFreeA();
  testFreeB();

  std::string filename = "asan_report.log." + std::to_string(getpid());

  ASSERT_TRUE(!filename.empty()) << "ASan report not found!";

  std::ifstream reportFile(filename);
  ASSERT_TRUE(reportFile) << "Unable to open file: " << filename;

  // Read all data from the file.
  std::stringstream strStream;
  strStream << reportFile.rdbuf();

  std::string libA = "/lib/libasanDoubleFree_a.so";
  std::string libB = "/lib/libasanDoubleFree_b.so";
  std::string errorMsg = "ERROR: AddressSanitizer: attempting double-free";

  bool libAErrorFound = false;
  bool libBErrorFound = false;

  // Let's look through the whole log and find the lines we need.
  bool skipLine = false;
  std::string line;
  while (std::getline(strStream, line)) {
    if (line.find("memory map") != std::string::npos)
      skipLine = !skipLine;
    if (skipLine)
      continue;

    // If we found an error related to 'b' library - mark it.
    // We expect that no error will be found.
    if (line.find(libB) != std::string::npos)
      libBErrorFound = true;

    // If we found the expected error message, we need to check the second
    // line after that error, which will reference the library.
    if (line.find(errorMsg) != std::string::npos) {
      std::string libLine;
      if (std::getline(strStream, line) && std::getline(strStream, libLine))
        if (libLine.find(libA) != std::string::npos)
          libAErrorFound = true;
    }
  }

  if (libAErrorFound && !libBErrorFound)
    SUCCEED();
  else
    FAIL() << "Error!\nFound libA error: " << libAErrorFound
           << "\nFound libB error: " << libBErrorFound;
}

GTEST_API_ int main(int argc, char **argv) {
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
