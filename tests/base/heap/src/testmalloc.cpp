#include <malloc.h>
#include <stdio.h>
#include <stdlib.h>

#include "testmalloc.h"

namespace testmalloc {
int allocateInt(void) {
  printf("Trying to allocate %d\n", (int)sizeof(int));

  int *mem = (int *)malloc(sizeof(int));

  if (mem == NULL) {
    printf("Could not allocate memory\n");
    return 1;
  } else {
    printf("Memory allocated\n");
    free(mem);
    printf("Memory freed\n");
    return 0;
  }
}

int allocateChar(void) {
  printf("Trying to allocate %d\n", (int)sizeof(char));

  char *mem = (char *)malloc(sizeof(char));

  if (mem == NULL) {
    printf("Could not allocate memory\n");
    return 1;
  } else {
    printf("Memory allocated\n");
    free(mem);
    printf("Memory freed\n");
    return 0;
  }
}

int allocateDouble(void) {
  printf("Trying to allocate %d\n", (int)sizeof(double));

  double *mem = (double *)malloc(sizeof(double));

  if (mem == NULL) {
    printf("Could not allocate memory\n");
    return 1;
  } else {
    printf("Memory allocated\n");
    free(mem);
    printf("Memory freed\n");
    return 0;
  }
}

} // namespace testmalloc
