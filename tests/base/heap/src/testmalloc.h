#pragma once

namespace testmalloc {
int allocateInt(void);
int allocateChar(void);
int allocateDouble(void);
} // namespace testmalloc
