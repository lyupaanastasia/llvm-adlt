#include "gmock/gmock.h"
#include "gtest/gtest.h"
#include <stdlib.h>
#include <stdio.h>

#include "testmalloc.h"

TEST(MallocTests, TestAllocateInt) {
  ASSERT_EQ(0, testmalloc::allocateInt());
}

TEST(MallocTests, TestAllocateChar) {
  ASSERT_EQ(0, testmalloc::allocateChar());
}

TEST(MallocTests, TestAllocateDouble) {
  ASSERT_EQ(0, testmalloc::allocateDouble());
}

int main(int argc, char **argv) {
  testing::InitGoogleMock(&argc, argv);
  return RUN_ALL_TESTS();
}
