#pragma once
#ifndef _LIB1_H_
#define _LIB1__H_

#ifndef __cplusplus
#error must use C++ compiler
#endif

void f1() noexcept(false);

#endif
