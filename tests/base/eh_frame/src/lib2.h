#pragma once
#ifndef _LIB2_H_
#define _LIB2__H_

#ifndef __cplusplus
#error must use C++ compiler
#endif

void f2() noexcept(false);

#endif
