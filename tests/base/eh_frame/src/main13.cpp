#include "gmock/gmock.h"
#include "gtest/gtest.h"
#include <stdlib.h>
#include <stdio.h>

#include "lib1.h"
#include "lib3.h"

TEST(test13, test13) {
  try {
    f1();
  } catch (...) {
    dprintf(2, "Exception from lib1 is caught\n");
  }

  try {
    f3();
  } catch (...) {
    dprintf(2, "Exception from lib3 is caught\n");
  }
  SUCCEED();
}

int main(int argc, char **argv) {
  testing::InitGoogleMock(&argc, argv);
  return RUN_ALL_TESTS();
}
