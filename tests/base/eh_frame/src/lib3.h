#pragma once
#ifndef _LIB3_H_
#define _LIB3__H_

#ifndef __cplusplus
#error must use C++ compiler
#endif

void f3() noexcept(false);

#endif
