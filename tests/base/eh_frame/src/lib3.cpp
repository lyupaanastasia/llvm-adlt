#include <stdio.h>
#include "lib3.h"

void f3() noexcept(false) {
  dprintf(2, "f3() before sending exception\n");
  throw "exception from f3()";
  dprintf(2, "f3() after sending exception\n");
}
