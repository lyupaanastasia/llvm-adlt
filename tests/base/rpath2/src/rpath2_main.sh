#!/bin/bash

# ARGS: qemu_with_args, rpt_opts, preconds, bin, test_opts
LINK_TARGETS="libadlt_other_fs.so libother_fs_test.so"

NO_RPATH=rpath2_main_no_rpath
WITH_RPATH=rpath2_main

## Setup
# Move project libraries to lib/0 (lib/ is accessible by default)
# Make a link libb->liba
(mkdir -p lib/0 && rm -f lib/librpath2_a.so && \
     cd lib/0 && mv ../librpath2_b.so . && ln -sf librpath2_b.so librpath2_a.so)

## _norpath cases:

# _norpath case without LD_LIBRARY_PATH should not work at all
# Gtest cannot handle that properly so currently it is commented out
# Planned to run after porting to LIT
# eval $1 $2 $3 $NO_RPATH $5

# lib/0 has a link liba->libb.
# So next should work via link (print messageB)
eval LD_LIBRARY_PATH=lib/0 $1 $2 $3 $NO_RPATH $5

# That should work directly with lib_a (print messageA)
eval LD_LIBRARY_PATH=lib_a $1 $2 $3 $NO_RPATH $5

## _rpath cases (rpath set to lib_a)

# Should print messageA (via rpath)
eval $1 $2 $3 $WITH_RPATH $5

# LD_LIBRARY_PATH has precedence but also points to lib_a
# Also expecting messageA
eval LD_LIBRARY_PATH=lib_a $1 $2 $3 $WITH_RPATH $5

# LD_LIBRARY_PATH has precedence, will use link liba->libb
# Expecting messageB
eval LD_LIBRARY_PATH=lib/0 $1 $2 $3 $WITH_RPATH $5

# Tear down
( cd lib && mv 0/librpath2_b.so . && rm -rf 0 )

exit 0
