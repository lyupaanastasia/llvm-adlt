#include "gmock/gmock.h"
#include "gtest/gtest.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>


extern "C" void testPrintMessage(void);

std::string output;
std::string expectedA = "Hello from lib A!\n";
std::string expectedB = "Hello from lib B!\n";
std::string expected;

TEST(TestRpath2, test1)
{
  testing::internal::CaptureStdout();
  testPrintMessage();
  output = testing::internal::GetCapturedStdout();
  ASSERT_EQ(output, expected);
}

int main(int argc, char **argv) {
  char *p = getenv("LD_LIBRARY_PATH");
  if (!p) {
    expected = expectedA;
  }
  else if (! strcmp(p, "lib_a")) {
    expected = expectedA;
  }
  else if (!strcmp(p, "lib/0")) {
    expected = expectedB;
  }
  else {
    std::cerr << "UNMATCHED CASE" <<std::endl;
  }
  testing::InitGoogleTest();
  (void) RUN_ALL_TESTS();
  return 0;
}
