# Extended rpath test

The test checks correctness of rpath functioning when the adlt library has duplicate symbols
and also when the rpath contradicts to LD_LIBRARY_PATH

## The layout:

- Test uses two libraries with the same function `printTestMessage()`.
The `liba` and `libb` print `messageA` and `messageB` respectively.
- The `liba` and `libb` installed to `lib/0/`, The link `liba->libb` is created in `lib/0/`
- The `liba` is also installed into `lib_a/`

## _norpath tests:

1. (Commented out). The norpath test without `LD_LIBRARY_PATH`. Should not run at all since ldd cannot find libraries.
Commented out because gtest cannot handle launch failure.
2. `LD_LIBRARY_PATH=lib/0`. Expecting messageB since link liba->libb is in effect.
3. `LD_LIBRARY_PATH=lib_a`. Expecting messageA.

## _rpath tests (rpath set to lib_a for all)

1. No `LD_LIBRARY_PATH` set, should use rpath and print messageA.
2. `LD_LIBRARY_PATH=lib_a`. LD_LIBRARY_PATH has precedence but is the same as rpath. Also expecting messageA
3. `LD_LIBRARY_PATH=lib/0`. LD_LIBRARY_PATH has precedence and set to `lib/0/` where we have a link liba->libb.
Expecting messageB
