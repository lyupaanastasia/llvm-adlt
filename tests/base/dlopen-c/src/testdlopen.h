#pragma once

namespace testdlopen {
std::string testVer(const char *filename);
int testPrintf(const char *filename, const char *text);
int testMax(const char *filename, int a, int b);
} // namespace testdlopen