#include "gmock/gmock.h"
#include "gtest/gtest.h"
#include <stdlib.h>
#include <cstdio>

#include "testdlopen.h"

TEST(DlopenCTests, TestGetVer) {
  ASSERT_EQ("1.0", testdlopen::testVer(PROJECT_LIB_PREFIX"dlopenc-lib.so"));
}

TEST(DlopenCTests, TestPrintf) {
  ASSERT_EQ(0, testdlopen::testPrintf(PROJECT_LIB_PREFIX"dlopenc-lib.so", "some text\n"));
}

TEST(DlopenCTests, TestMax) {
  ASSERT_EQ(2, testdlopen::testMax(PROJECT_LIB_PREFIX"dlopenc-lib.so", 1, 2));
}

int main(int argc, char **argv) {
  testing::InitGoogleMock(&argc, argv);
  return RUN_ALL_TESTS();
}

