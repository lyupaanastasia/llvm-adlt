#include <stdio.h>

static const char *ver = "1.0.b";

const char* clibGetVerB(void) {
  return ver;
}

void clibPrintfB(const char *text) {
  printf("%s", text);
}

int clibMinB(int a, int b) {
  return a < b ? a : b;
}
