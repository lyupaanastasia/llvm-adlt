#pragma once
#include <string>

namespace testbt {
std::string testVer(
  const char *symbolNameA, const char *fileNameA, std::string &verA,
  const char *symbolNameB, const char *fileNameB, std::string &verB);
std::string testVerBT(
  const char *symbolNameA, const char *fileNameA, std::string &btA,
  const char *symbolNameB, const char *fileNameB, std::string &btB);
std::string testMaxMin(
  const char *symbolNameA, const char *fileNameA, int aa, int ab, int &valA,
  const char *symbolNameB, const char *fileNameB, int ba, int bb, int &valB);
std::string testMaxMinBT(
  const char *symbolNameA, const char *fileNameA, std::string &btA,
  const char *symbolNameB, const char *fileNameB, std::string &btB);
} // namespace testbt
