#include <cstdio>
#include <dlfcn.h>
#include <execinfo.h>

#include "bt-test.h"

#define STRING_MAX_LEN  256

namespace testbt {
typedef const char* (*clibGetVer_t)();
typedef const int (*clibMaxMin_t)(int, int);

// close library
void closeLib(void *handle) {
  printf("\tClosing library\n");
  dlclose(handle);
}

// open library
void *openLib(const char *fileName) {
  printf("\tLoading library %s\n", fileName);
  void *handle = dlopen(fileName, RTLD_LAZY);
  if (!handle) {
    printf("Error loading library!\n");
    dlerror();
  }

  return handle;
}

std::string getSymbol(void *saddr) {
  Dl_info info = { 0 };
  dladdr(saddr, &info);
  char *str_buffer = NULL;
	str_buffer = (char *)malloc(STRING_MAX_LEN * sizeof(char));
  if (info.dli_sname && info.dli_saddr && info.dli_fbase && info.dli_fname) {
    snprintf(str_buffer, STRING_MAX_LEN, "<%s+%#lx>[%#lx] -> %s", info.dli_sname, 
      (uintptr_t)saddr - (uintptr_t)info.dli_saddr,
      (uintptr_t)saddr - (uintptr_t)info.dli_fbase, info.dli_fname);
    std::string ret(str_buffer);
    free(str_buffer);
    return ret;
  }

  return "Error: could not resolve symbol data\n";
}

// test if functions without parameters are loading and working properly
std::string testVer(
  const char *symbolNameA, const char *fileNameA, std::string &verA,
  const char *symbolNameB, const char *fileNameB, std::string &verB) {
  printf("Testing symbol lookup\n");
  void *handleA = openLib(fileNameA);
  if (!handleA) {
    printf("Exiting!\n");
    return "test failure\n";
  }
  void *handleB = openLib(fileNameB);
  if (!handleB) {
    printf("Exiting!\n");
    return "test failure\n";
  }

  printf("\tLookup symbols %s, %s\n", symbolNameA, symbolNameB);
  clibGetVer_t getA = (clibGetVer_t)dlsym(handleA, symbolNameA);
  if (!getA) {
    printf("Error lookup symbol %s!\n", symbolNameA);
    printf("Exiting!\n");
    return "test failure\n";
  }
  clibGetVer_t getB = (clibGetVer_t)dlsym(handleB, symbolNameB);
  if (!getB) {
    printf("Error lookup symbol %s!\n", symbolNameB);
    printf("Exiting!\n");
    return "test failure\n";
  }

  printf("\tCalling %s, %s\n", symbolNameA, symbolNameB);
  verA = getA();
  verB = getB();

  closeLib(handleA);
  closeLib(handleB);

  printf("Exiting symbol lookup test\n");
  return "test not failed";
}

// test if functions without parameters symbol can be backteaced properly after
// loading
std::string testVerBT(
  const char *symbolNameA, const char *fileNameA, std::string &btA,
  const char *symbolNameB, const char *fileNameB, std::string &btB) {
  printf("Testing symbol backtrace\n");
  void *handleA = openLib(fileNameA);
  if (!handleA) {
    printf("Exiting!\n");
    return "test failure\n";
  }
  void *handleB = openLib(fileNameB);
  if (!handleB) {
    printf("Exiting!\n");
    return "test failure\n";
  }

  printf("\tLookup symbols %s, %s\n", symbolNameA, symbolNameB);
  clibGetVer_t getA = (clibGetVer_t)dlsym(handleA, symbolNameA);
  if (!getA) {
    printf("Error lookup symbol %s!\n", symbolNameA);
    printf("Exiting!\n");
    return "test failure\n";
  }
  clibGetVer_t getB = (clibGetVer_t)dlsym(handleB, symbolNameB);
  if (!getB) {
    printf("Error lookup symbol %s!\n", symbolNameB);
    printf("Exiting!\n");
    return "test failure\n";
  }

  btA = getSymbol((void *)getA);
  btB = getSymbol((void *)getB);
  printf("\tFrom dladdr for %s: %s\n", symbolNameA, btA.c_str());
  printf("\tFrom dladdr for %s: %s\n", symbolNameB, btB.c_str());

  closeLib(handleA);
  closeLib(handleB);

  printf("Exiting symbol backtrace test\n");
  return "test not failed";
}

// test if functions with 2 parameters are loading and working properly
std::string testMaxMin(
  const char *symbolNameA, const char *fileNameA, int aa, int ab, int &valA,
  const char *symbolNameB, const char *fileNameB, int ba, int bb, int &valB) {
  printf("Testing symbol lookup\n");
  void *handleA = openLib(fileNameA);
  if (!handleA) {
    printf("Exiting!\n");
    return "test failure\n";
  }
  void *handleB = openLib(fileNameB);
  if (!handleB) {
    printf("Exiting!\n");
    return "test failure\n";
  }

  printf("\tLookup symbols %s, %s\n", symbolNameA, symbolNameB);
  clibMaxMin_t getA = (clibMaxMin_t)dlsym(handleA, symbolNameA);
  if (!getA) {
    printf("Error lookup symbol %s!\n", symbolNameA);
    printf("Exiting!\n");
    return "test failure\n";
  }
  clibMaxMin_t getB = (clibMaxMin_t)dlsym(handleB, symbolNameB);
  if (!getB) {
    printf("Error lookup symbol %s!\n", symbolNameB);
    printf("Exiting!\n");
    return "test failure\n";
  }

  printf("\tCalling %s, %s\n", symbolNameA, symbolNameB);
  valA = getA(aa, ab);
  valB = getB(ba, bb);

  closeLib(handleA);
  closeLib(handleB);

  printf("Exiting symbol lookup test\n");
  return "test not failed";
}

// test if functions with 2 parameters symbol can be backteaced properly after
// loading
std::string testMaxMinBT(
  const char *symbolNameA, const char *fileNameA, std::string &btA,
  const char *symbolNameB, const char *fileNameB, std::string &btB) {
  printf("Testing symbol backtrace\n");
  void *handleA = openLib(fileNameA);
  if (!handleA) {
    printf("Exiting!\n");
    return "test failure\n";
  }
  void *handleB = openLib(fileNameB);
  if (!handleB) {
    printf("Exiting!\n");
    return "test failure\n";
  }

  printf("\tLookup symbols %s, %s\n", symbolNameA, symbolNameB);
  clibMaxMin_t getA = (clibMaxMin_t)dlsym(handleA, symbolNameA);
  if (!getA) {
    printf("Error lookup symbol %s!\n", symbolNameA);
    printf("Exiting!\n");
    return "test failure\n";
  }
  clibMaxMin_t getB = (clibMaxMin_t)dlsym(handleB, symbolNameB);
  if (!getB) {
    printf("Error lookup symbol %s!\n", symbolNameB);
    printf("Exiting!\n");
    return "test failure\n";
  }

  btA = getSymbol((void *)getA);
  btB = getSymbol((void *)getB);
  printf("\tFrom dladdr for %s: %s\n", symbolNameA, btA.c_str());
  printf("\tFrom dladdr for %s: %s\n", symbolNameB, btB.c_str());

  closeLib(handleA);
  closeLib(handleB);

  printf("Exiting symbol backtrace test\n");
  return "test not failed";
}
} // namespace testbt
