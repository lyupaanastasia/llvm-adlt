#include "gmock/gmock.h"
#include "gtest/gtest.h"
#include <stdlib.h>
#include <stdio.h>

#include "bt-test.h"

TEST(Backtrace2Tests, TestGetVer) {
  std::string verA("");
  std::string verB("");
  std::string resVer = testbt::testVer(
    "clibGetVerA", PROJECT_LIB_PREFIX"backtrace-lib_a.so", verA,
    "clibGetVerB", PROJECT_LIB_PREFIX"backtrace-lib_b.so", verB);

  ASSERT_EQ("1.0.a", verA);
  ASSERT_EQ("1.0.b", verB);

  std::string btA("");
  std::string btB("");
  std::string resBT = testbt::testVerBT(
    "clibGetVerA", PROJECT_LIB_PREFIX"backtrace-lib_a.so", btA,
    "clibGetVerB", PROJECT_LIB_PREFIX"backtrace-lib_b.so", btB);

  bool okA = (btA.find("clibGetVerA") != std::string::npos) &&
    (btA.find("backtrace-lib_a.so") != std::string::npos); 
  bool okB = (btB.find("clibGetVerB") != std::string::npos) &&
    (btB.find("backtrace-lib_b.so") != std::string::npos); 
  
  ASSERT_EQ(true, okA && okB);
}

TEST(Backtrace2Tests, TestMaxMin) {
  int max = 0;
  int min = 0;
  std::string resMaxMin = testbt::testMaxMin(
    "clibMaxA", PROJECT_LIB_PREFIX"backtrace-lib_a.so", 1, 55, max,
    "clibMinB", PROJECT_LIB_PREFIX"backtrace-lib_b.so", 2, 11, min);

  ASSERT_EQ(55, max);
  ASSERT_EQ(2, min);
  
  std::string btA("");
  std::string btB("");
  std::string resBT = testbt::testVerBT(
    "clibMaxA", PROJECT_LIB_PREFIX"backtrace-lib_a.so", btA,
    "clibMinB", PROJECT_LIB_PREFIX"backtrace-lib_b.so", btB);

  bool okA = (btA.find("clibMaxA") != std::string::npos) &&
    (btA.find("backtrace-lib_a.so") != std::string::npos); 
  bool okB = (btB.find("clibMinB") != std::string::npos) &&
    (btB.find("backtrace-lib_b.so") != std::string::npos); 
  
  ASSERT_EQ(true, okA && okB);
}

int main(int argc, char **argv) {
  testing::InitGoogleMock(&argc, argv);
  return RUN_ALL_TESTS();
}
