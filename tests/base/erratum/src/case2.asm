	.text


	// Principle of fix is that when we detected error in sequence of commands.
	// One instruction replace by branch with fixed instructions.
	// At end of branch, we return back.

	// Example of fixed instructions:
	// 0000000000003ff8 <_Z13erratum_case2v>:
	// 	3ff8: d0000008     	adrp	x8, 0x5000 <_Z13erratum_case2v+0x8>
	// 		0000000000003ff8:  R_AARCH64_ADR_GOT_PAGE	var2
	// 	3ffc: f9400021     	ldr	x1, [x1]
	// 	4000: 14000009     	b	0x4024 <__CortexA53843419_4000>
	// 		0000000000004000:  R_AARCH64_LD64_GOT_LO12_NC	var2
	// 	4004: f9400120     	ldr	x0, [x9]
	// 	4008: d65f03c0     	ret

	// 0000000000004024 <__CortexA53843419_4000>:
	// 	4024: f9414509     	ldr	x9, [x8, #648]
	// 	4028: 17fffff7     	b	0x4004 <_Z13erratum_case2v+0xc>


	.balign 4096
	.space  4096 - 8
	.globl	_Z13erratum_case2v
	.p2align	2
	.type	_Z13erratum_case2v,@function
_Z13erratum_case2v:
	// First: ADRP instuction, which writes to a register Rn (for example x8).
	// This instruction must be located in memory at an address where the bottom 12 bits are equal to 0xFF8 or 0xFFC.
	adrp x8, :got:var2
	// Second: load or store instruction
	ldr x1, [x1, #0]
	// Third: Optionally one instruction
	// In test Skipped third instruction.
	// Four: load or store instruction, using Rn (x8) as the base address register.
	// This command might access an incorrect address
	ldr x9, [x8, :got_lo12:var2]
	ldr	x0, [x9]
	ret
.Lfunc_end0:
	.size	_Z13erratum_case2v, .Lfunc_end0-_Z13erratum_case2v


	.type  var2,@object
	.data
	.globl var2
	.p2align	2
var2:
	.word 2
	.size	var2, 4


	.section	".note.GNU-stack","",@progbits
	.addrsig
	.addrsig_sym var2
