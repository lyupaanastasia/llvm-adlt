#include "gmock/gmock.h"
#include "gtest/gtest.h"

int erratum_case1(void);
int erratum_case2(void);


TEST(ErratumTests, Case1) { ASSERT_EQ(1, erratum_case1()); }
TEST(ErratumTests, Case2) { ASSERT_EQ(2, erratum_case2()); }

int main(int argc, char **argv) {
  testing::InitGoogleMock(&argc, argv);
  return RUN_ALL_TESTS();
}
