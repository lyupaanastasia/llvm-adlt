	.text


	// Principle of fix is that when we detected error in sequence of commands.
	// One instruction replace by branch with fixed instructions.
	// At end of branch, we return back.

	// Example of fixed instructions:
	// 0000000000003ff8 <_Z13erratum_case1v>:
	// 	3ff8: d0000008     	adrp	x8, 0x5000 <_Z13erratum_case1v+0x8>
	// 		0000000000003ff8:  R_AARCH64_ADR_GOT_PAGE	var1
	// 	3ffc: f9414509     	ldr	x9, [x8, #648]
	// 		0000000000003ffc:  R_AARCH64_LD64_GOT_LO12_NC	var1
	// 	4000: b000000a     	adrp	x10, 0x5000 <_Z13erratum_case1v+0xc>
	// 		0000000000004000:  R_AARCH64_ADR_GOT_PAGE	var1
	// 	4004: 14000009     	b	0x4028 <__CortexA53843419_4004>
	// 		0000000000004004:  R_AARCH64_LD64_GOT_LO12_NC	var1
	// 	4008: f9400120     	ldr	x0, [x9]
	// 	400c: d65f03c0     	ret

	// 0000000000004028 <__CortexA53843419_4004>:
	//	4028: f9414509     	ldr	x9, [x8, #648]
	// 	402c: 17fffff7     	b	0x4008 <_Z13erratum_case1v+0x10>


	.balign 4096
	.space  4096 - 8
	.globl	_Z13erratum_case1v
	.p2align	2
	.type	_Z13erratum_case1v,@function
_Z13erratum_case1v:
	// First: ADRP instuction, which writes to a register Rn (for example x8).
	adrp   x8, :got:var1
	// Second: load or store instruction
	ldr    x9, [x8, :got_lo12:var1]
	// Third: Another instuction
	adrp   x10, :got:var1
	// Four: load or store instruction, using Rn (x8) as the base address register.
	// This command might access an incorrect address
	ldr    x9, [x8, :got_lo12:var1]
	ldr	x0, [x9]
	ret
.Lfunc_end0:
	.size	_Z13erratum_case1v, .Lfunc_end0-_Z13erratum_case1v


	.type  var1,@object
	.data
	.globl var1
	.p2align	2
var1:
	.word 1
	.size	var1, 4


	.section	".note.GNU-stack","",@progbits
	.addrsig
	.addrsig_sym var1
