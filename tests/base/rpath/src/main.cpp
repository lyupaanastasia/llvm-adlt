#include "gmock/gmock.h"
#include "gtest/gtest.h"
#include <stdio.h>
#include <dlfcn.h>
#include <stdlib.h>

const char* def_libname="librpathTests_a.so";
const char *libname;
int testno;

class DynLib
{
public:
	DynLib(): lib(NULL), testPrintMessagefromA(NULL)
	{
    lib = dlopen(libname, RTLD_LAZY);
    printf("try to dlopen lib %s\n", libname);
    testPrintMessagefromA = 0;
    if (lib) {
      *(void**)(&testPrintMessagefromA) = dlsym(lib, "testPrintMessagefromA");
    }
	}
	~DynLib()
	{
		if (0 != lib) {
      dlclose(lib);
    }
	}
	void *lib;
  void (*testPrintMessagefromA)();
};

class TestRpath : public ::testing::Test
{
protected:
	void SetUp()
	{
		dynLib = new DynLib();
	}
	void TearDown()
	{
		delete dynLib;
	}
	DynLib *dynLib;
};

TEST_F(TestRpath, test1)
{
	switch (testno) {
    case 0:
      // without -rpath, without LD_LIBRARY_PATH
      printf("without -rpath, without LD_LIBRARY_PATH - lib is unaccessible\n");
	    EXPECT_EQ(dynLib->lib, NULL);
      break;
    case 1:
      // without -rpath, with LD_LIBRARY_PATH
      printf("without -rpath, with LD_LIBRARY_PATH - check that lib is loaded,\n");
	    ASSERT_NE(dynLib->lib, NULL);
      printf("load and call testPrintMessagefromA():\n");
      ASSERT_NE(dynLib->testPrintMessagefromA, NULL);
      dynLib->testPrintMessagefromA();
	    break;
    case 2:
      // with -rpath, without LD_LIBRARY_PATH
      printf("with -rpath, without LD_LIBRARY_PATH - check that lib is loaded,\n");
	    ASSERT_NE(dynLib->lib, NULL);
      printf("load and call testPrintMessagefromA():\n");
      ASSERT_NE(dynLib->testPrintMessagefromA, NULL);
      dynLib->testPrintMessagefromA();
	    break;
    default:
      fprintf(stderr,"%s %d", "unknown test case", testno);
      exit(1);
    }
}

int main(int argc, char **argv) {  

  testno = argc > 1 ? atoi(argv[1]) : 0 ;
  libname = argc > 2 ? argv[2] : def_libname;

  printf("Hello! This test is for the -rpath key\n"); 
  
  testing::InitGoogleMock(&argc, argv);
  return RUN_ALL_TESTS();

  return 0;
}
