#define STRINGIZE1(value) #value
#define STRINGIZE(value) STRINGIZE1(value)
#define LIB_STRING STRINGIZE(LIB_SUFFIX)

/***
 * Constructor/destructor test classes
 ***/

class Class {
public:
  Class(int);
  ~Class();
  int Payload(const char *);

private:
  int intVar = 0;
};

/***
 * Vtable test classes
 ***/

class GlobalPoly {
public:
  GlobalPoly(int);
  virtual int VirtualMethod();
  static GlobalPoly *New(int value);
  static GlobalPoly *NewDerived(int value);

  virtual ~GlobalPoly();

protected:
  int intVar = 0;
};

class GlobalPolyDerived : public GlobalPoly {
public:
  GlobalPolyDerived(int);
  int VirtualMethod() override;
};

class InlinePoly {
public:
  InlinePoly(int value) : intVar(value) {}
  virtual int VirtualMethod() {
    puts("InlinePoly" LIB_STRING "::VirtualMethod");
    return intVar;
  }
  static InlinePoly *New(int value);
  static InlinePoly *NewDerived(int value);

  virtual ~InlinePoly() = default;

protected:
  int intVar = 0;
};

class InlinePolyDerived : public InlinePoly {
public:
  InlinePolyDerived(int value) : InlinePoly(value) {}
  int VirtualMethod() override {
    puts("InlinePolyDerived" LIB_STRING "::VirtualMethod");
    return intVar + 20;
  }
};

class LocalPoly {
public:
  LocalPoly(int value);
  virtual int VirtualMethod();
  static LocalPoly *New(int value);
  static LocalPoly *NewDerived(int value);

  virtual ~LocalPoly();

protected:
  int intVar = 0;
};

/***
 * Virtual inheritance test classes
 ***/

class DiamondRoot {
public:
  DiamondRoot(int data);

  virtual int VirtualMethod();
  virtual int VirtualMethod1();
  virtual int VirtualMethod2();
  virtual int VirtualMethod3();

protected:
  int rootData;
};

class DiamondLeft : public virtual DiamondRoot {
public:
  DiamondLeft(int rootData, int data);

  int VirtualMethod1() override;

protected:
  int leftData;
};

class DiamondRight : public virtual DiamondRoot {
public:
  DiamondRight(int rootData, int data);

  int VirtualMethod2() override;

protected:
  int rightData;
};

class DiamondChild : public DiamondLeft, public DiamondRight {
public:
  DiamondChild(int rootData, int leftData, int rightData, int data);

  int VirtualMethod3() override;

protected:
  int childData;
};

#undef STRINGIZE1
#undef STRINGIZE
#undef LIB_STRING
#undef LIB_SUFFIX
