#include "gmock/gmock.h"
#include "gtest/gtest.h"
#include <cstdio>
#include <cstdlib>

#include "a.h"
#include "b.h"

TEST(CtorDtorTests, TestClassA) {
  int initA = 1;
  liba::Class *a = new liba::Class(initA);
  const char *payloadA = "Class A payload\n";
  int retA = a->Payload(payloadA);
  delete a;
  ASSERT_EQ(retA, initA + 2);
}

TEST(CtorDtorTests, TestClassB) {
  int initB = 2;
  libb::Class *b = new libb::Class(initB);
  const char *payloadB = "Class B payload\n";
  int retB = b->Payload(payloadB);
  delete b;
  ASSERT_EQ(retB, initB + 2);
}

template <typename T, int Add = 0> struct InitA {
  using Type = T;
  static constexpr int InitVal = 1;
  static constexpr int AddVal = Add;
};

template <typename T, int Add = 0> struct InitB {
  using Type = T;
  static constexpr int InitVal = 2;
  static constexpr int AddVal = Add;
};

using PolyTypes =
    ::testing::Types<InitA<liba::GlobalPoly, 0>, InitB<libb::GlobalPoly, 0>,
                     InitA<liba::InlinePoly, 10>, InitB<libb::InlinePoly, 10>,
                     InitA<liba::LocalPoly, 20>, InitB<libb::LocalPoly, 20>>;

template <typename PolyT> class VtableTests : public ::testing::Test {
public:
  using ::testing::Test::Test;
};

TYPED_TEST_SUITE(VtableTests, PolyTypes);

TYPED_TEST(VtableTests, TestVirtualMethod) {
  int initVal = TypeParam::InitVal;

  auto *ptr = TypeParam::Type::New(initVal);
  ASSERT_EQ(ptr->VirtualMethod(), initVal);
  delete ptr;
}

TYPED_TEST(VtableTests, TestDerivedVirtualMethod) {
  int initVal = TypeParam::InitVal;
  int addVal = TypeParam::AddVal;

  auto *ptr = TypeParam::Type::NewDerived(initVal);
  ASSERT_EQ(ptr->VirtualMethod(), initVal + 10 + addVal);
  delete ptr;
}

using DiamondTypes =
    ::testing::Types<InitA<liba::DiamondChild>, InitB<libb::DiamondChild>>;

template <typename DiamondT>
class VirtualInheritanceTests : public ::testing::Test {
public:
  using ::testing::Test::Test;
  using DiamondChildT = typename DiamondT::Type;
  using DiamondLeftT = typename DiamondChildT::DiamondLeft;
  using DiamondRightT = typename DiamondChildT::DiamondRight;
  using DiamondRootT = typename DiamondChildT::DiamondRoot;

  __attribute__((noinline)) DiamondRootT *createRoot(int val) {
    return new DiamondRootT(val);
  }
  __attribute__((noinline)) DiamondRootT *createLeft(int val, int leftVal) {
    return new DiamondLeftT(val, leftVal);
  }
  __attribute__((noinline)) DiamondRootT *createRight(int val, int rightVal) {
    return new DiamondRightT(val, rightVal);
  }
  __attribute__((noinline)) DiamondRootT *
  createChild(int val, int leftVal, int rightVal, int childVal) {
    return new DiamondChildT(val, leftVal, rightVal, childVal);
  }
};

TYPED_TEST_SUITE(VirtualInheritanceTests, DiamondTypes);

TYPED_TEST(VirtualInheritanceTests, TestRoot) {
  int initVal = TypeParam::InitVal;

  auto *ptr = this->createRoot(initVal);
  EXPECT_EQ(ptr->VirtualMethod(), initVal);
  EXPECT_EQ(ptr->VirtualMethod1(), initVal + 1);
  EXPECT_EQ(ptr->VirtualMethod2(), initVal - 1);
  EXPECT_EQ(ptr->VirtualMethod3(), initVal * 2);
}

TYPED_TEST(VirtualInheritanceTests, TestLeft) {
  int initVal = TypeParam::InitVal;
  int leftVal = 10 + initVal;

  auto *ptr = this->createLeft(initVal, leftVal);
  EXPECT_EQ(ptr->VirtualMethod(), initVal);
  EXPECT_EQ(ptr->VirtualMethod1(), leftVal);
  EXPECT_EQ(ptr->VirtualMethod2(), initVal - 1);
  EXPECT_EQ(ptr->VirtualMethod3(), initVal * 2);
}

TYPED_TEST(VirtualInheritanceTests, TestRight) {
  int initVal = TypeParam::InitVal;
  int rightVal = 20 + initVal;

  auto *ptr = this->createRight(initVal, rightVal);
  EXPECT_EQ(ptr->VirtualMethod(), initVal);
  EXPECT_EQ(ptr->VirtualMethod1(), initVal + 1);
  EXPECT_EQ(ptr->VirtualMethod2(), rightVal);
  EXPECT_EQ(ptr->VirtualMethod3(), initVal * 2);
}

TYPED_TEST(VirtualInheritanceTests, TestChild) {
  int initVal = TypeParam::InitVal;
  int leftVal = 10 + initVal;
  int rightVal = 20 + initVal;
  int childVal = 30 + initVal;

  auto *ptr = this->createChild(initVal, leftVal, rightVal, childVal);
  EXPECT_EQ(ptr->VirtualMethod(), initVal);
  EXPECT_EQ(ptr->VirtualMethod1(), leftVal);
  EXPECT_EQ(ptr->VirtualMethod2(), rightVal);
  EXPECT_EQ(ptr->VirtualMethod3(), childVal);
}

int main(int argc, char **argv) {
  testing::InitGoogleMock(&argc, argv);
  return RUN_ALL_TESTS();
}
