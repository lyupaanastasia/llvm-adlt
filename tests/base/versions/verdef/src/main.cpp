//===--- utils/unittest/UnitTestMain/TestMain.cpp - unittest driver -------===//
//
// Part of the LLVM Project, under the Apache License v2.0 with LLVM Exceptions.
// See https://llvm.org/LICENSE.txt for license information.
// SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception
//
//===----------------------------------------------------------------------===//

#include "gmock/gmock.h"
#include "gtest/gtest.h"
#include <stdio.h>
#include <stdlib.h>

extern "C" {
int get_valA_fromA();
int get_valA_fromB();
int get_valB_fromB();
int get_valB_fromA();

extern int duplicate_a;
extern int duplicate_b;
extern int duplicate_1;
extern int duplicate_2;
};

TEST(VersionsTest, testCase1) {
  // get versioned symbol from each lib
  ASSERT_EQ(1, get_valA_fromA());
  ASSERT_EQ(2, get_valB_fromB());
  // cross getting val
  ASSERT_EQ(1, get_valA_fromB());
  ASSERT_EQ(2, get_valB_fromA());
}

TEST(VersionsTest, testCase2) {
  // different names, same versions
  ASSERT_EQ(3, duplicate_a);
  ASSERT_EQ(4, duplicate_b);
  // same names, different versions
  ASSERT_EQ(5, duplicate_1);
  // same names, same versions
  ASSERT_EQ(7, duplicate_2);
}

int main(int argc, char **argv) {
  testing::InitGoogleMock(&argc, argv);
  return RUN_ALL_TESTS();
}
