#!/bin/bash

LOG=$(dirname "$0")/test.log

eval $* &> $LOG
grep -m 1 -L "val: symbol not found" $LOG
EXITVAL=$?

if [[ EXITVAL -eq 0 ]]; then
    echo "val: symbol not found"
    echo "Test was successful!"
    exit 0
else
    echo "Bad exit val: more info in $LOG" 1>& 2
    exit 1
fi
