#include <stdio.h>

extern int val;
extern int duplicate_a;
extern int duplicate_1;
extern int duplicate_2;

int get_val_fromA() { return val; };
int get_duplicateA_fromA()  { return duplicate_a; };
int get_duplicate1_fromA()  { return duplicate_1; };
int get_duplicate2_fromA()  { return duplicate_2; };
