//===--- utils/unittest/UnitTestMain/TestMain.cpp - unittest driver -------===//
//
// Part of the LLVM Project, under the Apache License v2.0 with LLVM Exceptions.
// See https://llvm.org/LICENSE.txt for license information.
// SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception
//
//===----------------------------------------------------------------------===//

#include "gmock/gmock.h"
#include "gtest/gtest.h"
#include <stdio.h>
#include <stdlib.h>

extern "C" {
int get_val_fromA();
int get_val_fromB();
int get_duplicateA_fromA();
int get_duplicate1_fromA();
int get_duplicate2_fromA();
int get_duplicateB_fromB();
int get_duplicate1_fromB();
int get_duplicate2_fromB();
};

TEST(VerneedsTest, testCase1) {
  // get sym from adlt from outer lib
  ASSERT_EQ(1, get_val_fromA());
  ASSERT_EQ(2, get_val_fromB());
}

TEST(VerneedsTest, testCase2) {
  // different names, same versions
  ASSERT_EQ(3, get_duplicateA_fromA());
  ASSERT_EQ(4, get_duplicateB_fromB());
  // same names, different versions
  ASSERT_EQ(5, get_duplicate1_fromA());
  ASSERT_EQ(6, get_duplicate1_fromB());
  // same names, same versions
  ASSERT_EQ(7, get_duplicate2_fromA());
  ASSERT_EQ(7, get_duplicate2_fromB());
}


int main(int argc, char **argv) {
  testing::InitGoogleMock(&argc, argv);
  return RUN_ALL_TESTS();
}
