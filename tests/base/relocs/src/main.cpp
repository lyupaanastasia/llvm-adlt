#include "tests.h"
#include "gmock/gmock.h"
#include "gtest/gtest.h"

TEST(RelocTests, dyn_relocs) {
  ASSERT_EQ(5, relative_test());
  ASSERT_EQ(2, glob_dat_test());
  ASSERT_EQ(1, jump_slot_test());
}

TEST(RelocTests, abs_relocs) { ASSERT_EQ(3, abs64_test()); }

TEST(RelocTests, plt_relocs) {
  ASSERT_EQ(1, jump26_test());
  ASSERT_EQ(1, call26_test());
  ASSERT_EQ(1, condbr19_test());
  ASSERT_EQ(1, tstbr14_test());
}

TEST(RelocTests, got_relocs) {
  ASSERT_EQ(1, ld64_got_lo12_nc_test());
  ASSERT_EQ(1, ld64_gotpage_lo15_test());
}

TEST(RelocTests, pc_relative_relocs) {
  ASSERT_EQ(1, adr_prel_pg_hi21_test());
  ASSERT_EQ(1, adr_prel_pg_hi21_nc_test());
  ASSERT_EQ(1, adr_prel_lo21_test());
  ASSERT_EQ(1, ld_prel_lo19_test());
}

TEST(RelocTests, null_relocs) { ASSERT_EQ(0, none_test()); }

TEST(RelocTests, tlsdesc_relocs) { ASSERT_EQ(2, tlsdesc_test()); }

TEST(RelocTests, tls_dyn_relocs) { ASSERT_EQ(2, tls_tprel64_test()); }

TEST(RelocTests, tlsie_relocs) { ASSERT_EQ(2, tlsie_adr_ld64_test()); }

int main(int argc, char **argv) {
  testing::InitGoogleMock(&argc, argv);
  return RUN_ALL_TESTS();
}
