	.text


	.globl	hi21_hidden_func
	.hidden hi21_hidden_func
	.p2align	4
	.type	hi21_hidden_func,@function
hi21_hidden_func:
	mov	w0, #1
	ret
.Lfunc_end0:
	.size	hi21_hidden_func, .Lfunc_end0-hi21_hidden_func


	.globl	adr_prel_pg_hi21_test
	.p2align	2
	.type	adr_prel_pg_hi21_test,@function
adr_prel_pg_hi21_test:
	stp	x29, x30, [sp, #-16]!
	mov	x29, sp
	adrp x8, hi21_hidden_func // R_AARCH64_ADR_PREL_PG_HI21
	add	x9, x8, #:lo12:hi21_hidden_func // R_AARCH64_LDST64_ABS_LO12_NC
	blr	x9
	ldp	x29, x30, [sp], #16
	ret
.Lfunc_end1:
	.size	adr_prel_pg_hi21_test, .Lfunc_end1-adr_prel_pg_hi21_test


	.section	".note.GNU-stack","",@progbits
	.addrsig
	.addrsig_sym hi21_hidden_func
