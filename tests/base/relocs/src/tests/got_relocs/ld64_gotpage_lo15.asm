	// partially based on llvm-project/lld/test/ELF/aarch64-gotpage.s
	.text


	.globl	ld64_gotpage_lo15_test
	.p2align	2
	.type	ld64_gotpage_lo15_test,@function
ld64_gotpage_lo15_test:
	adrp	x8, :got:ld64_gotpage_lo15_var // R_AARCH64_ADR_GOT_PAGE
	ldr	x8, [x8, :gotpage_lo15:ld64_gotpage_lo15_var] // R_AARCH64_LD64_GOTPAGE_LO15
	ldr	w0, [x8]
	ret
.Lfunc_end0:
	.size	ld64_gotpage_lo15_test, .Lfunc_end0-ld64_gotpage_lo15_test


	.type	ld64_gotpage_lo15_var,@object
	.data
	.globl	ld64_gotpage_lo15_var
	.p2align	2
ld64_gotpage_lo15_var:
	.word	1
	.size	ld64_gotpage_lo15_var, 4


	.section	".note.GNU-stack","",@progbits
	.addrsig
	.addrsig_sym ld64_gotpage_lo15_var
