	// partially based on llvm-project/llvm/test/MC/AArch64/elf-reloc-pcreladdressing.s
	.text


	.globl	ld64_got_lo12_nc_test
	.p2align	2
	.type	ld64_got_lo12_nc_test,@function
ld64_got_lo12_nc_test:
	adrp	x8, :got:ld64_got_lo12_nc_var // R_AARCH64_ADR_GOT_PAGE
	ldr	x8, [x8, :got_lo12:ld64_got_lo12_nc_var] // R_AARCH64_LD64_GOT_LO12_NC
	ldr	w0, [x8]
	ret
.Lfunc_end0:
	.size	ld64_got_lo12_nc_test, .Lfunc_end0-ld64_got_lo12_nc_test


	.type	ld64_got_lo12_nc_var,@object
	.data
	.globl	ld64_got_lo12_nc_var
	.p2align	2
ld64_got_lo12_nc_var:
	.word	1
	.size	ld64_got_lo12_nc_var, 4


	.section	".note.GNU-stack","",@progbits
	.addrsig
	.addrsig_sym ld64_got_lo12_nc_var
