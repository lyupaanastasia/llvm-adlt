	.text


	.globl	jump_slot_f1
	.p2align	2
	.type	jump_slot_f1,@function
jump_slot_f1:
	mov	w0, #1
	ret
.Lfunc_end0:
	.size	jump_slot_f1, .Lfunc_end0-jump_slot_f1


	.globl	jump_slot_test
	.p2align	2
	.type	jump_slot_test,@function
jump_slot_test:
	stp	x29, x30, [sp, #-16]!
	mov	x29, sp
	bl	jump_slot_f1
	ldp	x29, x30, [sp], #16
	ret
.Lfunc_end1:
	.size	jump_slot_test, .Lfunc_end1-jump_slot_test


	.section	".note.GNU-stack","",@progbits
	.addrsig
	.addrsig_sym jump_slot_f1
