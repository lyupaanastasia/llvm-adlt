	// partially based on llvm-project/lld/test/ELF/aarch64-relative.s
	.text


	.globl	call26_f1
	.p2align	2
	.type	call26_f1,@function
call26_f1:
	mov	w0, #1
	ret
.Lfunc_end0:
	.size	call26_f1, .Lfunc_end0-call26_f1


	.globl	call26_test
	.p2align	2
	.type	call26_test,@function
call26_test:
	stp	x29, x30, [sp, #-16]!
	mov	x29, sp
	bl	call26_f1
	ldp	x29, x30, [sp], #16
	ret
.Lfunc_end1:
	.size	call26_test, .Lfunc_end1-call26_test


	.section	".note.GNU-stack","",@progbits
	.addrsig
	.addrsig_sym call26_f1
