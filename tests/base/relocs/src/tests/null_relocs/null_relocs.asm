	// partially based on llvm-project/llvm/test/MC/AArch64/reloc-directive.s
	.text


	.globl	none_test
	.p2align	2
	.type	none_test,@function
none_test:
	.reloc 0, R_AARCH64_NONE, i
	mov	w0, wzr
	ret
.Lfunc_end0:
	.size	none_test, .Lfunc_end0-none_test


	.type	i,@object
	.data
	.globl	i
	.p2align	2
i:
	.word	1
	.size	i, 4


	.section	".note.GNU-stack","",@progbits
	.addrsig
	.addrsig_sym i
