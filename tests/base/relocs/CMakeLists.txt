initProject(PROJECT_NAME relocsTests EXECUTABLES main
    LIBS
        dyn_relocs
        abs_relocs
        plt_relocs
        got_relocs
        pc_relative_relocs
        null_relocs
        tlsdesc_relocs
        tls_dyn_relocs
        tlsie_relocs
)


enable_language(ASM)
add_library(${dyn_relocs} SHARED
    src/tests/dyn_relocs/glob_dat.asm
    src/tests/dyn_relocs/jump_slot.asm
    src/tests/dyn_relocs/relative.asm
)

add_library(${abs_relocs} SHARED
    src/tests/abs_relocs/abs64.asm
)

add_library(${plt_relocs} SHARED
    src/tests/plt_relocs/jump26.asm
    src/tests/plt_relocs/call26.asm
    src/tests/plt_relocs/condbr19.asm
    src/tests/plt_relocs/tstbr14.asm
)

add_library(${got_relocs} SHARED
    src/tests/got_relocs/ld64_got_lo12_nc.asm
    src/tests/got_relocs/ld64_gotpage_lo15.asm
)

add_library(${pc_relative_relocs} SHARED
    src/tests/pc_relative_relocs/adr_prel_pg_hi21.asm
    src/tests/pc_relative_relocs/adr_prel_pg_hi21_nc.asm
    src/tests/pc_relative_relocs/adr_prel_lo21.asm
    src/tests/pc_relative_relocs/ld_prel_lo19.asm
)

add_library(${null_relocs} SHARED
    src/tests/null_relocs/null_relocs.asm
)

add_library(${tlsdesc_relocs} SHARED
    src/tests/tlsdesc_relocs/tlsdesc.asm
)

add_library(${tls_dyn_relocs} SHARED
    src/tests/tls_dyn_relocs/tls_tprel64.asm
)

add_library(${tlsie_relocs} SHARED
    src/tests/tlsie_relocs/tlsie_adr_ld64.asm
)


add_custom_target(
	${ADLT_PROJECT_LIB}  ALL
	DEPENDS ${dyn_relocs} ${abs_relocs}
	        ${plt_relocs} ${got_relocs}
	        ${pc_relative_relocs} ${null_relocs}
	        ${tlsdesc_relocs} ${tls_dyn_relocs}
	        ${tlsie_relocs}
	COMMAND python3 ${toolsDir}/build_adlt_lib.py
            -i ${ADLT_LIB_CONTENTS}
	    -od ${CMAKE_CURRENT_BINARY_DIR}
            -o "${ADLT_PROJECT_LIB}"
    )

add_adlt_test(TARGET ${main} SOURCES src/main.cpp
    LIBS
        ${dyn_relocs}
        ${abs_relocs}
        ${plt_relocs}
        ${got_relocs}
        ${pc_relative_relocs}
        ${null_relocs}
        ${tlsdesc_relocs}
        ${tls_dyn_relocs}
        ${tlsie_relocs}
)
