#include <stdio.h>

extern const int valA;
extern const int valB;
const int valC = 3;
int val = 3;

int get_val() { return val; }
int get_val_from_C() { return val; }
int get_valA_from_C() { return valA; }
int get_valB_from_C() { return valB; }
int get_valC_from_C() { return valC; }
