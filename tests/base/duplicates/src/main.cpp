//===--- utils/unittest/UnitTestMain/TestMain.cpp - unittest driver -------===//
//
// Part of the LLVM Project, under the Apache License v2.0 with LLVM Exceptions.
// See https://llvm.org/LICENSE.txt for license information.
// SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception
//
//===----------------------------------------------------------------------===//

#include "gmock/gmock.h"
#include "gtest/gtest.h"
#include <stdio.h>
#include <stdlib.h>

extern "C" {
extern int val;
int get_val(void);

int get_val_from_A(void);
int get_val_from_B(void);
int get_val_from_C(void);

int get_valA_from_A(void);
int get_valB_from_A(void);
int get_valC_from_A(void);
int get_valA_from_B(void);
int get_valB_from_B(void);
int get_valC_from_B(void);
int get_valA_from_C(void);
int get_valB_from_C(void);
int get_valC_from_C(void);
};

TEST(DuplicatesTests, TestGetVal1) {
  ASSERT_EQ(1, get_val_from_A());
  ASSERT_EQ(2, get_val_from_B());
  ASSERT_EQ(3, get_val_from_C());
}

TEST(DuplicatesTests, TestGetVal2) {
  ASSERT_EQ(1, get_valA_from_A());
  ASSERT_EQ(2, get_valB_from_A());
  ASSERT_EQ(3, get_valC_from_A());

  ASSERT_EQ(1, get_valA_from_B());
  ASSERT_EQ(2, get_valB_from_B());
  ASSERT_EQ(3, get_valC_from_B());

  ASSERT_EQ(1, get_valA_from_C());
  ASSERT_EQ(2, get_valB_from_C());
  ASSERT_EQ(3, get_valC_from_C());
}

// This test used symbols from first lib (A)
TEST(DuplicatesTests, TestGetVal3) {
  ASSERT_EQ(1, val);
  ASSERT_EQ(1, get_val());
}


int main(int argc, char **argv) {
  testing::InitGoogleMock(&argc, argv);
  return RUN_ALL_TESTS();
}
