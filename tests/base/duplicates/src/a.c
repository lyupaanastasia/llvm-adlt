#include <stdio.h>

extern const int valB;
extern const int valC;
const int valA = 1;
int val = 1;

int get_val() { return val; }
int get_val_from_A() { return val; }
int get_valA_from_A() { return valA; }
int get_valB_from_A() { return valB; }
int get_valC_from_A() { return valC; }
