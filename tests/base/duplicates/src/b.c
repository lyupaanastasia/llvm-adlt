#include <stdio.h>

extern const int valA;
extern const int valC;
const int valB = 2;
int val = 2;

int get_val() { return val; }
int get_val_from_B() { return val; }
int get_valA_from_B() { return valA; }
int get_valB_from_B() { return valB; }
int get_valC_from_B() { return valC; }
