#include "gmock/gmock.h"
#include "gtest/gtest.h"
#include <stdlib.h>
#include <cstdio>

#include "testdlopen.h"

TEST(DlopenCPPTests, TestGetStorage) {
  ASSERT_EQ(42, testdlopen::testCtor(PROJECT_LIB_PREFIX"dlopencpp-lib.so", 42));
}

TEST(DlopenCPPTests, TestGetVer) {
  ASSERT_EQ("1.1", testdlopen::testVer(PROJECT_LIB_PREFIX"dlopencpp-lib.so"));
}

TEST(DlopenCPPTests, TestPrintf) {
  ASSERT_EQ(0, testdlopen::testPrintf(PROJECT_LIB_PREFIX"dlopencpp-lib.so", "some text\n"));
}

TEST(DlopenCPPTests, TestMax) {
  ASSERT_EQ(2, testdlopen::testMax(PROJECT_LIB_PREFIX"dlopencpp-lib.so", 1, 2));
}

int main(int argc, char **argv) {
  testing::InitGoogleMock(&argc, argv);
  return RUN_ALL_TESTS();
}
