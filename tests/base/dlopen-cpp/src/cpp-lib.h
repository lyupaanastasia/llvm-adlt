#pragma once

#include "cpp-lib-iface.h"

namespace cpplib {
class CppDynamicLib : public ICppDynamicLib {
  private:
    const int versionMajor = 1;
    const int versionMinor = 1;

    int storageVar;

    std::string makeVersion() const;
  public: 
    CppDynamicLib(int value) : storageVar(value) {};
    virtual int getStorage() const;
    virtual int getMax(int a, int b) const;
    virtual std::string getVersion(void) const;
    
};
}// namesapce cpplib