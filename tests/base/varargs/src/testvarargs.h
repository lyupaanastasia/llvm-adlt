#pragma once

namespace testvarargs {
int simplePrintf(void);
void formattedPrintf(void);
int simpleSumFunction(int, int);
int varargsSumFunction(int, ...);
} // namespace testvarargs