#include "gmock/gmock.h"
#include "gtest/gtest.h"
#include <stdlib.h>
#include <cstdio>

#include "testvarargs.h"

TEST(VarArgsTests, TestSimpleSum) {
  ASSERT_EQ(1+2, testvarargs::simpleSumFunction(1, 2));
}
TEST(VarArgsTests, TestVarArgsSum) {
  ASSERT_EQ(1+2+3, testvarargs::varargsSumFunction(3, 1, 2, 3));
}

int main(int argc, char **argv) {
  testing::InitGoogleMock(&argc, argv);
  return RUN_ALL_TESTS();
}