//===--- utils/unittest/UnitTestMain/TestMain.cpp - unittest driver -------===//
//
// Part of the LLVM Project, under the Apache License v2.0 with LLVM Exceptions.
// See https://llvm.org/LICENSE.txt for license information.
// SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception
//
//===----------------------------------------------------------------------===//

#include "gmock/gmock.h"
#include "gtest/gtest.h"
#include <stdio.h>
#include <stdlib.h>

extern "C" {
int f1(void);
int f2(void);

void testPrintA(void);
void testPrintB(void);

int testPrintMessageAfromA(void);
int testPrintMessageBfromB(void);

int testPrintMessageAfromB(void);
int testPrintMessageBfromA(void);

extern const char *strA;
extern const char *strB;

extern const char *msgA;
extern const char *msgB;
};

TEST(CrossTests, CheckSources) {
  ASSERT_STREQ("a.c", strA);
  ASSERT_STREQ("b.c", strB);
}

TEST(CrossTests, CheckMessages) {
  ASSERT_STREQ("message from lib A!", msgA);
  ASSERT_STREQ("message from lib B!", msgB);
}

TEST(CrossTests, TestSumF1F2) {
  int sum = 0;
  const int countRuns = 1000000;
  for (size_t i = 0; i < countRuns; i++)
    sum += f1();
  ASSERT_EQ(3000000, sum);

  for (size_t i = 0; i < countRuns; i++)
    sum += f2();
  ASSERT_EQ(7000000, sum);
}

int main(int argc, char **argv) {
  printf("Hello! This test contains libs built from sources: ");
  const char *sources[] = {strA, strB};
  const int countSources = sizeof(sources) / sizeof(sources[0]);
  for (size_t i = 0; i < countSources; i++)
    printf("%s ", sources[i]);
  printf("\n");

  printf("Test print calls in libs: \n");
  testPrintA();
  testPrintB();

  testPrintMessageAfromA();
  testPrintMessageBfromB();

  testPrintMessageAfromB();
  testPrintMessageBfromA();

  // run gtests
  testing::InitGoogleMock(&argc, argv);
  return RUN_ALL_TESTS();
}
