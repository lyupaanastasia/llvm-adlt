#include <stdio.h>

extern int f3(void);
extern const char *msgB;

// int B = 0;

const char *strA = "a.c";
const char *msgA = "message from lib A!";

void testPrintA(void) { printf("this is lib A!\n"); }

void testPrintMessageAfromA(void) { printf("%s\n", msgA); }

void testPrintMessageBfromA(void) {
  printf("print msg B from %s: %s\n", strA, msgB);
}

int f1(void) { return f3(); }

int f4(void) { return 4; }