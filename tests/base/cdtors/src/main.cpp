#include "gmock/gmock.h"
#include "gtest/gtest.h"
#include <stdlib.h>
#include <stdio.h>
#include <memory>

#include "a.h"
#include "b.h"

TEST(ApiTests, ClassAFlagsAPI) {
  int initA = 0b01;
  auto sp_a = std::make_unique<liba::ClassA>(initA);
  ASSERT_EQ(sp_a->Value(), 0b01);
  sp_a->SetFlag(1);
  auto ret = sp_a->Value();
  sp_a.reset();
  ASSERT_EQ(ret, 0b11);
}

TEST(ApiTests, ClassBFlagsAPI) {
  int initB = 0b10;
  auto sp_b = std::make_unique<libb::ClassB>(initB);
  ASSERT_EQ(sp_b->Value(), 0b10);
  sp_b->SetFlag(0);
  auto ret = sp_b->Value();
  ASSERT_EQ(ret, 0b11);
}

TEST(CtorTests, TestACtors) {
  const auto& so_a_data = liba::ClassA::Instance();
  so_a_data.Log("CtorTests::TestACtors");
  ASSERT_TRUE(so_a_data.HasFlag(liba::INIT_0));
  ASSERT_TRUE(so_a_data.HasFlag(liba::INIT_1));
  ASSERT_FALSE(so_a_data.HasFlag(liba::INIT_2));
  ASSERT_FALSE(so_a_data.HasFlag(liba::INIT_3));
  ASSERT_FALSE(so_a_data.HasFlag(liba::FINI_0));
  ASSERT_FALSE(so_a_data.HasFlag(liba::FINI_1));
  ASSERT_FALSE(so_a_data.HasFlag(liba::FINI_2));
  ASSERT_FALSE(so_a_data.HasFlag(liba::FINI_3));
}

TEST(CtorTests, TestBCtors) {
  const auto& so_b_data = libb::ClassB::Instance();
  so_b_data.Log("CtorTests::TestBCtors");
  ASSERT_TRUE(so_b_data.HasFlag(liba::INIT_0));
  ASSERT_TRUE(so_b_data.HasFlag(liba::INIT_1));
  ASSERT_TRUE(so_b_data.HasFlag(liba::INIT_2));
  ASSERT_FALSE(so_b_data.HasFlag(liba::INIT_3));
  ASSERT_FALSE(so_b_data.HasFlag(liba::FINI_0));
  ASSERT_FALSE(so_b_data.HasFlag(liba::FINI_1));
  ASSERT_FALSE(so_b_data.HasFlag(liba::FINI_2));
  ASSERT_FALSE(so_b_data.HasFlag(liba::FINI_3));
}

TEST(CtorTests, TestAExternStaticInit) {
  ASSERT_EQ(liba::class_a_instance_0.Value(), 42+0);
  ASSERT_EQ(liba::class_a_instance_1.Value(), 42+1);
}

TEST(CtorTests, TestBExternStaticInit) {
   ASSERT_EQ(libb::class_b_instance_0.Value(), 42+0);
   ASSERT_EQ(libb::class_b_instance_1.Value(), 42+1);
   ASSERT_EQ(libb::class_b_instance_2.Value(), 42+2);
}

int main(int argc, char **argv) {
  testing::InitGoogleMock(&argc, argv);
  return RUN_ALL_TESTS();
}
