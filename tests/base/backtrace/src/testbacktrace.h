#pragma once
#include <string>

namespace testbt {
std::string testVer(const char *filename);
std::string testVerBT(const char *filename);
int testMax(const char *filename, int a, int b);
std::string testMaxBT(const char *filename);
} // namespace testbt
