#include "gmock/gmock.h"
#include "gtest/gtest.h"
#include <stdlib.h>
#include <stdio.h>

#include "testbacktrace.h"

TEST(BacktraceTests, TestGetVer) {
  ASSERT_EQ("1.0", testbt::testVer(PROJECT_LIB_PREFIX"backtrace-lib.so"));
  
  auto ret = testbt::testVerBT(PROJECT_LIB_PREFIX"backtrace-lib.so");
  ASSERT_EQ(true, ret.find("clibGetVer") != std::string::npos);
}
TEST(BacktraceTests, TestMax) {
  ASSERT_EQ(42, testbt::testMax(PROJECT_LIB_PREFIX"backtrace-lib.so", 42, 1));
  
  auto ret = testbt::testMaxBT(PROJECT_LIB_PREFIX"backtrace-lib.so");
  ASSERT_EQ(true, ret.find("clibMax") != std::string::npos);
}

int main(int argc, char **argv) {
  testing::InitGoogleMock(&argc, argv);
  return RUN_ALL_TESTS();
}
