#include <cstdio>
#include <dlfcn.h>
#include <execinfo.h>

#include "testbacktrace.h"

#define STRING_MAX_LEN  256

namespace testbt {
typedef const char* (*clibGetVer_t)();
typedef int (*clibMax_t)(int, int);

// close library
void closeLib(void *handle) {
  printf("\tClosing library\n");
  dlclose(handle);
}

// open library
void *openLib(const char *filename) {
  printf("\tLoading library %s\n", filename);
  void *handle = dlopen(filename, RTLD_LAZY);
  if (!handle) {
    printf("Error loading library!\n");
    dlerror();
  }

  return handle;
}

std::string getSymbol(void *func) {
  Dl_info info = { 0 };
  dladdr(func, &info);
  char *str_buffer = NULL;
	str_buffer = (char *)malloc(STRING_MAX_LEN * sizeof(char));
  if (info.dli_sname && info.dli_saddr && info.dli_fbase && info.dli_fname) {
    snprintf(str_buffer, STRING_MAX_LEN, "    <%s+%#lx>[%#lx] -> %s\n", info.dli_sname, 
      (uintptr_t)func - (uintptr_t)info.dli_saddr,
      (uintptr_t)func - (uintptr_t)info.dli_fbase, info.dli_fname);
    std::string ret(str_buffer);
    free(str_buffer);
    return ret;
  }

  return "Error: could not resolve symbol data\n";
}

// test if clibGetVer is loading and working properly
std::string testVer(const char *filename) {
  printf("Testing clibGetVer\n");
  void *handle = openLib(filename);
  if (!handle) {
    printf("Exiting!\n");
    return "test failure";
  }

  printf("\tLoading symbol clibGetVet\n");
  clibGetVer_t getVer = (clibGetVer_t) dlsym(handle, "clibGetVer");

  printf("\tCalling clibGetVer\n");
  const char *ver = getVer();

  closeLib(handle);

  printf("Exiting clibGetVer test\n");
  return ver;
}

// test if clibGetVer symbol can be backteaced properly after loading
std::string testVerBT(const char *filename) {
  printf("Testing clibGetVer symbol backtrace\n");
  void *handle = openLib(filename);
  if (!handle) {
    printf("Exiting!\n");
    return "test failure";
  }

  printf("\tLoading symbol clibGetVer\n");
  clibGetVer_t getVer = (clibGetVer_t) dlsym(handle, "clibGetVer");

  auto ret = getSymbol((void *)getVer);
  printf("\tThis is what dladdr returned: %s\n", ret.c_str());

  closeLib(handle);

  printf("Exiting clibGetVer symbol backtrace test\n");
  return ret;
}

// test if clibmax is loading and working properly
int testMax(const char *filename, int a, int b) {
  printf("Testing clibMax\n");
  void *handle = openLib(filename);
  if (!handle) {
    printf("Exiting!\n");
    return -1;
  }
  
  printf("\tLoading symbol clibMax\n");
  clibMax_t libMax = (clibMax_t) dlsym(handle, "clibMax");

  printf("\tCalling clibMax\n");
  int max =  libMax(a, b);

  closeLib(handle);

  printf("Exiting clibMax test\n");
  return max;
}

// test if clibGetVer symbol can be backteaced properly after loading
std::string testMaxBT(const char *filename) {
  printf("Testing clibMax symbol backtrace\n");
  void *handle = openLib(filename);
  if (!handle) {
    printf("Exiting!\n");
    return "test failure";
  }

  printf("\tLoading symbol clibMax\n");
  clibMax_t libMax = (clibMax_t) dlsym(handle, "clibMax");

  auto ret = getSymbol((void *)libMax);
  printf("\tThis is what dladdr returned: %s\n", ret.c_str());

  closeLib(handle);

  printf("Exiting clibMax symbol backtrace test\n");
  return ret;
}
} // namespace testbt
