#include <stdio.h>

const char *msgB = "message from lib B!";

int fB(void) {
  printf("LIB B:\n%s\n", msgB);
  return 1;
}
