//===--- utils/unittest/UnitTestMain/TestMain.cpp - unittest driver -------===//
//
// Part of the LLVM Project, under the Apache License v2.0 with LLVM Exceptions.
// See https://llvm.org/LICENSE.txt for license information.
// SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception
//
//===----------------------------------------------------------------------===//

#include "gmock/gmock.h"
#include "gtest/gtest.h"
#include <dlfcn.h>
#include <stdio.h>
#include <stdlib.h>

int g_argc; // Making arg and arv global to access within TESTs
char **g_argv;

int (*fE)() = nullptr;


#define RESOLVE(handle, symbol)                                                \
  *(void **)(&symbol) = dlsym(handle, #symbol);                                \
  if (symbol == 0) {                                                           \
    fprintf(stderr, "dlsym() failed for %s with %s\n", #symbol, dlerror());    \
    exit(EXIT_FAILURE);                                                        \
  }

TEST(Deps, TestDlopen) {
  const char *def_libname = "libdepsTests_e.so";
  const char *libname = g_argc > 1 ? g_argv[1] : def_libname;
  // init
  void *handle = dlopen(libname, RTLD_LAZY);
  if (!handle) {
    fprintf(stderr, "dlopen() failed for %s with %s\n", libname, dlerror());
    exit(EXIT_FAILURE);
  }
  // check
  RESOLVE(handle, fE);
  int res = fE();
  ASSERT_EQ(res, 2);

  dlclose(handle);
}

int main(int argc, char **argv) {
  // run gtests
  testing::InitGoogleMock(&argc, argv);
  g_argc = argc;
  g_argv = argv;
  return RUN_ALL_TESTS();
}
