#include <stdio.h>

extern int fB(void);

const char *msgA = "message from lib A!";

int fA(void) {
  printf("LIB A:\n%s\nCall fB():\n", msgA);
  int res = fB();
  printf("\n");
  return 1 + res;
}
