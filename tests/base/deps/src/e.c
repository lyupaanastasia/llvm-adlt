#include <stdio.h>

extern int fA(void);

const char *msgE = "message from lib E!";

int fE(void) {
  printf("LIB E:\n%s\nCall fA():\n", msgE);
  return fA();
}
