//===--- utils/unittest/UnitTestMain/TestMain.cpp - unittest driver -------===//
//
// Part of the LLVM Project, under the Apache License v2.0 with LLVM Exceptions.
// See https://llvm.org/LICENSE.txt for license information.
// SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception
//
//===----------------------------------------------------------------------===//

#include "gmock/gmock.h"
#include "gtest/gtest.h"
#include <stdio.h>
#include <stdlib.h>

extern "C" {
extern int fE(void);
};

TEST(Deps, TestStandardLinking) {
  int res = fE();
  ASSERT_EQ(res, 2);
}

int main(int argc, char **argv) {
  // run gtests
  testing::InitGoogleMock(&argc, argv);
  return RUN_ALL_TESTS();
}
