# Guideline for writing benchmarks

The `main.py -p ...` allows to benchmark any binary from our tests but
majority of them uses `libgtest` which is legacy and is heavy - it will
cause a baseline in measurement.

So, let us write benchmarks as lightweight as possible - without gtest,
and if it is plain c - without C++ libraries.
