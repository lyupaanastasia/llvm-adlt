#include <chrono>
#include <iostream>
#include <thread>

extern "C" {
#include "common.h"
}

#define THR_DEBUG 0

typedef std::function<void(void *)> callback_cpp_t;

namespace ver {
namespace CPP {
void startThreads(int num, int *data, callback_cpp_t callback, cpp_data_t *outResults);
void stopThreads(void);
void threadFunc(void *param);
} // namespace CPP
} // namespace ver

static std::thread *threads = NULL;
static int NUM_THREADS = 0;
static threadparam_t *tData = NULL;

thread_local cpp_data_t tlsData = CPP_TLS_DATA_INIT;

void ver::CPP::threadFunc(void *param) {
  threadparam_t *threadData = static_cast<threadparam_t *>(param);
  tlsData += (cpp_data_t)threadData->data;

#if THR_DEBUG
  std::__thread_id id = std::this_thread::get_id();
  std::cout << "CPP: Thread 0x" << std::hex << id << std::dec << ": "
            << "Entered! id: " << threadData->id << " " 
            << "TlsData: " << tlsData
            << "\n";
#endif

  while (waitFlag) //{}
    std::this_thread::yield();

  tlsData++;
  *(cpp_data_t *)threadData->dest = tlsData;

#if THR_DEBUG
  std::cout << "CPP: Thread 0x" << std::hex << id << std::dec << ": "
            << "End loop! id: " << threadData->id << " " 
            << "Result: " << *(cpp_data_t *)threadData->dest
            << "\n";
#endif
}

void ver::CPP::startThreads(int num, int *data, callback_cpp_t callback,
                            cpp_data_t *outResults) {
  threads = new std::thread[num];
  tData = new threadparam_t[num];
  if (!callback)
    callback = threadFunc;
  NUM_THREADS = num;

  for (int i = 0; i < num; i++) {
#if THR_DEBUG
    std::cout << "CPP: Starting thread (id: " << std::dec << i << "): "
              << "with data: " << data[i] 
              << "\n";
#endif
    tData[i] =
        (threadparam_t){.id = i, .data = data[i], .dest = &outResults[i]};
    threads[i] = std::thread(callback, &tData[i]);
  }
}

void ver::CPP::stopThreads() {
  for (int i = 0; i < NUM_THREADS; i++)
    threads[i].join();
  delete[] threads;
  delete[] tData;
}
