// TLS test for ADLT project

/* references:
https://maskray.me/blog/2021-02-14-all-about-thread-local-storage
https://www.ibm.com/docs/en/xl-c-and-cpp-linux/16.1.0?topic=descriptions-ftls-model-qtls
https://man7.org/linux/man-pages/man2/sched_yield.2.html
*/

#include "gmock/gmock.h"
#include "gtest/gtest.h"
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <thread>

extern "C" {
#include "common.h"
bool waitFlag;
}

#define THR_DEBUG 0

namespace ver {

namespace C {
extern "C" {
void startThreads(int num, int *data, callback_t callback, c_data_t *outResults);
void stopThreads(void);
void *mainThreadFunc(void *param);
const c_array_t initData = {C_INIT};

__thread c_array_t mainTlsData = {C_INIT};
};
} // namespace C

namespace CPP {
typedef std::function<void(void *)> callback_cpp_t;
void startThreads(int num, int *data, callback_cpp_t callback, cpp_data_t *outResults);
void stopThreads(void);
void mainThreadFunc(void *param);
const cpp_data_t initData = {CPP_TLS_DATA_INIT};

thread_local cpp_data_t mainTlsData = {CPP_TLS_DATA_INIT};
} // namespace CPP

} // namespace ver

// external callbacks
void *ver::C::mainThreadFunc(void *param) {
  threadparam_t *threadData = (threadparam_t *)param;
  for(size_t i = 0; i < C_ARRAY_LEN; ++i)
    ver::C::mainTlsData[i] += (c_data_t)threadData->data + (c_data_t)i;

#if THR_DEBUG
  unsigned long id = (unsigned long)pthread_self();
  printf("C main: Thread 0x%lx: Entered! id: %d TlsData: %g\n",
         id, threadData->id, (double)ver::C::mainTlsData[C_ARRAY_LEN - 1]);
#endif

  while (waitFlag) //{}
    sched_yield();

  for(size_t i = 0; i < C_ARRAY_LEN; ++i)
    ver::C::mainTlsData[i] += 20;

  *(c_data_t *)threadData->dest = ver::C::mainTlsData[C_ARRAY_LEN - 1];

#if THR_DEBUG
  printf("C main: Thread 0x%lx: End loop! id: %d Result: %g\n",
         id, threadData->id, (double)(*(c_data_t *)threadData->dest));
#endif
  return NULL;
}

void ver::CPP::mainThreadFunc(void *param) {
  threadparam_t *threadData = static_cast<threadparam_t *>(param);
  ver::CPP::mainTlsData += (cpp_data_t)threadData->data;

#if THR_DEBUG
  std::__thread_id id = std::this_thread::get_id();
  std::cout << "CPP main: "
            << "Thread 0x" << std::hex << id << ": Entered! "
            << "id: " << threadData->id << " "
            << "TlsData: " << ver::CPP::mainTlsData << "\n";
#endif

  while (waitFlag) //{}
    std::this_thread::yield();

  ver::CPP::mainTlsData += 20;
  *(cpp_data_t *)threadData->dest = ver::CPP::mainTlsData;

#if THR_DEBUG
  std::cout << "CPP main: "
            << "Thread 0x" << std::hex << id << ": End loop! "
            << "id: " << threadData->id << " "
            << "Result: " << *(cpp_data_t *)threadData->dest << "\n";
#endif
}

// tests
const int NUM_THREADS = 4;
c_data_t resultsC[NUM_THREADS];
cpp_data_t resultsCPP[NUM_THREADS];

TEST(TLS, internal_callback) {
  int dataC[NUM_THREADS] = {200, 2, 5, 7};
  int dataCPP[NUM_THREADS] = {201, 3, 7, 8};
  memset(resultsC, 0, NUM_THREADS * sizeof(c_data_t));
  memset(resultsCPP, 0, NUM_THREADS * sizeof(cpp_data_t));

  waitFlag = true;
  ver::C::startThreads(NUM_THREADS, dataC, NULL, resultsC);
  ver::CPP::startThreads(NUM_THREADS, dataCPP, NULL, resultsCPP);

  waitFlag = false;
  ver::C::stopThreads();
  ver::CPP::stopThreads();

  for (int i = 0; i < NUM_THREADS; i++) {
    ASSERT_EQ(resultsC[i], static_cast<c_data_t>(dataC[i] + ver::C::initData[C_ARRAY_LEN - 1] + C_ARRAY_LEN - 1 + 1));
    ASSERT_EQ(resultsCPP[i], static_cast<cpp_data_t>(dataCPP[i] + ver::CPP::initData + 1));
  }
}

TEST(TLS, external_callback) {
  int dataC[NUM_THREADS] = {100, 20, 50, 70};
  int dataCPP[NUM_THREADS] = {101, 21, 51, 71};
  memset(resultsC, 0, NUM_THREADS * sizeof(c_data_t));
  memset(resultsCPP, 0, NUM_THREADS * sizeof(cpp_data_t));

  waitFlag = true;
  ver::C::startThreads(NUM_THREADS, dataC, ver::C::mainThreadFunc, resultsC);
  ver::CPP::startThreads(NUM_THREADS, dataCPP, ver::CPP::mainThreadFunc,
                         resultsCPP);

  waitFlag = false;
  ver::C::stopThreads();
  ver::CPP::stopThreads();

  for (int i = 0; i < NUM_THREADS; i++) {
    ASSERT_EQ(resultsC[i], static_cast<c_data_t>(dataC[i] + ver::C::initData[C_ARRAY_LEN - 1] + C_ARRAY_LEN - 1 + 20));
    ASSERT_EQ(resultsCPP[i], static_cast<cpp_data_t>(dataCPP[i] + ver::CPP::initData + 20));
  }
}

int main(int argc, char **argv) {
  testing::InitGoogleMock(&argc, argv);
  return RUN_ALL_TESTS();
}
