#pragma once

#include <stdbool.h>
extern bool waitFlag;

typedef char c_data_t;
typedef int cpp_data_t;

#define C_TLS_DATA_INIT   (c_data_t)0x71
#define CPP_TLS_DATA_INIT (cpp_data_t)0x7E7E7E7E
#define C_ARRAY_LEN       (16*17)

typedef struct {
  int id;
  int data;
  void *dest;
} threadparam_t;

typedef void *(callback_t)(void *);

typedef c_data_t c_array_t[C_ARRAY_LEN];

#define C_I   C_TLS_DATA_INIT
// 'c' init data define (C_ARRAY_LEN units)
#define C_INIT \
C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, \
C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, \
C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, \
C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, \
C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, \
C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, \
C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, \
C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, \
C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, \
C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, \
C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, \
C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, \
C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, \
C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, \
C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, \
C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, \
C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I, C_I+1
