#!/usr/bin/python3
import os

main = "python3 main.py"
buildOpt = "-cb"
for i in [
    f"{main} {buildOpt} tls/emu",
    f"{main} -r tls/emu/orig",
    f"{main} -r tls/emu/adlt",

    f"{main} {buildOpt} tls/local-dynamic",
    f"{main} -r tls/local-dynamic/orig",
    f"{main} -r tls/local-dynamic/adlt",

    f"{main} {buildOpt} tls/global-dynamic",
    f"{main} -r tls/global-dynamic/orig",
    f"{main} -r tls/global-dynamic/adlt",

    f"{main} {buildOpt} tls/initial-exec",
    f"{main} -r tls/initial-exec/orig",
    f"{main} -r tls/initial-exec/adlt",
]:
    os.system(i)
