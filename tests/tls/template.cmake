macro(tlsProject)
    set(oneValueArgs PROJECT_NAME)
    set(multiValueArgs TLS_CFLAGS)
    cmake_parse_arguments(PARSED "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN})

    initProject(PROJECT_NAME ${PARSED_PROJECT_NAME}
        EXECUTABLES main LIBS aC aCPP)

    add_compile_options(${PARSED_TLS_CFLAGS})

    add_library(${aC} SHARED ../src/a.c)
    add_library(${aCPP} SHARED ../src/a.cpp)
    add_custom_target(
	${ADLT_PROJECT_LIB}  ALL
	DEPENDS ${aC} ${aCPP}
	COMMAND python3 ${toolsDir}/build_adlt_lib.py
            -i ${ADLT_LIB_CONTENTS}
	    -od ${CMAKE_CURRENT_BINARY_DIR}
            -o "${ADLT_PROJECT_LIB}"
    )


    target_link_libraries(${aC} pthread)

    add_adlt_test(TARGET ${main} SOURCES ../src/main.cpp LIBS ${aC} ${aCPP})
endmacro(tlsProject)
