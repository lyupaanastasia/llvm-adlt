#include <stdio.h>
#include <string.h>

#if !defined(SHARED_LIB1) && !defined(SHARED_LIB2)
#include "gmock/gmock.h"
#include "gtest/gtest.h"
#endif

struct FA {
  virtual void f();
};

int g_argc; // Making arg and arv global to access within TESTs
char ** g_argv;

void *create_FB();
void *create_FC();

#ifdef SHARED_LIB1

#include "utils.h"
struct FB {
  virtual void f();
};
void FB::f() {}

void *create_FB() {
  create_derivers<FB>();
  return (void *)(new FB());
}

#elif defined SHARED_LIB2

#include "utils.h"
struct FC {
  virtual void f();
};
void FC::f() {}

void *create_FC() {
  create_derivers<FC>();
  return (void *)(new FC());
}

#else

void FA::f() {}

TEST(CFICrossDsoSimpleFailTests, TestFail) {
  fprintf(stderr, "cross-dso simple fail example\n");

  FA *a;
  void *pb = create_FB(), *pc = create_FC();

  fprintf(stderr, "=0=\n");

  if (g_argc > 1 && g_argv[1][0] == 'x') {
    // Test cast. BOOM.
    // CFI-DIAG-CAST: runtime error: control flow integrity check for type 'A' failed during cast to unrelated type
    // CFI-DIAG-CAST-NEXT: note: vtable is of type '{{(struct )?}}B'
    a = (FA*)pb;
  } else {
    // Invisible to CFI. Test virtual call later.
    memcpy(&a, &pb, sizeof(a));
  }

  fprintf(stderr, "=1=\n");

  // CFI-DIAG-CALL: runtime error: control flow integrity check for type 'A'
  // failed during virtual call CFI-DIAG-CALL-NEXT: note: vtable is of type
  // '{{(struct )?}}B'
  a->f(); // UB here

  fprintf(stderr, "=2=\n");

  if (g_argc > 1 && g_argv[1][0] == 'x') {
    // Test cast. BOOM.
    // CFI-DIAG-CAST: runtime error: control flow integrity check for type 'A' failed during cast to unrelated type
    // CFI-DIAG-CAST-NEXT: note: vtable is of type '{{(struct )?}}C'
    a = (FA*)pc;
  } else {
    // Invisible to CFI. Test virtual call later.
    memcpy(&a, &pc, sizeof(a));
  }

  fprintf(stderr, "=3=\n");

  // CFI-DIAG-CALL: runtime error: control flow integrity check for type 'A'
  // failed during virtual call CFI-DIAG-CALL-NEXT: note: vtable is of type
  // '{{(struct )?}}C'
  a->f(); // UB here

  fprintf(stderr, "=4=\n");
}

int main(int argc, char *argv[]) {
  testing::InitGoogleMock(&argc, argv);
  // InitGoogleMock() removes all Google Mock & Google Test flag args.
  // So we make the remaining ones globally available to all test cases
  g_argc = argc;
  g_argv = argv;
  return RUN_ALL_TESTS();
}
#endif
