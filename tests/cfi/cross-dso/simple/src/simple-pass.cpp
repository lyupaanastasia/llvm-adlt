#include <stdio.h>
#include <string.h>

#if !defined(SHARED_LIB1) && !defined(SHARED_LIB2)
#include "gmock/gmock.h"
#include "gtest/gtest.h"
#endif

struct PA {
  virtual void f();
};

PA *create_PB();
PA *create_PC();

int icall1();
int icall2();

#ifdef SHARED_LIB1

#include "utils.h"
struct PB : public PA {
  virtual void f();
};
void PB::f() {}

PA *create_PB() {
  create_derivers<PB>();
  return new PB();
}

int icall1() {
  return 1;
}

#elif defined SHARED_LIB2

#include "utils.h"
struct PC : public PA {
  virtual void f();
};
void PC::f() {}

PA *create_PC() {
  create_derivers<PC>();
  return new PC();
}

int icall2() {
  return 0;
}
#else

void PA::f() {}

TEST(CFICrossDsoSimplePassTests, TestVPtr) {
  fprintf(stderr, "cross-dso simple pass example vptr\n");

  PA *b = create_PB();
  PA *c = create_PC();

  fprintf(stderr, "=1=\n");
  b->f(); // OK
  fprintf(stderr, "=2=\n");
  c->f(); // OK
  fprintf(stderr, "=3=\n");
}

TEST(CFICrossDsoSimplePassTests, TestICall) {
  fprintf(stderr, "cross-dso simple pass example icall\n");

  using func_t = int (*)();

  func_t arr[] = {icall1, icall2};
  int idx = 0;

  fprintf(stderr, "=1=\n");
  idx = arr[idx](); // OK, call icall1
  fprintf(stderr, "=2=\n");
  idx = arr[idx](); // OK, call icall2
  fprintf(stderr, "=3=\n");
  idx = arr[idx](); // OK, call icall1
  fprintf(stderr, "=4=\n");
}

int main(int argc, char *argv[]) {
  testing::InitGoogleMock(&argc, argv);
  return RUN_ALL_TESTS();
}
#endif
