#include <dlfcn.h>

#include "gmock/gmock.h"
#include "gtest/gtest.h"

struct base_t;
void do_f(base_t *ptr);

TEST(CFICrossDsoDtorTests, MainTest) {
  using create_t = void (*)();
  auto *lib = dlopen("libcrossdsodtorTests_libuse.so", RTLD_NOW);
  auto *fn = (create_t)dlsym(lib, "create");
  fn();
  // check that calls to `dlclose`ing libuse from libbase still work
  dlclose(lib);
  // check that calls to libbase NDSO still work after unloading libuse NDSO
  using do_f_t = void(*)(base_t *);
  do_f_t ptr = do_f;
  (*ptr)(nullptr);
}

int main(int argc, char **argv) {
  testing::InitGoogleMock(&argc, argv);
  return RUN_ALL_TESTS();
}
