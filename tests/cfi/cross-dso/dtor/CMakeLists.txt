initProject(PROJECT_NAME crossdsodtorTests
    EXECUTABLES cfidtortest
    LIBS libbase libuse)

add_compile_options(-fsanitize=cfi -fsanitize-cfi-cross-dso -flto -fvisibility=default -fno-sanitize-trap=cfi -fsanitize-recover=cfi)
add_link_options(-lclang_rt.ubsan_standalone)

add_library(${libbase} SHARED src/libbase.cpp)
add_library(${libuse} SHARED src/libuse.cpp)
target_link_libraries(${libuse} PUBLIC ${libbase})

add_custom_target(
	${ADLT_PROJECT_LIB}  ALL
	DEPENDS ${libbase} ${libuse}
	COMMAND python3 ${toolsDir}/build_adlt_lib.py
            -i ${ADLT_LIB_CONTENTS}
	    -od ${CMAKE_CURRENT_BINARY_DIR}
            -o "${ADLT_PROJECT_LIB}"
    )

add_adlt_test(TARGET ${cfidtortest} SOURCES src/cfi_dtor_test.cpp LIBS ${libbase} ADLT_EXTRA_LIBS ${libuse})
