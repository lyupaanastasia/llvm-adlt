#!/bin/bash

# ARGS: qemu_with_args, rpt_opts, preconds, bin, test_opts
LINK_TARGETS="libadlt_other_fs.so libother_fs_test.so"

echo Running test on original files
eval $*

echo Running test on moved links

### Setup
D=$(dirname $(readlink lib/libother_fs_test.so))

# go to dir with libraries
pushd $D >& /dev/null

# Mount ramfs on ./mnt
mkdir -p mnt
sudo mount -t tmpfs -o size=1024m - mnt

# Move libraries to mnt, set links to them
mv ${LINK_TARGETS}  mnt
for i in mnt/* ; do
    ln -s $i
done

### Run
# Go to original lib and run the test
popd >&/dev/null
eval $*

### Teardown
pushd $D >& /dev/null
rm -f ${LINK_TARGETS}
mv mnt/* .
sudo umount  mnt
popd >&/dev/null

exit 0
