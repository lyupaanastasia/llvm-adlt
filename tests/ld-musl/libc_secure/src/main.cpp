#include "gmock/gmock.h"
#include "gtest/gtest.h"
#include <stdio.h>
#include "test.h"

int g_argc; // Making argc, argv global
char **g_argv;

TEST(LdMuslLibcSecureTest, LibcSecureMain) {
  dprintf(1, "binary %s\n", g_argv[0]);
  EXPECT_EQ(getvar(), VAL);
}

int main(int argc, char **argv, char **envp) {
  testing::InitGoogleMock(&argc, argv);
  g_argc = argc;
  g_argv = argv;
  return RUN_ALL_TESTS();
}
