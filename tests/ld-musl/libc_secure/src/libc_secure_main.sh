#!/bin/bash

# ARGS: qemu_with_args, rpt_opts, preconds, bin, test_opts
LINK_TARGETS="liblibc_secure_test.so libadlt_libc_secure.so"

# Make binary SGID - enough to raise libc.secure
sudo chgrp 0 libc_secure_main
sudo chmod 2755 libc_secure_main

echo Running test on original files
eval $*

EXITVAL1=$?

echo 'Running test on moved files (no links but LD_LIBRARY_PATH set)'

### Setup
# get abspath of lib/
D=$(cd $(dirname lib/liblibc_secure_test.so); pwd)


# go to dir with libraries
pushd $D >& /dev/null

# Move libraries to /tmp.
# DO NOT SET links to them - we need to check LD_LIBRARY_PATH
# Use loop instead of 1 mv - orig and adlt have different filesets
for i in ${LINK_TARGETS}; do
    if [ -f $i ]; then
	mv $i /tmp
    fi
done

export LD_LIBRARY_PATH=/tmp
### Run
# Go to original lib and run the test
popd >&/dev/null
echo $* && eval $*

EXITVAL2=$?

### Teardown
(
    cd /tmp
    # Use loop instead of 1 mv - orig and adlt have different filesets
    for i in ${LINK_TARGETS}; do
	if [ -f $i ]; then
	    mv $i $D
    fi
done
)

if [[ EXITVAL1 -eq 0 && EXITVAL2 -eq 127 ]]; then
    exit 0
else
    echo "Bad exit vals: expected 0 127, got $EXITVAL1 $EXITVAL2" 1>& 2
    exit 1
fi
