initProject(PROJECT_NAME dlopen
  EXECUTABLES test_direct test_dlopen test_doublemap LIBS foo)

add_library(${foo} SHARED src/foo.c)
set(HELPERS src/sampledata.c src/errexit.c)

add_custom_target(
	${ADLT_PROJECT_LIB}  ALL
	DEPENDS ${foo}
	COMMAND python3 ${toolsDir}/build_adlt_lib.py
            -i ${ADLT_LIB_CONTENTS}
	    -od ${CMAKE_CURRENT_BINARY_DIR}
            -o "${ADLT_PROJECT_LIB}"
    )



add_adlt_test(TARGET ${test_direct} SOURCES src/test_direct.cpp ${HELPERS} LIBS ${foo})

add_adlt_test(TARGET ${test_dlopen} SOURCES src/test_dlopen.cpp ${HELPERS})

add_adlt_test(TARGET ${test_doublemap} SOURCES src/test_doublemap.cpp ${HELPERS} LIBS ${foo})
