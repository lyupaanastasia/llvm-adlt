#include "gmock/gmock.h"
#include "gtest/gtest.h"
#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include "constant.h"

extern "C" {
    #include "errexit.h"
    #include "sampledata.h"
    #include "foo.h"
}

TEST(LdMuslDlOpenDirestTests, Test0) {    
    EXPECT_EQ(CONSTANT, get_constant());
    set_buf(get_sample_data(), get_sample_data_len() );
    size_t len = get_buflen();
    EXPECT_EQ(len, get_sample_data_len());
    EXPECT_EQ(0, cmp_buf(get_sample_data()));
}

int main(int argc, char **argv)
{
    SET_VERBOSE_EXPECT_MAYBE();
    testing::InitGoogleMock(&argc, argv);
    return RUN_ALL_TESTS();
}

