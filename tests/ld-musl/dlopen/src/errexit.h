#pragma once
#include <stdlib.h>
#define expect(cond) errexit_if_false(__FILE__, __LINE__, #cond, cond)
void errexit_if_false(const char *file, int lineno, const char *scond, int cond);
#define SET_VERBOSE_EXPECT() 	setenv("VERBOSE_EXPECT", "1", 1)
#define VERBOSE_EXPECT_REQUESTED() NULL != getenv("VERBOSE_EXPECT")

#define SET_VERBOSE_EXPECT_MAYBE()    \
    if (argc>1 && argv[1][0] == '-' && argv[1][1] == 'v') { \
	argc--;           \
	argv++;           \
	SET_VERBOSE_EXPECT(); \
    }
