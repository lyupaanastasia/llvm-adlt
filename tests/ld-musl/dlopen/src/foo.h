#include <stddef.h>

int get_constant();
void free_buf_maybe();
void set_buf(void *buf, size_t len);
size_t get_buflen();
void *get_bufptr();
int cmp_buf(void *caller_buf);

