#include <stdlib.h>
#include <string.h>
#include "constant.h"
#include "foo.h"


static void *buf=0;
static size_t buflen=0;

int get_constant() { return CONSTANT; }

void free_buf_maybe()
{
    if (buf)
	{
	    free(buf);
	    buflen = 0;
	}
}

void set_buf(void *p, size_t len)
{
    free_buf_maybe();
    if (0 != (buf = malloc(len)))
	{
	    buflen = len;
	    memcpy(buf, p, len);
	}
}

size_t get_buflen()
{
    return buflen;
}

void *get_bufptr()
{
    return buf;
}

int cmp_buf(void *caller_buf)
{
    return memcmp(buf, caller_buf, buflen);
}


