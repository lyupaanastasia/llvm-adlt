#include "gmock/gmock.h"
#include "gtest/gtest.h"
#include <dlfcn.h>
#include <stdlib.h>
#include <stdio.h>
#include "constant.h"

extern "C" {
    #include "errexit.h"
    #include "sampledata.h"
    #include "foo.h"
}

int (*s_get_constant)()=0;
size_t (*s_get_buflen)()=0;
void (*s_set_buf)(void*, size_t)=0;
int (*s_cmp_buf)(void*)=0;

int g_argc; // Making arg and arv global to access within TESTs
char ** g_argv;

#define RESOLVE(handle, var, symbol)		     \
    *(void**)(&var) = dlsym(handle, #symbol); \
    if (0 == symbol)                       \
    { \
	fprintf(stderr, "dlsym() failed for %s with %s\n", #symbol, dlerror()); \
	exit(EXIT_FAILURE); \
    }

TEST(LdMuslDlOpenDoubleMapTests, Test0) {
    void *lib;
    const char *libname;
    
    libname = g_argc>1 ? g_argv[1] : def_libname;
    
    /* Check direct mapping */
    EXPECT_EQ(CONSTANT, get_constant());
    set_buf(get_sample_data(), get_sample_data_len());
    EXPECT_EQ(get_buflen(), get_sample_data_len());
    EXPECT_EQ(0, cmp_buf(get_sample_data()));


    /* Resolve and use same symbols but via explicit dlopen */
    lib = dlopen(libname, RTLD_LAZY);
    if (0 == lib)
	{
	    fprintf(stderr, "dlopen() failed for %s with %s\n",
		    libname, dlerror());
	    exit(EXIT_FAILURE);
	}
    RESOLVE(lib, s_get_constant, get_constant);
    RESOLVE(lib, s_set_buf, set_buf);
    RESOLVE(lib, s_get_buflen, get_buflen);
    RESOLVE(lib, s_cmp_buf, cmp_buf);
    EXPECT_EQ(CONSTANT, s_get_constant());
    s_set_buf(get_sample_data(), get_sample_data_len());
    EXPECT_EQ(s_get_buflen(), get_sample_data_len());
    EXPECT_EQ(0, s_cmp_buf(get_sample_data()));
    EXPECT_TRUE(cmp_buf == s_cmp_buf);
    dlclose(lib);
}

int main(int argc, char **argv)
{    
    SET_VERBOSE_EXPECT_MAYBE();
    testing::InitGoogleMock(&argc, argv);
    // InitGoogleMock() removes all Google Mock & Google Test flag args.
    // So we make the remaining ones globally available to all test cases
    g_argc = argc;
    g_argv = argv;
    return RUN_ALL_TESTS();
}

