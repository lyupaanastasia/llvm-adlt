#include "gmock/gmock.h"
#include "gtest/gtest.h"
#include <dlfcn.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include "constant.h"

extern "C" {
    #include "sampledata.h"
    #include "errexit.h"
}

int (*get_constant)()=0;
size_t (*get_buflen)()=0;
void (*set_buf)(void*, size_t)=0;
int (*cmp_buf)(void*)=0;

int g_argc; // Making arg and arv global to access within TESTs
char ** g_argv;

#define RESOLVE(handle, symbol)			 \
    *(void**)(&symbol) = dlsym(handle, #symbol); \
    if (0 == symbol)                       \
    { \
	fprintf(stderr, "dlsym() failed for %s with %s\n", #symbol, dlerror()); \
	exit(EXIT_FAILURE); \
    }

TEST(LdMuslDlOpenTests, Test0) {
    const char *libname;
    void *lib;
    
    libname = g_argc>1 ? g_argv[1] : def_libname;
    if (g_argc > 1) {
	int ret;
	EXPECT_EQ(chdir("lib"), 0);
	ret = symlink(def_libname, libname);
	if (ret && EEXIST != errno) {
	    dprintf(2, "could not create symlink %s -> %s, %s \n",
		    libname, def_libname, strerror(errno));
	}
	else {
	    dprintf(1, "USING LINK  %s -> %s\n",
		    libname, def_libname);
	}
	EXPECT_EQ(chdir(".."), 0);
    }
    
    lib = dlopen(libname, RTLD_LAZY);
    if (0 == lib) {
	fprintf(stderr, "dlopen() failed for %s with %s\n",
		libname, dlerror());
	exit(EXIT_FAILURE);
    }
    RESOLVE(lib, get_constant);
    RESOLVE(lib, set_buf);
    RESOLVE(lib, get_buflen);
    RESOLVE(lib, cmp_buf);
    EXPECT_EQ(CONSTANT, get_constant());
    set_buf(get_sample_data(), get_sample_data_len());
    size_t len = get_buflen();
    EXPECT_EQ(len, get_sample_data_len());
    EXPECT_EQ(0, cmp_buf(get_sample_data()));
    dlclose(lib);    
}

int main(int argc, char **argv)
{    
    SET_VERBOSE_EXPECT_MAYBE();
    testing::InitGoogleMock(&argc, argv);
    // InitGoogleMock() removes all Google Mock & Google Test flag args.
    // So we make the remaining ones globally available to all test cases
    g_argc = argc;
    g_argv = argv;
    return RUN_ALL_TESTS();
}

