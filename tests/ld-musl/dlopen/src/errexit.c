#include <stdlib.h>
#include <stdio.h>
#include "errexit.h"

void errexit_if_false(const char *file, int lineno, const char *scond, int condition) {
    if (!condition) {
	fprintf(stderr, "%s:%d: false condition '%s'\n",
		file, lineno, scond);
	exit(EXIT_FAILURE);
    }
    else if (VERBOSE_EXPECT_REQUESTED()) {
	fprintf(stderr, "%s:%d: true condition '%s'\n",
		file, lineno, scond);
    }	
}
