#pragma once
#include <ctype.h>

size_t *get_sample_data();
size_t get_sample_data_len();
