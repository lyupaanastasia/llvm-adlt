#include <stdlib.h>
#include "sampledata.h"

static const size_t DATALEN = 1024;

size_t get_sample_data_len() { return DATALEN; }
size_t *get_sample_data() {
    static char *p = 0;
    if (!p) {
	p = malloc(get_sample_data_len());
	for (int i=0; i<get_sample_data_len(); i++) {
	    p[i] = i&0xff;
	}
    }
    return (size_t*)p;
}
   
