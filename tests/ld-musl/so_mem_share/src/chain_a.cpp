// those are from libcommon
#include "gmock/gmock.h"
#include "gtest/gtest.h"
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

extern "C" {
#include "a.h"
#include "common.h"
}

#define PROG "so_mem_share_chain_a"
#define EXECPROG "./so_mem_share_chain_b"

int g_argc; // Making arg and arv global to access within TESTs
char **g_argv;
char **g_envp;

TEST(LdMuslSoMemShareTests, TestChainA) {
  int testno = g_argc > 1 ? atoi(g_argv[1]) : 0;
  pid_t child;

  switch (testno) {
  case 0:
    // smoke - standalone app
    fprintf(stderr, "Test 0 - standalone app\n");
    printvar_a(PROG);
    ASSERT_TRUE("standalone" && (getvar() == V_OK));
    break;
  case 1:
    // fork
    fprintf(stderr, "Test 1 - fork test\n");
    printvar_a(PROG);
    ASSERT_TRUE("before fork" && (getvar() == V_OK));
    child = fork();
    ASSERT_TRUE("fork failed" && (child >= 0));
    if (child) {
      int status;
      waitpid(child, &status, 0);
      printvar_a(PROG "parent");
      ASSERT_TRUE("forked (parent)" && (getvar() == V_OK));
      ASSERT_EQ(WEXITSTATUS(status), 0);
    } else {
      printvar_a(PROG "forked child");
      ASSERT_TRUE("forked (child)" &&  (getvar() == V_OK));
    }
    break;
  case 2:
    // fork/exec
    fprintf(stderr, "Test 2 - fork/exec test\n");
    printvar_a(PROG);
    ASSERT_TRUE("before fork/exec" && (getvar() == V_OK));
    child = fork();
    ASSERT_TRUE("fork failed" && (child >= 0));
    if (child) {
      int status;
      waitpid(child, &status, 0);
      printvar_a(PROG " (parent)");
      ASSERT_TRUE("forked (parent)" && (getvar() == V_OK));
      sleep(1);
      ASSERT_EQ(WEXITSTATUS(status), 0);
    } else {
      char *argv_b[] = {(char*)EXECPROG, 0};
      int ret;
      fprintf(stderr, "forked child - trying to exec\n");
      ret = execve(EXECPROG, argv_b, g_envp);
      if (ret) {
        perror("execve error");
      } else {
        fprintf(stderr, "ret 0 but exited from execve\n");
      }
      ASSERT_TRUE("exec failed" && 0);
    }
    break;
  default:
    fprintf(stderr, "%s \'%s\'\n", PROG ": unknown test case", g_argv[1]);
  }
}

int main(int argc, char **argv, char **envp) {
  testing::InitGoogleMock(&argc, argv);
  // InitGoogleMock() removes all Google Mock & Google Test flag args.
  // So we make the remaining ones globally available to all test cases
  g_argc = argc;
  g_argv = argv;
  g_envp = envp;
  return RUN_ALL_TESTS();
}
