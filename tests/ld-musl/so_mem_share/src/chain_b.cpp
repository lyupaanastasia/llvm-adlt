// those are from libcommon
#include "gmock/gmock.h"
#include "gtest/gtest.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

extern "C" {
#include "b.h"
#include "common.h"
}

#define PROG "chain_b"

TEST(LdMuslSoMemShareTests, TestChainB) {
  printvar_b(PROG);
  sleep(1);
  ASSERT_EQ(getvar(), V_OK);
}

int main(int argc, char **argv) {
  testing::InitGoogleMock(&argc, argv);
  return RUN_ALL_TESTS();
}
