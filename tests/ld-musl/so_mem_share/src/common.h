#pragma once

#define msg(x)  do { printf x ; fflush(stdout); } while (0)

#define V_PREINIT 1
#define V_ADD 1
#define V_OK  (V_PREINIT + V_ADD)

void mkinit();
void mkfin();
void printvar(const char *caller, const char *tag);
int getvar();
