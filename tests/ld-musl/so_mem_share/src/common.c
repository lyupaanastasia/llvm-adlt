#include <stdio.h>

#include "common.h"

static int  theVar = V_PREINIT;

int getvar() { return theVar; }

void printvar(const char *caller, const char *tag) {
    const char *defcaller="common";
    const char *deftag="implicit";
    if (!caller || !*caller) {
	caller = defcaller;
	tag = deftag;
    }
    msg(("printvar (%s:%s): %d\n", caller, tag, theVar));
}

 __attribute__((constructor)) void mkinit()
{
    theVar += V_ADD; 
}

 __attribute__((destructor)) void mkfin()
{
    theVar -= V_ADD; 
}
