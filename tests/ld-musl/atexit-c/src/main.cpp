#include "gtest/gtest.h"
#include <cstdio>
#include <cstdlib>

#include "liba.h"
#include "libb.h"

static void atexit_main1() { dprintf(2, "main1."); }

static void atexit_main2() { dprintf(2, "main2."); }

void atexit_main() {
  atexit(atexit_main1);
  atexit(atexit_a1);
  atexit(atexit_b1);
  atexit(atexit_a2);
  atexit(atexit_b2);
  atexit(atexit_main2);
  exit(0);
}

TEST(atexitMuslCTests, TestExitMultilib) {
  ASSERT_EXIT(atexit_main(), testing::ExitedWithCode(0),
              "main2.b2.a2.b1.a1.main1.");
}

int main(int argc, char **argv) {
  testing::InitGoogleTest(&argc, argv);
  (void)RUN_ALL_TESTS();
  return 0;
}
