#include "gmock/gmock.h"
#include "gtest/gtest.h"
#include <stdio.h>

int g_argc; // Making argc, argv global
char **g_argv;

TEST(LdMuslUnusedLibTests, UnusedLibMain) {
    fprintf(stdout, "binary %s\n", g_argv[0]);
}

int main(int argc, char **argv, char **envp) {
  testing::InitGoogleMock(&argc, argv);
  g_argc = argc;
  g_argv = argv;
  return RUN_ALL_TESTS();
}
