#!/bin/bash

# Must be sourced
if ! ( return 0  >& /dev/null ) ; then
    echo "The script expected to be sourced"
    return 1
fi

if [[ x"$1" == x ]]; then
    echo "Usage: . ${BASH_SOURCE[0]} path_to_sdk_prebuilt" 2>& 1
    return 1
fi
export SDK_PARTLY="$1"
