#include <stdio.h>
#include <stdlib.h>
#include <libxml/parser.h>

void print_xml_version(const char *filename) {
    xmlDoc *document = xmlReadFile(filename, NULL, 0);
    if (document == NULL) {
        dprintf(2, "Error loading file %s\n", filename);
        return;
    } else {
        dprintf(2, "readfile passed\n");
    }

    xmlNode *root_element = xmlDocGetRootElement(document);
    if (root_element != NULL) {
        printf("XML version: %s\n", root_element->doc->version);
    } else {
        printf("Root element not found.\n");
    }

    xmlFreeDoc(document);
    dprintf(2, "freedoc passed\n");
    xmlCleanupParser();
    dprintf(2, "cleanup passed\n");
}

int main(int argc, char **argv) {
    if (argc != 2) {
        fprintf(stderr, "Usage: %s <file_name>\n", argv[0]);
        return EXIT_FAILURE;
    }

    print_xml_version(argv[1]);
    return EXIT_SUCCESS;
}
