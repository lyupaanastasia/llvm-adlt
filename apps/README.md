# Running xml-parser

1. Install the llvm toolchain
2. Install qemu-aarch64-static 
3. Install custom sysroot pre-built includes and libs
   - Download appspawn libs package. Unpack it at _xml-parser/data/usr/lib/aarch64-linux-ohos_ directory
     - libxml.z.so
     - libxml2.z.so
   - Install _libxml2-dev_ package and copy header files to the project
     - /usr/include/libxml2/libxml -> xml-parser/data/usr/include/libxml
     - dependency /usr/include/unicode -> xml-parser/data/usr/include/unicode
 4. Launch build_script.sh  
   - "Usage: $(basename $0) 'SDK_PATH' 'CLANG_PATH' 'QEMU_BIN_PATH'
     - SDK_PATH - the path where llvm toolchain (including clang) is located
     - CLANG_PATH - the full path to clang 
     - QEMU_BIN_PATH - the path to qemu-aarch64-static