#!/bin/bash

if [[ $# -ne 3 ]]; then 
    echo "Usage: $(basename $0) 'SDK_PATH' 'CLANG_PATH' 'QEMU_BIN_PATH' args" 1>& 2
    exit 1
fi

SCRIPT_DIR=$(cd $(dirname $0); pwd)
export LLVM_ADLT_HOME=$(cd ${SCRIPT_DIR}/..; pwd)

rm -rf ${LLVM_ADLT_HOME}/apps/xml-parser/out/*

export SDK_ROOT=$1
export SDK_PREBUILT=${SDK_ROOT}
export LLVM=$2
QEMU_BIN_PATH=$3

cmake -S xml-parser \
      -B xml-parser/out/build \
      -DCMAKE_INSTALL_PREFIX=xml-parser/out \
      -DCMAKE_TOOLCHAIN_FILE=${LLVM_ADLT_HOME}/tools/cmake/aarch64.cmake \
      -DCMAKE_BUILD_TYPE=Release
echo END CONFIG

cmake --build xml-parser/out/build --target install -v
echo END BUILD

cd ${LLVM_ADLT_HOME}/apps/xml-parser/out/qemu/orig && \
${QEMU_BIN_PATH}/qemu-aarch64-static -L "${PWD}" XMLParser_main "${LLVM_ADLT_HOME}/apps/xml-parser/src/note.xml"

cd ${LLVM_ADLT_HOME}/apps/xml-parser/out/qemu/adlt && \
${QEMU_BIN_PATH}/qemu-aarch64-static -L "${PWD}" XMLParser_main "${LLVM_ADLT_HOME}/apps/xml-parser/src/note.xml"

echo END RUN
