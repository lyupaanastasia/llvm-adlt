#!/bin/bash
help() {
cat 1>& 2 <<EOF
Example - Using gdbserver.sh to launch test in vscode

1. Prepare project in vscode (remote ssh, folders must include at least llvm-adlt)
2. Build tests 
   $ main.py -cbd all
3. Launch vscode
4. launch gdbserver.sh with test binary (relative to out/qemu) 
and with args if necessary. For example
    $ ./gdbserver.sh ld-musl/dlopen/adlt/test_dlopen libfoo.so
5. Press <F5> in vscode

Explanation:
	The gdbserver.sh builds the file .vscode/launch.json, inserting the binary 
with args into it and after that launches the gdbserver. For example, after the above 
call of gdbserver.sh, the .vscode/launch.json will contain the following lines

     ...
     "request": "launch",
     "program": "${workspaceFolder}/out/qemu/ld-musl/dlopen/adlt/dlopen_test_dlopen"
     "cwd": "${workspaceFolder}/out/qemu/ld-musl/dlopen/adlt"
     "miDebuggerServerAddress": ":8081",
     ...

and then the gdvserver (bult in qemu-static) will be launched, note argument at the end of line
    `qemu-aarch64-static -g 8081 -L . -E -LD_LIBRARY_PATH=. dlopen_test_dlopen libfoo.so`
EOF
}
errexit() {
    echo $* 1>& 2
    help
    exit 1
}

if [[ -z "$*" ]]; then
    errexit "Usage: $(basename $0) 'program relative to out/qemu' args"
fi

(
    
    REL=$(dirname $1)
    PROG=$(basename $1)
    # Leave prog args in $*
    shift

    cd $(dirname $0)
    pushd .vscode
    cat stub.head >launch.json
    echo '"program": "${workspaceFolder}/out/qemu/'${REL}/${PROG}'"' >>launch.json
    echo '"cwd": "${workspaceFolder}/out/qemu/'${REL}'"' >> launch.json
    cat stub.tail >>launch.json
    popd
    cd out/qemu/$REL
    qemu-aarch64-static -g 8081 -L . -E -LD_LIBRARY_PATH=. $PROG $*
)
