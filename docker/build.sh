#!/bin/bash

export IMAGE="adlt-builder"
export volumes=${PWD}/volumes
export user=$(id -urn)
export uid=$(id -ur)
export group=$(id -rgn)
export gid=$(id -rg)
# --- End of customisable section

docker build --network=host \
       --build-arg user=${user} \
       --build-arg uid=${uid} \
       --build-arg group=${group} \
       --build-arg gid=${gid} \
       -t ${IMAGE} .
if [[ x$? != x0 ]] ; then
    echo Build failed 1>&2
    exit
fi

mkdir -p ${volumes}/home/${user} ${volumes}/work/llvm-adlt ${volumes}/work/sdk
cat <<EOF > docker-compose.yml
version: "3.8"
services:
  ${IMAGE}:
    extra_hosts:
      - ${IMAGE}:127.0.0.1
    image: ${IMAGE}
    privileged: true
    network_mode: host
    container_name: ${IMAGE}
    hostname: ${IMAGE}
    restart: "no"
    volumes:
      - ${volumes}/home/${user}:/home/${user}
      - ${volumes}/work:/import/work
EOF

export wd=${PWD}
cat <<EOF

FYI: To run, try ( cd ${PWD} ; docker compose run -it --rm ${IMAGE} )
You can also add to your .bashrc the function
${IMAGE} ()
{
    pushd ${PWD} >/dev/null
    docker compose run -it --rm ${IMAGE}
    popd >/dev/null
}

EOF
