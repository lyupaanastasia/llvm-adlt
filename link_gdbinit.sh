#!/bin/bash

usage() {
cat <<EOF 1>& 2
Usage: 
       $(basename $0) path/to/test
       That will link .gdbinit in dest dir to ./gdbinit.path.to.test
EOF
}

# If file $1 does not exist, create obe with 'break main'
mkinit_maybe() {
    if [[ ! -s $1 ]]; then
	echo 'break main' > $1
    fi
}

if (( $# != 1)) || [[ ! -d out/qemu/$1 ]] ; then
    usage;
    exit 1
fi

SUFFIX=$(echo $1|sed -e 's?/?.?g' -e  's/\.$//g'  )
echo $SUFFIX
mkinit_maybe gdbinit.${SUFFIX}
ln -s $(pwd)/gdbinit.${SUFFIX} out/qemu/$1/.gdbinit
echo passed
