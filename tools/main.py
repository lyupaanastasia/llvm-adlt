#!/usr/bin/python3
import argparse
import os
import shutil
from pathlib import Path
from typing import Iterator, List, Tuple

import util
from build_musl import buildMusl, buildOrigMusl
from test_utils import TestData, TestInfo
from util import logLevel

from config import TEST_LIST, Config
from tools import Tests, LLDTests, LitTests, Tools, shell

import build_report


class ConfigError(RuntimeError):
    pass


def makeTestParams(status):
    test_params = []
    match status:
        case "only-benchmarks":
            expectedTestState = "BENCHMARK"
        case "only-fail":
            expectedTestState = "FAILED"
        case _:
            expectedTestState = "PASSED"
    for key, val in TEST_LIST.items():
        testList = [test for test in val["tests"] if test.state == expectedTestState]
        if len(testList) > 0:
            test_params.append((key, testList))
    return test_params


def getTestsByStatus(status):
    testPaths = set()
    match status:
        case "only-benchmarks":
            expectedTestState = "BENCHMARK"
        case "only-fail":
            expectedTestState = "FAILED"
        case _:
            expectedTestState = "PASSED"
    for key, val in TEST_LIST.items():
        testList = [test for test in val["tests"] if test.state == expectedTestState]
        if len(testList) > 0:
            testPaths.add("/".join(key.split("/")[:-1]))
    return testPaths


def MoveCompilerRt(ret=False):
    lib = "lib/clang/15.0.4/lib/aarch64-linux-ohos"
    src = Config.LLVM / lib
    dst = Config.SDK_PARTLY / lib
    if not ret:
        if not Path(f"{dst}_tmp").exists():
            shutil.move(dst, f"{dst}_tmp")
            shell(f"ln -sf {src} {dst}")
    else:
        if Path(f"{dst}_tmp").exists():
            os.unlink(dst)
            shutil.move(f"{dst}_tmp", dst)


def buildInstallTarget(target, silentInstall=True):
    if Config.SDK_PREBUILT != None:
        return
    outDir = Config.SDK_ROOT / "out" / "llvm_make"
    installDir = Config.SDK_ROOT / "out" / "llvm-install"
    # ignore crt, libcxx related warnings at install
    installOut = Path("/dev") / "null" if silentInstall else None
    shell(f"cmake --build {outDir} --target {target}")
    if Config.SDK_PARTLY != None:
        shell(
            f"cp -rfu --preserve=all {outDir} -T {installDir}",
            out=installOut,
            err=installOut,
        )
    else:
        shell(
            f"cmake --build {outDir} --target install", out=installOut, err=installOut
        )


def buildLLD(silentInstall=True):
    buildInstallTarget("lld", silentInstall)


def buildReadElf(silentInstall=True):
    buildInstallTarget("llvm-readelf", silentInstall)


def buildStrip(silentInstall=True):
    buildInstallTarget("llvm-strip", silentInstall)


def testWithBinaryName(string):

    if string in [
        "all",
        "orig",
        "adlt",
        "only-ok",
        "only-fail",
        "only-benchmarks",
        "lld-tests",
        "lit-tests",
    ]:
        return string

    strList = string.split(",")
    testList = []
    for string in strList:
        testName = ""
        testBinaries = []
        if string.find(":"):
            tempList = string.split(":")
            if len(tempList) > 1:
                testName = tempList[0]
                testBinaries = tempList[1:]
            else:
                testName = string
        else:
            testName = string
        testList.append((testName, testBinaries))
    return testList


def expandTestSpec(spec: Tuple[str, List[str]]) -> Iterator[Tuple[str, List[TestInfo]]]:
    name, binaries = spec

    if name.endswith("/"):
        name = name[:-1]

    if not (name.endswith("/orig") or name.endswith("/adlt")):
        yield from expandTestSpec((name + "/orig", binaries))
        yield from expandTestSpec((name + "/adlt", binaries))
        return

    if name not in TEST_LIST:
        raise ConfigError(f"Unknown test name: {name}")

    allTests: List[TestInfo] = TEST_LIST[name]["tests"]
    if not binaries:
        yield (name, allTests)
        return
    else:
        bin2test = {test.test_data.binary: test for test in allTests}
        unknownBinaries = set(binaries) - set(bin2test.keys())
        if unknownBinaries:
            raise ConfigError(
                f"Unknown binaries: {unknownBinaries} for {name}, "
                f"known are: {','.join(bin2test.keys())}"
            )
        yield (name, [bin2test[binary] for binary in binaries])


def parseArgs(tests):
    parser = argparse.ArgumentParser(formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument(
        "--debug",
        "-d",
        help="remote debug option. Used in conjunction with 'build' and 'debug' options",
        action="store_true",
        default=False,
    )
    parser.add_argument(
        "--build",
        "-b",
        help="""build demos (mode or test name)
    Available modes:
        all
        lld
        lld-tests
        readelf
        strip
        musl
        orig-musl (excluded from all)
    Available test names (for building several tests, list test names separated by comma):
        """
        + "\n        ".join(Tools().getParents(tests.keys(), "/")),
        default=None,
    )
    parser.add_argument("--buildDebug", "-db", "-bd", help="build demos in debug mode")
    parser.add_argument(
        "--clean",
        "-c",
        help="""clean demos (mode or test name)
    Available modes:
        all
    Available test names (for cleaning several tests, list test names separated by comma):
        """
        + "\n        ".join(Tools().getParents(tests.keys(), "/")),
        default=None,
    )
    parser.add_argument(
        "--cleanBuild", "-cb", "-bc", help="clean and build demo", default=None
    )
    parser.add_argument(
        "--cleanBuildDebug",
        "-cbd",
        "-cdb",
        "-bcd",
        help="clean and build demo in debug mode",
        default=None,
    )
    parser.add_argument(
        "--run",
        "-r",
        help="""run QEMU tests (mode or test name)
    Available modes:
        all
        lld-tests
        orig
        adlt
        only-ok
        only-fail
        only-benchmarks
        {folder with tests, e.g. base}
    Available test names (to running several tests, list test names separated by comma;
    to run only a specific subtest, specify the binary subtest with a colon after the test name,
    use quotes if subtest binary name contains spaces):
        """
        + "\n        ".join(tests.keys()),
        default=None,
        type=testWithBinaryName,
    )
    parser.add_argument(
        "--profile",
        "-p",
        help="""profile (benchmark) QEMU tests (mode or test name).
Use together with -r.
Wants passwordless sudo.
Falls back to "time" if no perf or no sudo access.
Limitation - does not profile scripts.
        """,
        action="store_true",
        required=False,
    )
    parser.add_argument(
        "--count",
        "-C",
        help="""set number of repetitions for -p.
        """,
        required=False,
        type=int,
        default=100,
    )
    parser.add_argument(
        "--withOrigMusl",
        help="run adlt tests with orig and modified musl",
        action="store_true",
        default=False,
    )
    parser.add_argument(
        "--noSilentInstall",
        help="no silent install",
        action="store_true",
        default=False,
    )
    parser.add_argument(
        "--runDebug",
        "-rd",
        "-dr",
        help="run demo in debug mode",
        default=None,
        type=testWithBinaryName,
    )
    parser.add_argument("--trace", help="trace", action="store_true", default=False)
    parser.add_argument(
        "--qemuLogFile",
        help="log QEMU results to file",
        action="store_true",
        default=False,
    )
    parser.add_argument(
        "--logLevel",
        help="set log level (INFO by default).",
        type=util.LogLevel.from_string,
        choices=list(util.LogLevel),
        default=util.LogLevel.INFO,
    )
    parser.add_argument(
        "--reportXML",
        help="generate xml report with results of test run. Used in conjunction with 'run' option",
        action="store_true",
        default=False,
    )
    parser.add_argument(
        "--run-env",
        help="Set environment specific settings from configs/run_env.yaml.",
        default="default",
    )

    args = parser.parse_args()
    if args.cleanBuildDebug:
        args.debug = True
        args.clean = args.cleanBuildDebug
        args.build = args.cleanBuildDebug

    if args.cleanBuild == "all":
        args.clean = "all"
        args.build = "all"
    elif args.cleanBuild:
        args.clean = args.cleanBuild
        args.build = args.cleanBuild

    if args.buildDebug:
        args.debug = True
        args.build = args.buildDebug

    if args.count < 1:
        args.count = 1

    if args.runDebug:
        args.run = args.runDebug
        args.debug = True

    util.logLevel = args.logLevel
    return args


def findTestGroup(toBuild, tests):
    return set(j for i in toBuild for j in tests if i in j.split("/"))


def main():
    args = parseArgs(TEST_LIST)
    testDirs = Tools().getParents(TEST_LIST.keys(), "/")
    util.updateRunEnvironment(Config.RUN_ENV_CONFIG_PATH, args.run_env)

    silentInstall = not args.noSilentInstall
    if args.clean:
        toClean = set(args.clean.split(","))
        if "lld" in toClean:
            toClean.remove("lld")
        if "readelf" in toClean:
            toClean.remove("readelf")
        if "musl" in toClean:
            toClean.remove("musl")
        if "orig-musl" in toClean:
            toClean.remove("orig-musl")
        if "lld-tests" in toClean:
            LLDTests().clean(args.trace)
            toClean.remove("lld-tests")
        if "lit-tests" in toClean:
            LitTests().clean(args.trace)
            toClean.remove("lit-tests")
        shell("rm -rf ./*/*__pycache*")
        if args.clean == "all":
            shell("rm -rf out")
        else:
            Tests().clean(toClean, args.trace)

    tests = set(Tools().getParents(TEST_LIST.keys(), "/"))

    if args.build == "all":
        buildLLD(silentInstall)
        buildReadElf(silentInstall)
        buildStrip(silentInstall)
        buildMusl(silentInstall)
        Tests().configure(args.debug)
        toBuild = tests
        Tests().build(toBuild, testDirs, silentInstall, args.trace)
        LLDTests().build(args.trace)
        # LitTests().configure(args.debug)
    elif args.build:
        toBuild = set(args.build.split(","))
        if "lld" in toBuild:
            buildLLD(silentInstall)
            toBuild.remove("lld")
        if "readelf" in toBuild:
            buildReadElf(silentInstall)
            toBuild.remove("readelf")
        if "strip" in toBuild:
            buildStrip(silentInstall)
            toBuild.remove("strip")
        if "musl" in toBuild:
            buildMusl(silentInstall)
            toBuild.remove("musl")
        if "orig-musl" in toBuild:
            buildOrigMusl(silentInstall)
            toBuild.remove("orig-musl")
        if "only-ok" in toBuild:
            toBuild = getTestsByStatus("only-ok")
        if "only-fail" in toBuild:
            toBuild = getTestsByStatus("only-fail")
        if "only-benchmarks" in toBuild:
            toBuild = getTestsByStatus("only-benchmarks")
        if "lld-tests" in toBuild:
            LLDTests().build(args.trace)
            toBuild.remove("lld-tests")
        if "lit-tests" in toBuild:
            LitTests().configure(args.debug)
            toBuild.remove("lit-tests")
        if toBuild:
            Tests().configure(args.debug)
            groups = set()
            if toBuild not in set(tests):
                groups = findTestGroup(toBuild, tests)
            Tests().build(
                groups if groups else toBuild, testDirs, silentInstall, args.trace
            )

    if args.run:
        profiler = ["", ""]
        if args.profile:
            if 0 == shell("sudo perf stat /bin/true >/dev/null 2>&1", show_error=False):
                profiler[0] = f"sudo perf stat -r {args.count} "
                profiler[1] = " > /dev/null"
            elif 0 == shell(
                "/usr/bin/time /bin/true >/dev/null 2>& 1", show_error=False
            ):
                profiler[0] = (
                    f"/usr/bin/time sh -c 'i={args.count}; while [ $i -ne 0 ]; do i=$(expr $i - 1 ); "
                )
                profiler[1] = "; done' >/dev/null"
            else:
                print("Profiler unavailable. Disabled.")
                exit(1)
        if args.run == "all":
            for key, val in TEST_LIST.items():
                Tests().run(
                    (key, val["tests"]),
                    val,
                    doLogFile=args.qemuLogFile,
                    isDebug=args.debug,
                    profiler=profiler,
                    doTrace=args.trace,
                    withOrigMusl=args.withOrigMusl,
                )
            LLDTests().run(args.trace)
            # LitTests().run(args.trace)
        elif args.run in ["orig", "adlt"]:
            for key, item in TEST_LIST.items():
                if key.split("/")[-1] == args.run:
                    Tests().run(
                        (key, item["tests"]),
                        item,
                        doLogFile=args.qemuLogFile,
                        isDebug=args.debug,
                        profiler=profiler,
                        doTrace=args.trace,
                        withOrigMusl=args.withOrigMusl,
                    )
        elif args.run == "lld-tests":
            LLDTests().run(args.trace)
        elif args.run == "lit-tests":
            LitTests().run(args.trace)
        else:
            if args.run in ["only-ok", "only-fail", "only-benchmarks"]:
                testParams = makeTestParams(args.run)
            else:
                if util.isStandaloneTest(args.run, TEST_LIST):
                    testParams = util.flatten(map(expandTestSpec, args.run))
                else:
                    testParams = util.fetchAllTestsFromBasePath(args.run, TEST_LIST)

            for testSpec in testParams:
                name, tests = testSpec
                testListBins = util.getTestBinsSet(TEST_LIST[name]["tests"])
                testSpecBins = util.getTestBinsSet(tests)
                print(f"testListBins: {testListBins}")
                print(f"testSpecBins: {testSpecBins}")
                if not testSpecBins.issubset(testListBins):
                    raise ConfigError(
                        f"Invalid 'binary name(s)' argument for {testSpec}"
                    )
                Tests().run(
                    testSpec,
                    TEST_LIST[name],
                    doLogFile=args.qemuLogFile,
                    isDebug=args.debug,
                    profiler=profiler,
                    doTrace=args.trace,
                    withOrigMusl=args.withOrigMusl,
                )
        if args.reportXML:
            build_report.main()


if __name__ == "__main__":
    if Config.SDK_PARTLY:
        # move out/compiler-rt to prebuilds and return after main()
        # TODO don't move compiler-rt
        try:
            MoveCompilerRt(ret=False)
            main()
        finally:
            MoveCompilerRt(ret=True)
    else:
        main()
