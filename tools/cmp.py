#!/usr/bin/python3
import os
import argparse
from pathlib import Path
from tools import shell, Tools
from config import Config, TEST_LIST


OBJDUMP = Tools().getLLVMTool("llvm-objdump")
READELF = Tools().getLLVMTool("llvm-readelf")
DIFF = "diff --color "
GREP = "grep "
BASE_OUT = Path("debug_logs")


def diff(greps):
    cmd = DIFF + " ".join(f for f in greps)
    shell(cmd)


def grepItem(pattern, file, out, range="-A 2 -B 2"):
    cmdGrep = GREP + f" \"{pattern}\" {file} {range}"
    shell(cmdGrep, out)


def parseArgs():
    parser = argparse.ArgumentParser()
    add_arg = parser.add_argument
    add_arg("--testName", "-t", help="testName", required=True)
    add_arg("--filePath", "-fp", help="filePath inside test", required=True)
    add_arg("--funcName", "-func", help="funcName", default="")
    add_arg("--symName", "-sym", help="symName", default="")
    add_arg("--secName", "-sec", help="secName", default="")
    add_arg("--relocs", "-rels", help="relocs", action='store_true')
    add_arg("--symbols", "-syms", help="symbols", action='store_true')
    add_arg("--dynamic", "-dyn", help="dyn for syms and rels", action='store_true')
    add_arg("--extended", "-x", help="extended mode for dump", action='store_true')
    add_arg("--full", "-full", help="full content", action='store_true')
    add_arg("--fullDis", "-fullDis", help="full content", action='store_true')
    add_arg("--sectionHeaders", "-sh",
            help="section headers", action='store_true')
    add_arg("--programHeaders", "-ph",
            help="program headers", action='store_true')
    add_arg("--dynamicTable", "-dt",
            help="dynamic table", action='store_true')
    add_arg("--all", "-a", help="all", action='store_true')
    add_arg("--regex", "-r", help="regex", default="")

    args = parser.parse_args()
    testDirs = Tools().getParents(TEST_LIST.keys(), '/')

    targets = [args.funcName, args.symName, args.secName, args.symbols, args.relocs, args.all,
               args.full, args.fullDis, args.sectionHeaders, args.programHeaders, args.dynamicTable]
    if not any(targets):
        print("Nothing to do!")
        exit(0)

    if args.testName not in set(testDirs):
        print("TEST ERROR: Incorrect value of 'testName' argument." + args.testName)
        exit(0)
    return args

def cmpSymCmd(args, item, file):
    if args.extended:
        cmd = f"{READELF} " + ("--dt" if args.dynamic else "-s")
    else:
        cmd = f"{OBJDUMP} " + ("-T" if args.dynamic else "-t")
    cmd += f" {file}"
    pattern = f" | grep {args.symName}"
    return f"{cmd} {pattern}"

def cmpFuncCmd(args, item, file):
    pattern = f"| awk -v RS= \'/^[[:xdigit:]]+ <{args.funcName}>/\'"
    return f"{OBJDUMP} -drS {file} {pattern}"


def cmpFullDisCmd(args, item, file):
    return f"{OBJDUMP} -drS {file}"


def cmpSecCmd(args, item, file):
    adltPostFix = "_" + args.filePath.split("/")[-1].split(".")[0]
    adltSecName = args.secName + adltPostFix \
        if args.secName not in \
        [".rela.dyn", ".rela.plt", ".dynamic",
            ".comment", ".gnu.hash", ".hash",
            ".plt", ".got", ".got.plt",
            ".dynsym", ".dynstr",
            ".symtab", ".shstrtab", ".strtab"] \
        else args.secName
    sec = adltSecName if "adlt" in item else args.secName
    return f"{OBJDUMP} -s -j {sec if sec else args.secName} {file}"


def cmpSymbolsCmd(args, item, file):
    if args.extended:
        cmd = f"{READELF} " + ("--dt" if args.dynamic else "-s")
    else:
        cmd = f"{OBJDUMP} " + ("-T" if args.dynamic else "-t")
    cmd += f" {file}"
    return cmd


def cmpRelocsCmd(args, item, file):
    if args.extended:
        cmd = f"{READELF} " + ("--dyn-relocations" if args.dynamic else "-r")
    else:
        cmd = f"{OBJDUMP} " + ("-R" if args.dynamic else "-r")
    cmd += f" {file}"
    return cmd


def cmpAllCmd(args, item, file):
    if args.extended:
        cmd = f"{READELF} -a"
    else:
        cmd = f"{OBJDUMP} -xsSdRr"
    cmd += f" {file}"
    return cmd

def cmpFullCmd(args, item, file):
    return f"{OBJDUMP} --full-contents {file}"


def cmpSectionHeadersCmd(args, item, file):
    return f"{READELF} --section-headers {file}"


def cmpProgramHeadersCmd(args, item, file):
    return f"{READELF} --program-headers {file}"


def cmpDynamicTableCmd(args, item, file):
    return f"{READELF} --dynamic-table {file}"


def main():
    args = parseArgs()
    def genCmd(x): return None
    out = None
    if args.funcName:
        genCmd = cmpFuncCmd
        out = BASE_OUT / args.funcName
    elif args.symName:
        genCmd = cmpSymCmd
        out = BASE_OUT / args.symName
    elif args.secName:
        genCmd = cmpSecCmd
        out = BASE_OUT / args.secName
    elif args.symbols:
        genCmd = cmpSymbolsCmd
        out = BASE_OUT / ("dyn-symbols" if args.dynamic else "symbols")
    elif args.relocs:
        genCmd = cmpRelocsCmd
        out = BASE_OUT / ("dyn-relocs" if args.dynamic else "relocs")
    elif args.full:
        genCmd = cmpFullCmd
        out = BASE_OUT / "full"
    elif args.fullDis:
        genCmd = cmpFullDisCmd
        out = BASE_OUT / "full-disassembly"
    elif args.sectionHeaders:
        genCmd = cmpSectionHeadersCmd
        out = BASE_OUT / "section-headers"
    elif args.programHeaders:
        genCmd = cmpProgramHeadersCmd
        out = BASE_OUT / "program-headers"
    elif args.dynamicTable:
        genCmd = cmpDynamicTableCmd
        out = BASE_OUT / "dynamic-table"
    elif args.all:
        genCmd = cmpAllCmd
        out = BASE_OUT / "all"

    # gen and print
    testPath = Config.INSTALL_DIR / 'qemu' / args.testName
    dumps, greps = [], []
    isTooBig = any([args.full, args.fullDis, args.all])
    for item in ["orig", "adlt"]:
        # prepare
        file = testPath / item / args.filePath
        dumpOut = out / (f"{item}." + \
            ("readelf" if args.extended else "objdump") + ".asm")
        grepOut = f"{dumpOut}.grep.asm"
        cmd = genCmd(args, item, file)
        print(cmd)
        # generate
        os.makedirs(out, exist_ok=True)
        shell(cmd, dumpOut)
        dumps.append(dumpOut)

        if not isTooBig:
            grepItem(args.regex, dumpOut, grepOut)
            greps.append(grepOut)

    if isTooBig and not args.regex:
        print("Full results in: ", out)
        exit(0)

    diff(greps)


if __name__ == "__main__":
    main()
