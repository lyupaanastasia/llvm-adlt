"""
@file config.py
@brief This script contains settings for testing.
"""

import os
from pathlib import Path

from util import parseTestConfig


class Config:
    """
    Config class.

    Holds useful environment variables and settings.
    """

    SDK_ROOT = Path(os.environ["SDK_ROOT"])
    LLVM = Path(os.environ["LLVM"])
    # contain path to prebuilt part of sdk
    SDK_PARTLY = (
        None if not os.environ.get("SDK_PARTLY") else Path(os.environ.get("SDK_PARTLY"))
    )
    SDK_PREBUILT = os.environ.get("SDK_PREBUILT")
    SYSROOT = (
        SDK_ROOT
        / ("out" if not SDK_PREBUILT else "")
        / "sysroot"
        / "aarch64-linux-ohos"
        / "usr"
    )
    TEST_CONFIG_PATH = Path(__file__).parent / ".." / "configs" / "test_config.yaml"
    RUN_ENV_CONFIG_PATH = Path(__file__).parent / ".." / "configs" / "run_env.yaml"

    ROOT_DIR = Path.cwd()
    INSTALL_DIR = ROOT_DIR / "out"
    BUILD_DIR = ROOT_DIR / "out" / "build"
    MAKE_DIR = SDK_ROOT / "out" / "llvm_make"
    DEBUG_PORT = 8081


TEST_LIST = parseTestConfig(Config.TEST_CONFIG_PATH)
