import importlib.util
import importlib.machinery
import os
import json
import sys
import argparse
import xml.etree.ElementTree as ET
from lxml import etree
from test_utils import TestArtifact
from config import Config

from enum import Enum
from string import Template


class TestReportData(Enum):
    ADLT_REPO_ROOT = str(Config.ROOT_DIR)
    ADLT_TEST_DESC = "tools/main.py"
    REPORT_ROOT = str(Config.INSTALL_DIR)
    SUMMARY_REPORT_FILE = f"{str(Config.INSTALL_DIR)}/test_summary"


def prettify_report(xml_file):
    temp = etree.parse(xml_file)
    new_xml = etree.tostring(temp, pretty_print=True, encoding=str)
    with open(xml_file, "w") as files:
        files.write(new_xml)


class Args:
    def __init__(self):
        self.path = TestReportData.REPORT_ROOT.value
        self.adlt = TestReportData.ADLT_REPO_ROOT.value
        self.report = TestReportData.SUMMARY_REPORT_FILE.value


# flags if there were failed tests. if True, script will exit with cole 1
is_failed = False


def cmdline_args():
    parser = argparse.ArgumentParser(description="Summarize test reports")
    parser.add_argument(
        "-p",
        "--path",
        default=TestReportData.REPORT_ROOT.value,
        type=str,
        help="The root path to test results",
    )
    parser.add_argument(
        "-r",
        "--report",
        default=TestReportData.SUMMARY_REPORT_FILE.value,
        type=str,
        help="The report files path without file extension",
    )
    parser.add_argument(
        "--adlt",
        default=TestReportData.ADLT_REPO_ROOT.value,
        type=str,
        help="The adlt repo with test data",
    )
    args = parser.parse_args(sys.argv[1:])

    return args


def find_json_files(json_root):
    json_files = []
    for root, dirs, files in os.walk(json_root):
        for file in files:
            if file.endswith(TestArtifact.REPORT.value):
                json_files.append(os.path.join(root, file))
    return json_files


def get_adlt_test_info(adlt_root):
    remote_script_path = os.path.abspath(adlt_root + "/tools/main.py")
    remote_path = os.path.abspath(adlt_root + "/tools")
    sys.path.append(remote_path)
    # importlib.invalidate_caches()
    loader = importlib.machinery.SourceFileLoader(
        "llvm_adlt_tools_main", remote_script_path
    )
    llvm_adlt_tools_main = loader.load_module()
    sys.path.pop()
    tests = dict(llvm_adlt_tools_main.TEST_LIST)
    return tests


def get_bins(file, test_suites, json_root):
    print(f"Zero length {file}")
    report_path = os.path.dirname(os.path.relpath(file, json_root + "/qemu"))
    for testsuite_path in test_suites.keys():
        report_path_norm = os.path.normpath(report_path)
        testsuite_path_norm = os.path.normpath(testsuite_path)
        if report_path_norm == testsuite_path_norm:
            bins = set()
            for testInfo in test_suites[testsuite_path]["tests"]:
                bins.add(testInfo.test_data.binary)
            return bins


def get_stdlog_file(std_logfile, json_root):
    if os.path.exists(std_logfile):
        std_logfile = os.path.relpath(std_logfile, json_root)
    else:
        std_logfile = ""
    return std_logfile


def get_err_child_info(childname, childtext):
    errChild = ET.Element(childname)
    errChild.text = childtext
    return errChild


def get_error_dict(bins, stdout_path, stderr_path):
    child_info_dict = {
        "error": "An error has occured",
        "bin": ", ".join(bins),
        "stdout": stdout_path,
        "stderr": stderr_path,
    }
    return child_info_dict


def append_err_info(child_element, bins, reports_abs_path_template, json_root):
    stdout_abs_path = reports_abs_path_template.substitute(
        postfix=TestArtifact.LOG.value
    )
    stderr_abs_path = reports_abs_path_template.substitute(
        postfix=TestArtifact.ERR.value
    )
    stdout_path = get_stdlog_file(os.path.relpath(stdout_abs_path), json_root)
    stderr_path = get_stdlog_file(os.path.relpath(stderr_abs_path), json_root)
    child_info_dict = get_error_dict(bins, stdout_path, stderr_path)
    for childname, childtext in child_info_dict.items():
        errChild = get_err_child_info(childname, childtext)
        child_element.append(errChild)


def make_xml_report(json_files, json_root, test_suites):
    global is_failed
    statistics_data = {
        "tests": 0,
        "failures": 0,
        "disabled": 0,
        "errors": 0,
        "total_reports": 0,
        "empty_reports": 0,
    }

    root = ET.Element("testsuites")

    for file in json_files:
        report_path = os.path.dirname(os.path.relpath(file, json_root + "/qemu"))
        bins = get_bins(file, test_suites, json_root) or {}
        if os.stat(file).st_size != 0:
            with open(file, "r") as f:
                data = json.load(f)

            statistics_data["tests"] += data["tests"]
            statistics_data["failures"] += data["failures"]
            statistics_data["disabled"] += data["disabled"]
            statistics_data["errors"] += data["errors"]
            statistics_data["total_reports"] += 1

            testsuites = data["testsuites"]

            for testsuite in testsuites:
                testsuiteChild = ET.Element("testsuite")
                testsuiteChild.set("report_path", report_path)

                for key, item in testsuite.items():
                    if key != "testsuite":
                        testsuiteChild.set(key, str(item))
                root.append(testsuiteChild)

                errors = testsuite["errors"]
                if errors > 0:
                    # path without "test_detail.json":
                    prefix = file.split(TestArtifact.REPORT.value)[0]
                    reports_abs_path_template = Template(f"{prefix}$postfix")

                    append_err_info(
                        testsuiteChild, bins, reports_abs_path_template, json_root
                    )

                tests = testsuite["testsuite"]

                for test in tests:
                    testChild = ET.Element("testcase")
                    for key, item in test.items():
                        if key == "failures":
                            for failure in item:
                                failChild = ET.Element("failure")
                                failChild.text = failure["failure"]
                                for attr, val in failure.items():
                                    if attr == "failure":
                                        is_failed = True
                                        failChild.set(attr, str(val))
                                testChild.append(failChild)
                        else:
                            testChild.set(key, str(item))

                    testsuiteChild.append(testChild)
        else:
            is_failed = True
            testsuiteChild = ET.Element("testsuite")
            report_path = os.path.dirname(os.path.relpath(file, json_root + "/qemu"))
            testsuiteChild.set("name", report_path)
            root.append(testsuiteChild)

            testChild = ET.Element("testcase")
            testChild.set("name", "_".join(bins))
            testChild.set("classname", report_path)

            reports_abs_path_template = Template(f"$postfix")

            append_err_info(testChild, bins, reports_abs_path_template, json_root)

            testsuiteChild.append(testChild)

            num_of_tests = len(bins)
            statistics_data["tests"] += num_of_tests
            statistics_data["errors"] += num_of_tests
            statistics_data["total_reports"] += 1
            statistics_data["empty_reports"] += 1

    for key, item in statistics_data.items():
        root.set(key, str(item))

    tree = ET.ElementTree(root)

    return tree


def export_summory(file_name, xml_tree):
    # export xml
    if (
        is_failed
    ):  # Do not change. CI determines if job should be faied based on parsing of report name
        postfix = "FAILED"
    else:
        postfix = "PASSED"
    xml_file = f"{file_name}_{postfix}.xunit.xml"

    with open(xml_file, "wb") as files:
        xml_tree.write(files)

    prettify_report(xml_file)


def main(args=None):
    if not args:
        args = Args()
    json_files = find_json_files(args.path)
    tests = get_adlt_test_info(args.adlt)
    xml_tree = make_xml_report(json_files, args.path, tests)
    export_summory(args.report, xml_tree)


if __name__ == "__main__":
    args = cmdline_args()
    main(args)
    if is_failed:
        print("Detected some failed tests. Exiting with code=1")
        exit(1)
