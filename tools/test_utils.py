"""
@file test_utils.py
@brief This script contains structures for tests.
"""

from dataclasses import dataclass, field
from enum import Enum
from typing import List


class TestArtifact(Enum):
    """
    Enum for holding different types of output logs: LOG, REPORT, TMP, ERR.
    """

    LOG = "test_detail.log"
    REPORT = "test_detail.json"
    TMP = "test_detail.tmp"
    ERR = "test_detail.err.log"


@dataclass
class TestData:
    """
    TestData class represents the entity of a single test and stores its settings.

    Args:
        binary (str): Name of the binary test file.
        artifact_prefix (str): Test report name prefix. Shall be unique in test the folder.
        args (List[str]): Test executable options.
        env (List[str]): Test executable preconditional enviroment settings.
        qemu_args (str): Additional arguments to qemu.
    """

    binary: str
    artifact_prefix: str
    args: List[str] = field(default_factory=list)
    env: List[str] = field(default_factory=list)
    qemu_args: str = ""


@dataclass
class TestInfo:
    """
    TestInfo contains information about the test (TestData object and its state).

    Args:
        test_data (TestData): TestData object.
        state (str): Test state (default value PASSED).
    """

    test_data: TestData
    state: str = "PASSED"
