import os
from enum import Enum
from functools import wraps
from typing import Dict, List, Set, Tuple

import yaml
from test_utils import TestData, TestInfo

__all__ = ["print_call", "flatten"]


class LogLevel(Enum):
    """
    Enum for holding different log levels: INFO, DEBUG.
    """

    INFO = 1
    DEBUG = 2

    def __str__(self):
        return self.name

    @staticmethod
    def from_string(s):
        try:
            return LogLevel[s]
        except KeyError:
            raise ValueError()


logLevel = LogLevel.INFO


def shouldLog(level: LogLevel) -> bool:
    """
    Checks if the global logLevel value is suitable for logging to console.

    Args:
        level (LogLevel): Suitable level for logging.

    Returns:
        bool: True if logging, else otherwise.
    """
    global logLevel
    return logLevel.value >= level.value


def print_call(fun):
    """
    Decorator for functions where need to output information about the function call.
    """

    @wraps(fun)
    def wrapper(*args, **kwargs):
        if shouldLog(LogLevel.DEBUG):
            pargs = ", ".join(
                [f"{arg}" for arg in args]
                + [f"{key}={val}" for key, val in kwargs.items()]
            )
            print(f"@call: {fun.__name__}({pargs})")

        return fun(*args, **kwargs)

    return wrapper


def flatten(xss):
    """
    Concatenates all lists into single flat list.
    """
    return [x for xs in xss for x in xs]


def getTestBinsSet(testInfoList: List[TestInfo]) -> Set[str]:
    """
    Returns set of unique names of test binary files.

    Args:
        testInfoList (List[TestInfo]): List with TestInfo objects.

    Returns:
        Set[str]: Set with names of binary files.
    """
    return set(test_info.test_data.binary for test_info in testInfoList)


def parseTestConfig(config_path: str) -> Dict:
    """
    Parses yaml-config with tests

    Args:
        config_path (str): Path to config file.

    Returns:
        Dict: Config with tests in dict format.
    """
    output_test_list = {}

    test_config_data = None

    try:
        with open(config_path, "r") as f:
            test_config_data = yaml.safe_load(f)
    except Exception as e:
        raise RuntimeError(f"Error while loading/opening {config_path}: {e}!")

    try:
        for test in test_config_data["test-list"]["tests"]:
            for exec_type in ["orig", "adlt"]:
                path = f"{test['path']}/{exec_type}"
                tag = f"\nRunning {exec_type} {test['path'].split('/', 1)[1]} example:"

                tests = []

                for test_case in test["cases"]:
                    args = test_case["args"].copy()

                    qemu_args = ""
                    if "qemu-args" in test_case.keys():
                        qemu_args = test_case["qemu-args"]

                    if test_case["gtest-filter"]:
                        args.append(f"--gtest_filter={test_case['gtest-filter']}")

                    test_data = TestData(
                        test["binary"],
                        test_case["prefix"],
                        args,
                        test_case["env"],
                        qemu_args,
                    )

                    state = "PASSED"

                    if "states" in test_case:
                        if exec_type in test_case["states"]:
                            state = test_case["states"][exec_type]

                    tests.append(TestInfo(test_data, state))

                if path in output_test_list:
                    output_test_list[path]["tests"] += tests
                else:
                    output_test_list[path] = {
                        "tag": tag,
                        "tests": tests,
                    }
    except Exception as e:
        raise RuntimeError(f"Failed to parse config {config_path}: {e}!")

    return output_test_list


def isStandaloneTest(spec: Tuple[str, List[str]], test_list: Dict) -> bool:
    """
    Checks if the input argument is a single test from the test suite.

    Args:
        spec (Tuple[str, List[str]]): User input from args.run.

    Returns:
        bool: Whether a single test or not.
    """
    name, _ = spec[0]

    if name.endswith("/orig") or name.endswith("/adlt"):
        return name in test_list

    result = False

    if name.endswith("/"):
        name = name[:-1]

    for mod in ["/orig", "/adlt"]:
        result |= name + mod in test_list

    return result


def fetchAllTestsFromBasePath(spec: Tuple[str, List[str]], test_list: Dict) -> List:
    """
    Returns a list of tests from the root path.

    Args:
        spec (Tuple[str, List[str]]): User input from args.run.

    Returns:
        List: List of tests.
    """
    name, _ = spec[0]

    if name.endswith("/"):
        name = name[:-1]

    output = []

    for test_path in test_list:
        if test_path.startswith(name):
            output.append((test_path, test_list[test_path]["tests"]))

    return output


def updateRunEnvironment(config_path: str, env: str) -> None:
    """
    Reads data from run_env.yaml and sets the necessary environment variables.

    Args:
        config_path (str): Path to env-config.
        env (str): Env selected by the user via args.
    """
    try:
        with open(config_path, "r") as f:
            config_data = yaml.safe_load(f)
    except Exception as e:
        raise RuntimeError(f"Error while loading/opening {config_path}: {e}!")

    os.environ.update(config_data[env])
