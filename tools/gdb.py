#!/usr/bin/python3

# This script:
# - auto starts gdb server, does connection
# - puts break to main() and continues
# - kills all created processes at exit

import os
import argparse
import subprocess
from pathlib import Path

from tools import Tools, create_symlink
from config import Config, TEST_LIST

def print_test_keys():
    print("possible values for test are:")
    print("   ","\n    ".join(str(key) for key in TEST_LIST.keys()))

def parseArgs():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--run", "-r", help="run specified test in debug qemu mode", required=True)
    parser.add_argument(
        "--script", "-s", help="specified script path", required=False, type=str)
    parser.add_argument(
        "--main", "-m", help="break to main() and continue", required=False)

    parser.add_argument(
        "--test-number", "-n", help="select given test number (counting from 0)", required=False, type=int)

    parser.add_argument(
        "-tui", help="use 'gdb -tui (curses-based session)", required=False, action="store_true")


    args = parser.parse_args()

    if args.run not in set(TEST_LIST.keys()):
        print("TEST ERROR: Incorrect value of 'run' argument.")
        print_test_keys()
        exit(0)

    numberOfTests = len(TEST_LIST[args.run]["tests"])
    if args.test_number is not None and args.test_number not in range(0, numberOfTests):
        print(
            f'TEST ERROR: subtest {args.test_number} out of range, valid values  (0..{numberOfTests-1})')
        exit(0)

    return args


def main():
    args = parseArgs()
    testName = args.run
    testPath = Config.INSTALL_DIR / 'qemu' / testName
    binary = TEST_LIST[testName]["tests"][0 if args.test_number is None else args.test_number].test_data.binary
    opts = TEST_LIST[testName]["tests"][0 if args.test_number is None else args.test_number].test_data.args
    cmdQEMU = Tools().getBaseCommandQEMU(testName, isDebug=True)
    cmdQEMU += " " + binary + " " + " ".join(x for x in opts)
    tui_option = "-tui" if args.tui else ""
    cmdGDB = f"gdb-multiarch {binary} {tui_option} -ex=\"target remote localhost:8081\""
    if args.main and not args.script:
        cmdGDB += " -ex=\"b main\" -ex=\"c\""
    script = os.path.abspath(args.script) if args.script else ""

    killAllCmd = f"killall -9 {cmdQEMU.split()[0]} {cmdGDB.split()[0]} /usr/bin/python3"
    # prepare
    Tools().tryChdir(testPath, testName)
    currDir = Path.cwd()
    if script:
        cmdGDB += f" -command={script}"
    print(f"QEMU command: {cmdQEMU}\n")
    print(f"run QEMU from: {currDir}\n")
    print(f"GDB command: {cmdGDB}\n")

    # start server
    processQEMU = subprocess.Popen(cmdQEMU, shell=True)
    print(f"server started: pid: {processQEMU.pid}")

    # connect
    processGDB = subprocess.run(cmdGDB, shell=True)

    # stop server
    processQEMU.kill()
    # processGDB.kill()
    subprocess.run(f"killall -9 {cmdQEMU.split()[0]}", shell=True)
    print(f"server stopped: killed pid: {processQEMU.pid}")


if __name__ == "__main__":
    main()
