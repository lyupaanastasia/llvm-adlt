set(CMAKE_SYSTEM_PROCESSOR ARM)
set(triple aarch64-linux-ohos)

set(SDK_PARTLY $ENV{SDK_PARTLY})
if(SDK_PARTLY)
    set(LLVM $ENV{SDK_PARTLY})
    set(FUSE_LD "-fuse-ld=$ENV{LLVM}/bin/ld.lld")
else()
    set(LLVM $ENV{LLVM})
endif()
set(LLVM_BIN ${LLVM}/bin)
set(LLVM_SYMBOLIZER_PATH ${LLVM_BIN}/llvm-symbolizer)

set(ADD_CFLAGS $ENV{ADD_CFLAGS})
set(ADD_LDFLAGS $ENV{ADD_LDFLAGS})

set(CMAKE_ASM_COMPILER ${LLVM}/bin/clang++)
set(CMAKE_C_COMPILER ${LLVM}/bin/clang)
set(CMAKE_CXX_COMPILER ${LLVM}/bin/clang++)
set(CMAKE_SYSROOT $ENV{LLVM}/../sysroot)

set(ARCH_FLAGS "--target=${triple}" CACHE STRING "")
set(DIAG_FLAGS "-fcolor-diagnostics -Wall" CACHE STRING "")
set(COMMON_FLAGS "${ARCH_FLAGS} ${DIAG_FLAGS}" CACHE STRING "")

# Specify compiler flags
set(CMAKE_ASM_FLAGS "${CMAKE_ASM_FLAGS} ${ARCH_FLAGS} -Wno-unused-command-line-argument" CACHE STRING "")
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${COMMON_FLAGS}" CACHE STRING "")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${COMMON_FLAGS} -std=c++11 -fno-exceptions -fno-rtti -fno-threadsafe-statics" CACHE STRING "")
set(CMAKE_SHARED_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} ${COMMON_FLAGS} ${FUSE_LD} --rtlib=compiler-rt" CACHE STRING "")
set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} ${COMMON_FLAGS} ${FUSE_LD} --rtlib=compiler-rt" CACHE STRING "")
set(CMAKE_C_FLAGS_DEBUG "${CMAKE_C_FLAGS_DEBUG} -g -gdwarf-4 -O0" CACHE STRING "")
set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_C_FLAGS_DEBUG} -g -gdwarf-4 -O0" CACHE STRING "")

# Asm shared library
set(CMAKE_ASM_CREATE_SHARED_LIBRARY "<CMAKE_ASM_COMPILER> -fPIC ${CMAKE_CXX_FLAGS} ${CMAKE_ASM_FLAGS} ${FUSE_LD} -shared -Wl,--emit-relocs,--no-relax -Wl,-soname,lib<TARGET_NAME>.so -o <TARGET> <OBJECTS>")
