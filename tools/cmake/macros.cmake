# useful macros
if(NOT toolsDir)
    set(toolsDir ${CMAKE_SOURCE_DIR}/../tools)
endif()

macro(initProject)
    set(oneValueArgs PROJECT_NAME)
    set(multiValueArgs EXECUTABLES LIBS)
    cmake_parse_arguments(PARSED "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN})
    project(${PARSED_PROJECT_NAME})
    foreach(LIB ${PARSED_LIBS})
        set(${LIB} ${PROJECT_NAME}_${LIB})
    endforeach()
    foreach(EXEC ${PARSED_EXECUTABLES})
        set(${EXEC} ${PROJECT_NAME}_${EXEC})
    endforeach()
    unset(ADLT_LIB_CONTENTS)
    foreach(LIB ${PARSED_LIBS})
        list(APPEND ADLT_LIB_CONTENTS
            ${CMAKE_CURRENT_BINARY_DIR}/lib${PROJECT_NAME}_${LIB}.so)
    endforeach()
    set(ADLT_PROJECT_LIB libadlt_${PROJECT_NAME}.so)
endmacro(initProject)


macro(set_global_options)
    add_compile_options(${ADD_CFLAGS})
    add_link_options(${ADD_LDFLAGS} -Wl,--emit-relocs,--no-relax) # ,--verbose

    if(NOT $ENV{RELR} EQUAL "")
        if("$ENV{RELR}" EQUAL "1" OR "$ENV{RELR}" STREQUAL "android+relr")
            add_link_options("-Wl,--pack-dyn-relocs=android+relr")
        elseif("$ENV{RELR}" EQUAL "2" OR "$ENV{RELR}" STREQUAL "relr")
            add_link_options("-Wl,--pack-dyn-relocs=relr")
        elseif("$ENV{RELR}" EQUAL "3" OR "$ENV{RELR}" STREQUAL "android")
            add_link_options("-Wl,--pack-dyn-relocs=android")
        else()
            message( FATAL_ERROR "bad RELR=$ENV{RELR},
	             must be in {1, android+relr, 2, relr, 3, android}" )
         endif()
    endif()
endmacro(set_global_options)


macro(gen_qemu_env)
    set(oneValueArgs BINARY)
    set(multiValueArgs QEMU_LIBS)
    cmake_parse_arguments(PARSED "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN})
    string(REGEX REPLACE "${CMAKE_BINARY_DIR}" "" dest ${CMAKE_CURRENT_BINARY_DIR})
    set(outQemu qemu/${dest})

    add_custom_command(
        TARGET ${PARSED_BINARY} POST_BUILD
        COMMAND python3 ${toolsDir}/gen_qemu_env.py -od ${CMAKE_CURRENT_BINARY_DIR}
            $<IF:$<CONFIG:Debug>,--debug,>
        DEPENDS ${PARSED_QEMU_LIBS}
        )
    # modified linker and modified musl
    install(DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/lib DESTINATION ${outQemu}/adlt)
    # original linker and modified musl
    install(DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/lib DESTINATION ${outQemu}/orig)
    # original linker and original musl
    install(DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/lib DESTINATION ${outQemu}/withOrigMusl)
    install(DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/lib_orig/ DESTINATION ${outQemu}/withOrigMusl/lib)

    if(CMAKE_BUILD_TYPE STREQUAL "Debug")
        install(DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/obj DESTINATION ${outQemu}/adlt)
        install(DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/obj DESTINATION ${outQemu}/orig)

        # install(DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/out DESTINATION ${outQemu}/orig)
        # install(DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/out DESTINATION ${outQemu}/adlt)

        install(DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/toolchain DESTINATION ${outQemu}/orig)
        install(DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/toolchain DESTINATION ${outQemu}/adlt)

        # install(DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/ DESTINATION ${outQemu}/orig
        #     PATTERN "*.cpp" PATTERN "*.c" PATTERN "*.h")
        # install(DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/ DESTINATION ${outQemu}/adlt
        #     PATTERN "*.cpp" PATTERN "*.c" PATTERN "*.h")
    endif()
endmacro(gen_qemu_env)


macro(add_gtest)
    set(oneValueArgs TARGET)
    set(multiValueArgs DEPENDENCIES SOURCES)
    cmake_parse_arguments(PARSED "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN})
    set(LLVM_PROJ $ENV{SDK_ROOT}/toolchain/llvm-project)

    add_executable(${PARSED_TARGET} ${PARSED_SOURCES})
    set_property(TARGET ${PARSED_TARGET} PROPERTY CXX_STANDARD 14)
    target_include_directories(${PARSED_TARGET} PRIVATE
      # Set of include dirs for local SDK
      ${LLVM_PROJ}/llvm/utils/unittest/googletest/include
      ${LLVM_PROJ}/llvm/utils/unittest/googlemock/include
      ${LLVM_PROJ}/llvm/include
      ${LLVM_PROJ}/utils/bazel/llvm-project-overlay/llvm/include
      # Set of include dirs for prebuilt SDK
      $ENV{LLVM}/googletest/include
      $ENV{LLVM}/googlemock/include
      $ENV{LLVM}/include
      )
    target_link_libraries(${PARSED_TARGET} PRIVATE ${LIB_SUPPORT} ${LIB_GTEST} ${PARSED_DEPENDENCIES})
endmacro(add_gtest)

macro(add_adlt_test)
    set(options INSTALL_TEST_LIB_TO_LIB_A)
    set(oneValueArgs TARGET)
    set(multiValueArgs SOURCES LIBS ADLT_EXTRA_LIBS)
    cmake_parse_arguments(PARSED "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN})

    set(testLibsDir "lib")
    if(PARSED_INSTALL_TEST_LIB_TO_LIB_A)
        set(testLibsDir "lib_a")
    endif()

    add_gtest(TARGET ${PARSED_TARGET} SOURCES ${PARSED_SOURCES} DEPENDENCIES ${PARSED_LIBS})
    if(PARSED_ADLT_EXTRA_LIBS)
        add_dependencies(${PARSED_TARGET} ${PARSED_ADLT_EXTRA_LIBS})
    endif()
    gen_qemu_env(BINARY ${PARSED_TARGET} LIBS ${PARSED_LIBS} ${PARSED_ADLT_EXTRA_LIBS})
    string(REGEX REPLACE "${CMAKE_BINARY_DIR}" "" dest ${CMAKE_CURRENT_BINARY_DIR})

    unset(PROCESSED_LIBS)
    foreach(LIB ${PARSED_LIBS} ${PARSED_ADLT_EXTRA_LIBS})
        list(APPEND PROCESSED_LIBS
            ${CMAKE_CURRENT_BINARY_DIR}/lib${LIB}.so)
    endforeach()

    set(outQemu qemu/${dest})
    add_custom_command(
        TARGET ${PARSED_TARGET} POST_BUILD
        COMMAND python3 ${toolsDir}/adlt_makelinks.py
            -i ${PROCESSED_LIBS} -od ${CMAKE_INSTALL_PREFIX}/${outQemu}/adlt/${testLibsDir}
            -o "${ADLT_PROJECT_LIB}"
    )

    # modified linker and modified musl
    install(TARGETS ${PARSED_TARGET} DESTINATION ${outQemu}/adlt)
    # original linker and modified musl
    install(TARGETS ${PARSED_TARGET} DESTINATION ${outQemu}/orig)
    install(TARGETS ${PARSED_LIBS}  ${PARSED_ADLT_EXTRA_LIBS} DESTINATION ${outQemu}/orig/${testLibsDir})
    # original linker and original musl
    install(TARGETS ${PARSED_TARGET} DESTINATION ${outQemu}/withOrigMusl)
    install(TARGETS ${PARSED_LIBS}  ${PARSED_ADLT_EXTRA_LIBS} DESTINATION ${outQemu}/withOrigMusl/lib)
endmacro(add_adlt_test)
