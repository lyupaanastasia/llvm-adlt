import os
import argparse
from pathlib import Path

from tools import shell
from config import Config

def parseArgs():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--outputDir", "-od", help="output directory path", required=True, default=None)
    parser.add_argument(
        "--debug", help="debug", action='store_true',  default=False)
    parser.add_argument(
        "--trace", help="trace", action='store_true',  default=False)
    return parser.parse_args()


def main():
    args = parseArgs()
    outDir = Path(args.outputDir)
    outDirLib = outDir / "lib"
    outDirOrigLib = outDir / "lib_orig"

    if (args.trace):
        print(f"outDir: {outDir}")
        print(f"outDirLib: {outDirLib}")
        print(f"outDirOrigLib: {outDirOrigLib}")

    os.makedirs(outDir, exist_ok=True)
    os.makedirs(outDirLib, exist_ok=True)
    os.makedirs(outDirOrigLib, exist_ok=True)

    origMuslSysroot = Config.SDK_ROOT / "out" / "lib" / "musl-orig-aarch64-linux-ohos"
    origMuslLib = origMuslSysroot / "lib"
    sysRootLib = Config.SYSROOT / "lib"
    sdkOutDir = Config.SDK_ROOT / "out"

    if (args.trace):
        print(f"origMuslLib: {origMuslLib}")
        print(f"sysRootLib: {sysRootLib}")
        print(f"sdkOutDir: {sdkOutDir}")

    llvmTargetLib = Config.LLVM / "lib" / "aarch64-linux-ohos"
    clangTargetLib = Config.LLVM / "lib" / "clang" / \
        "15.0.4" / "lib" / "aarch64-linux-ohos"
    toolchainDir = Config.SDK_ROOT / "toolchain"
    # llvmSource = toolchainDir / "llvm-project"

    muslSourcePath = sdkOutDir / "llvm_build" / "obj"
    muslUnstrippedLib = sdkOutDir / "llvm_build" / "lib.unstripped" / "obj" / "third_party" / \
        "musl" / "usr" / "lib" / "aarch64-linux-ohos" / "libc.so"
    crtUbsanLib = "libclang_rt.ubsan_standalone.so"
    # crtUbsanSourcePath = llvmSource / "compiler-rt" / "lib" / "ubsan"

    toLink = {
        # common libs
        llvmTargetLib / "libc++.so":            outDirLib / "libc++.so",
        llvmTargetLib / "libc++.so.1":          outDirLib / "libc++.so.1",
        clangTargetLib / crtUbsanLib:           outDirLib / crtUbsanLib,
        sysRootLib / "libllvm_gtest.so.15":     outDirLib / "libllvm_gtest.so.15",
        sysRootLib / "libLLVMSupport.so.15":    outDirLib / "libLLVMSupport.so.15",
        sysRootLib / "libLLVMDemangle.so.15":   outDirLib / "libLLVMDemangle.so.15",
        sysRootLib / "libc.so":                 outDirLib / "ld-musl-aarch64.so.1",
        # orig musl
        origMuslLib / "libc.so":                outDirOrigLib / "ld-musl-aarch64.so.1",
    }

    toDebugLink = {
        toolchainDir: outDir / "toolchain",
        # sdkOutDir : outDir / "out",
        muslSourcePath: outDir / "obj",
        # crtUbsanSourcePath / "ubsan_handlers.cpp" : outDir / "ubsan_handlers.cpp",
    }

    for key, value in [*toLink.items(), *toDebugLink.items()]:
        cmd = f"ln -sf {key} {value}"
        if args.trace:
            print(cmd)
        if not os.path.exists(value):
            shell(cmd)


if __name__ == "__main__":
    main()
