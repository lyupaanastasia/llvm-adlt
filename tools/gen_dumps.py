#!/usr/bin/python3
import os
import argparse
from pathlib import Path
from tools import shell, Tools


def genDumps(file, out, dbg=False):
    if (dbg):
        print(file)
        print(out)
    objdump = Tools().getLLVMTool("llvm-objdump")
    readelf = Tools().getLLVMTool("llvm-readelf")
    readelfOut = Path(f"{out}.readelf")
    objdumpOut = Path(f"{out}.objdump")
    for i in [readelfOut, objdumpOut]:
        os.makedirs(i, exist_ok=True)

    objdumpMap = {
        # "--section-headers": "section_headers",
        # "--reloc": "reloc",
        # "--dynamic-reloc": "reloc_dynamic",
        "--full-contents": "full_contents",
        "--dynamic-syms --syms": "symbols",
        # "--syms": "syms",
        "--section=.adlt --section=.adlt.strtab -s": "adlt_section",
        # "--dynamic-syms": "syms_dynamic",
        # "--disassemble": "disassemble",
        "--disassemble-all --reloc --dynamic-reloc": "disasm_with_relocs",
        # "-xsSdD": "all",
    }
    for key, val in objdumpMap.items():
        shell(f"{objdump} {file} {key}", objdumpOut / f"{val}.asm")

    readelfMap = {
        "--symbols": "symbols",
        "--program-headers": "program_headers",
        "--relocations": "relocations",
        "--dynamic-table": "dynamic_table",
        "--section-headers": "section_headers",
        "--adlt-section --elf-output-style=LLVM": "adlt",
        # "--file-header": "file_header",
        # "--hex-dump" : "hex_dump",
    }
    for key, val in readelfMap.items():
        shell(f"{readelf} {file} {key}", readelfOut / f"{val}.asm")


def parseArgs():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--inputFiles", "-i", nargs='+', help="input files", required=True, default=None)
    parser.add_argument(
        "--outputDirs", "-od", nargs='+', help="output directories", default=None)
    parser.add_argument(
        "--debug", help="debug", action='store_true',  default=False)
    return parser.parse_args()


def main():
    args = parseArgs()
    for file in args.inputFiles:
        f = Path(file)
        if args.outputDirs:
            for outDir in args.outputDirs:
                out = Path(outDir) / f.stem
                os.makedirs(outDir, exist_ok=True)
                genDumps(f, out, args.debug)
        else:
            out = f.parent / f.stem
            genDumps(f, out, args.debug)


if __name__ == "__main__":
    main()
