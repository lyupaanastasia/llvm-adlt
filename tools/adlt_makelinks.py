import os
import argparse

from tools import shell, create_symlink
from config import Config


def parseArgs():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--inputLibs", "-i", nargs='*', help="input libs", required=True, default=None)
    parser.add_argument(
        "--outputDir", "-od", help="output directory path", required=True, default=None)
    parser.add_argument(
        "--output", "-o", help="output file name", required=False, default=None)
    parser.add_argument(
        "--linkOrigLib",  help="link to original libs", action='store_true', default=False)
    parser.add_argument(
        "--debug", help="debug", action='store_true',  default=False)
    return parser.parse_args()


def main():
    args = parseArgs()
    JOIN = os.path.join

    if len(args.inputLibs) == 0:
        exit(0)
    if (args.output is not None) and args.linkOrigLib :
        print("Ambiguous parameter. --linkOrigLib and --output cannot be used together")
        exit(0)

    libs = ' '.join(args.inputLibs)

    if (args.debug):
        print(args.outputDir)
    os.makedirs(args.outputDir, exist_ok=True)
    outLib = args.output

    libs = args.inputLibs
    if outLib is not None:
        libs.append(outLib)
    for lib in libs:
        _, libname = os.path.split(lib)
        create_symlink(
            os.path.abspath(outLib if not args.linkOrigLib else os.path.abspath(lib)),
            JOIN(os.getcwd(), args.outputDir, libname),
            args.debug,
            force=True
        )


if __name__ == "__main__":
    main()
