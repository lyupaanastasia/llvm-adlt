#!/usr/bin/python3
import os
import shutil
from pathlib import Path

from tools import shell
from config import Config


class MuslBuilder:
    LLVM_BUILD = Config.SDK_ROOT / "out" / "llvm_build"
    CLANG_GNI = Config.SDK_ROOT / "build" / "config" / "clang" / "clang.gni"
    CLANG_GNI_TMP = f"{CLANG_GNI}_tmp"
    GNI_ARGS = Config.SDK_ROOT / "out" / "llvm_build" / "args.gn"
    GNI_ARGS_TMP = Config.SDK_ROOT / "out" / "args.gn_tmp"
    installOut = None

    def getGnArgs(self):
        gn_args = ""
        with open(self.GNI_ARGS) as f:
            lines = f.readlines()
        for line in lines:
            # get debug args for gn
            if line.startswith("is_debug") or line.startswith("ohos_extra_cflags"):
                gn_args = line.strip().replace(" ", "") + " " + gn_args
        return gn_args

    def changeEnv(self):
        # change compiller path to prebuilds in clang.gni file (only for SDK_PARTLY)
        if Config.SDK_PARTLY != None:
            shutil.move(self.CLANG_GNI, self.CLANG_GNI_TMP)
            with open(self.CLANG_GNI_TMP, "r") as f1:
                data = f1.read()
            with open(self.CLANG_GNI, "w") as f2:
                old = "//out/llvm-install"
                new = (
                    f"{Config.SDK_ROOT}/prebuilts/clang/ohos/linux-x86_64/clang-15.0.4"
                )
                data = data.replace(old, new)
                f2.write(data)
        # save original args.gn, to don't lost original building flags.
        if os.path.exists(self.GNI_ARGS):
            shutil.copy2(self.GNI_ARGS, self.GNI_ARGS_TMP)

    def restoreEnv(self):
        # return original version of clang.gni (only for SDK_PARTLY)
        if Config.SDK_PARTLY != None:
            shutil.move(self.CLANG_GNI_TMP, self.CLANG_GNI)
        # return original args.gn
        if os.path.exists(self.GNI_ARGS_TMP):
            shutil.move(self.GNI_ARGS_TMP, self.GNI_ARGS)

    def runHb(self, target_name, gn_args=""):
        hb_build_py = Config.SDK_ROOT / "build" / "hb" / "main.py"
        llvm_gn_args = (
            "is_llvm_build=true startup_init_with_param_base=true use_thin_lto=false"
        )
        shell(
            f"python3 {hb_build_py} build \
            --product-name 'llvm_build' \
            --target-cpu 'arm64' \
            --build-target {target_name} \
            --gn-args '{gn_args} {llvm_gn_args}' \
            --deps-guard=false",
            out=self.installOut,
            cur_dir=Config.SDK_ROOT,
        )

    def clean_musl(self):
        gn = Config.SDK_ROOT / "prebuilts" / "build-tools" / "linux-x86" / "bin" / "gn"
        shell(
            f"{gn} clean {self.LLVM_BUILD}",
            out=self.installOut,
            err=self.installOut,
            cur_dir=Config.SDK_ROOT,
        )

    def build(self, installPath, silentInstall=True):
        if Config.SDK_PREBUILT != None:
            return
        # ignore crt, libcxx related warnings at install
        self.installOut = Path("/dev") / "null" if silentInstall else None
        gn_args = self.getGnArgs()

        # build musl
        try:
            self.changeEnv()
            self.clean_musl()
            # build musl headers
            self.runHb("musl_headers")
            # build musl lib
            self.runHb("soft_musl_libs", gn_args)
        finally:
            self.restoreEnv()

        # install musl
        target = "aarch64-linux-ohos"
        outUsrMusl = self.LLVM_BUILD / "obj" / "third_party" / "musl" / "usr"
        if not os.path.exists(outUsrMusl):
            return
        for folder in ["include", "lib"]:
            shutil.copytree(
                outUsrMusl / folder / target, installPath / folder, dirs_exist_ok=True
            )


def buildMusl(silentInstall=True):
    MuslBuilder().build(
        Config.SDK_ROOT / "out" / "sysroot" / "aarch64-linux-ohos" / "usr",
        silentInstall,
    )


def buildOrigMusl(silentInstall=True):
    if Config.SDK_PREBUILT != None:
        return
    out = Config.SDK_ROOT / "out" / "lib" / "musl-orig-aarch64-linux-ohos"
    musl_src = Config.SDK_ROOT / "third_party" / "musl"
    musl_src_orig = f"{musl_src}_orig"
    musl_src_tmp = f"{musl_src}_tmp"
    llvm_build = Config.SDK_ROOT / "out" / "llvm_build"
    llvm_build_tmp = f"{llvm_build}_tmp"
    # ignore crt, libcxx related warnings at install
    installOut = Path("/dev") / "null" if silentInstall else None

    # clean musl-orig out dir
    if os.path.exists(out):
        shutil.rmtree(out)
    os.makedirs(out, exist_ok=True)

    try:
        # temporary hide musl
        shutil.move(musl_src, musl_src_tmp)
        shutil.copytree(llvm_build, llvm_build_tmp, symlinks=True)
        # get sources of musl-orig
        if os.path.exists(musl_src_orig):
            shutil.move(musl_src_orig, musl_src)
        else:
            shell(
                f"git clone https://gitee.com/kosovpavel/third_party_musl.git \
                -b adlt-dev-master-base --depth=1 {musl_src}",
                out=installOut,
                err=installOut,
            )
        # build musl-orig
        MuslBuilder().build(out, silentInstall)
    finally:
        # save musl-orig
        if os.path.exists(musl_src):
            shutil.move(musl_src, musl_src_orig)
        if os.path.exists(llvm_build):
            shutil.rmtree(llvm_build)
        # return musl
        shutil.move(musl_src_tmp, musl_src)
        shutil.move(llvm_build_tmp, llvm_build)
