import os
import argparse
import shutil

from pathlib import Path

from tools import shell, create_symlink
from config import Config


def parseArgs():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--inputLibs", "-i", nargs="*", help="Input libs", required=True, default=None
    )
    parser.add_argument(
        "--outputDir", "-od", help="Output directory path", required=True, default=None
    )
    parser.add_argument(
        "--ldFlags", "-f", help="ld flags", required=False, default=None
    )
    parser.add_argument(
        "--output", "-o", help="Output file name", required=False, default="libadlt.so"
    )
    parser.add_argument("--debug", help="Debug", action="store_true", default=False)
    parser.add_argument("--trace", help="Trace", default=None)
    parser.add_argument(
        "--emitRelocs", help="Enable --emit-relocs", action="store_true", default=True
    )
    parser.add_argument(
        "--no-relax",
        help="Disable relaxation (enabled by default)",
        required=False,
        default=None,
    )
    parser.add_argument(
        "--stripped",
        help="Build and use stripped ADLT libraries.",
        default=None,
    )
    return parser.parse_args()


def main():
    args = parseArgs()

    if args.trace is None:
        if os.environ.get("ADLT_TRACE") and os.environ.get("ADLT_TRACE") != "0":
            args.trace = True
        else:
            # Disable by default.
            args.trace = False

    if args.stripped is None:
        if os.environ.get("STRIPPED") and os.environ.get("STRIPPED") != "0":
            args.stripped = True
        else:
            # Disable by default.
            args.stripped = False

    if args.ldFlags is None:
        args.ldFlags = os.environ.get("LD_FLAGS", "")
        if len(args.ldFlags) == 0:
            args.ldFlags = "--adlt"

    if args.no_relax is None:
        if os.environ.get("ADLT_NO_RELAX") and os.environ.get("ADLT_NO_RELAX") != "0":
            args.no_relax = True
        else:
            # Enable relaxation by default.
            args.no_relax = False

    print(
        f"ldFlags {args.ldFlags}\n"
        f"inputLibs {args.inputLibs}\n"
        f"outputDir {args.outputDir}\n"
        f"target {args.output}"
    )
    libs = " ".join(args.inputLibs)
    args.outputDir = Path(args.outputDir)

    if args.debug:
        print(args.outputDir)
    os.makedirs(args.outputDir, exist_ok=True)

    binaries = Path(Config.LLVM, "bin")
    ld = binaries / "ld.lld"
    strip = binaries / "llvm-strip"
    outLib = args.output
    cmd = f"{ld} {args.ldFlags} {libs} -o {outLib}"
    if args.emitRelocs:
        cmd += " --emit-relocs "
    if args.no_relax:
        cmd += " --no-relax "
    else:
        cmd += " --relax "
    if args.trace:
        cmd += " --adlt-trace"
        print(f"ADLT COMMAND: {cmd}")
    shell(cmd)
    if args.stripped:
        # do backup
        unstripped = Path("unstripped")
        unstrippedLib = unstripped / args.output
        os.makedirs(unstripped, exist_ok=True)
        shutil.copy(args.output, unstrippedLib)
        cmd = f"{strip} -o {args.output} {unstrippedLib}"
        print(f"STRIP: {cmd}")
        shell(cmd)


if __name__ == "__main__":
    main()
