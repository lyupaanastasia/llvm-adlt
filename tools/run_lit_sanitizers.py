#!/usr/bin/python3
"""
@file run_lit_sanitizers.py

@brief The script provides an interface to run lit tests from compiler-rt with qemu and ADLT.
"""
import argparse
from config import Config
from tools import shell

# TODO: Support more tests.
supportedTests = ["asan", "ubsan", "tsan"]

# Some tests require additional libraries to be built, so we need to take this into account.
extraBuildTargets = {"tsan": ["libcxx_tsan_aarch64"]}


def parseArgs() -> argparse.Namespace:
    """
    Parsing command line arguments.
    """
    global supportedTests

    parser = argparse.ArgumentParser(formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument(
        "--test",
        "-t",
        help=f"Test to test, available: {', '.join(supportedTests)}",
        default=None,
    )
    parser.add_argument(
        "--llvm-project",
        help="Path to llvm project directory.",
        default=None,
    )
    parser.add_argument(
        "--llvm-install",
        help="Path to llvm binaries.",
        default=Config.LLVM,
        required=False,
    )

    args = parser.parse_args()

    return args


def configureCompilerRtAndBuild(
    llvmProject: str, llvmInstall: str, buildTargets: list = []
) -> None:
    """
    Configuring a compiler-rt project using CMake to run lit tests.

    Args:
        llvmProject (str): Path to llvm project directory.
        llvmInstall (str): Path to llvm binaries.
        buildTargets (list): List of additional targets to build.
    """
    workDir = Config.BUILD_DIR / "lit-tests" / "compiler-rt"
    shell(f"rm -rf {workDir}")

    cmakeCmd = f"""
    cmake \
    -B {workDir} \
    -DCMAKE_SYSTEM_NAME=OHOS \
    -DOHOS=1 \
    {llvmProject}/compiler-rt \
    -G Ninja \
    -DCMAKE_MODULE_PATH="{Config.SDK_ROOT}/prebuilts/cmake/linux-x86/share/cmake-3.28/Modules/" \
    -DCMAKE_SYSTEM_NAME=OHOS \
    -DCMAKE_AR="{llvmInstall}/bin/llvm-ar" \
    -DLLVM_CONFIG_PATH="{llvmInstall}/bin/llvm-config" \
    -DCMAKE_ASM_COMPILER_TARGET="aarch64-linux-ohos" \
    -DCMAKE_ASM_FLAGS="--target=aarch64-linux-ohos -B{Config.SYSROOT}/lib -L{Config.SYSROOT}/lib -I{Config.SYSROOT}/include" \
    -DCMAKE_C_COMPILER="{llvmInstall}/bin/clang" \
    -DCMAKE_C_COMPILER_TARGET="aarch64-linux-ohos" \
    -DCMAKE_C_FLAGS="--target=aarch64-linux-ohos -B{Config.SYSROOT}/lib -L{Config.SYSROOT}/lib -I{Config.SYSROOT}/include" \
    -DCMAKE_CXX_COMPILER="{llvmInstall}/bin/clang++" \
    -DCMAKE_CXX_COMPILER_TARGET="aarch64-linux-ohos" \
    -DCMAKE_CXX_FLAGS="--target=aarch64-linux-ohos -B{Config.SYSROOT}/lib -L{Config.SYSROOT}/lib -I{Config.SYSROOT}/include" \
    -DCMAKE_EXE_LINKER_FLAGS="--target=aarch64-linux-ohos -B{Config.SYSROOT}/lib -L{Config.SYSROOT}/lib -I{Config.SYSROOT}/include" \
    -DCOMPILER_RT_BUILD_BUILTINS=OFF \
    -DCOMPILER_RT_BUILD_LIBFUZZER=OFF \
    -DCOMPILER_RT_BUILD_MEMPROF=OFF \
    -DCOMPILER_RT_BUILD_PROFILE=ON \
    -DCOMPILER_RT_BUILD_SANITIZERS=ON \
    -DCOMPILER_RT_BUILD_XRAY=OFF \
    -DCOMPILER_RT_DEFAULT_TARGET_ONLY=ON \
    -DCOMPILER_RT_INCLUDE_TESTS=ON \
    -DCOMPILER_RT_CAN_EXECUTE_TESTS=ON \
    -DCOMPILER_RT_TEST_COMPILER="{llvmInstall}/bin/clang" \
    -DCOMPILER_RT_TEST_COMPILER_CFLAGS="--target=aarch64-linux-ohos -B{Config.SYSROOT}/lib -L{Config.SYSROOT}/lib -I{Config.SYSROOT}/include" \
    -DCOMPILER_RT_TEST_STANDALONE_BUILD_LIBS=OFF \
    """

    shell(cmakeCmd)

    for target in buildTargets:
        shell(f"ninja -C {workDir} {target}")


def run(test: str) -> None:
    """
    Running compiler-rt lit tests with llvm-lit.

    Args:
        test (str): Test name.
    """
    lit = Config.BUILD_DIR / "lit-tests" / "compiler-rt" / "bin" / "llvm-lit"
    cfgPath = Config.ROOT_DIR / "lit-tests" / "compiler-rt" / test
    commonPath = Config.ROOT_DIR / "lit-tests" / "compiler-rt" / "common"
    runCmd = f"{lit} -v {cfgPath} --param emulator={commonPath}/qemu.py"
    shell(runCmd)


def main() -> None:
    global supportedTests, extraBuildTargets

    args = parseArgs()

    if args.test not in supportedTests:
        print("The unknown name of the test! Use --help.")
        exit(1)

    if not args.llvm_project:
        print("The path to llvm-project is not set! Use --help.")

    configureCompilerRtAndBuild(
        args.llvm_project, args.llvm_install, extraBuildTargets.get(args.test, [])
    )

    run(args.test)


if __name__ == "__main__":
    main()
