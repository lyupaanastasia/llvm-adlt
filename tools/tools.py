import os
import subprocess
from pathlib import Path
import time

import util
import json
import datetime
import config
from config import Config, TEST_LIST
from test_utils import TestArtifact


def shell(cmd, out=None, err=None, cur_dir=None, show_error=True):
    try:
        fout = open(out, "w") if out else out
        ferr = open(err, "w") if err else err
        return subprocess.check_call(
            cmd, shell=True, stdout=fout, stderr=ferr, close_fds=False, cwd=cur_dir
        )
    except subprocess.CalledProcessError as e:
        if show_error:
            print("\n" + str(e))
        return e.returncode


def create_symlink(src, dest, dbg=False, force=False):
    cmd = f"ln -sf {src} {dest}"
    if dbg:
        print(cmd)
    if force or not os.path.exists(dest):
        shell(cmd)


class Tests:  # Test operations
    def configure(self, isDebug=False):
        toolchain = Config.ROOT_DIR / "tools" / "cmake" / "aarch64.cmake"
        print(f"LLVM path: {Config.LLVM}")

        if isDebug:
            buildType = "Debug"
            optFlag = "-O0"
        else:
            buildType = "Release"
            optFlag = "-O3"

        print(f"Building tests in {buildType} mode.")
        Tools().configureCmakeProject(
            srcDir=Config.ROOT_DIR / "tests",
            buildDir=Config.BUILD_DIR,
            installDir=Config.INSTALL_DIR,
            envVars=f"LLVM={Config.LLVM}",
            buildType=buildType,
            addCFLAGS=f"{optFlag}",  # it might be enhanced
            toolchain=toolchain,
            silentConfigure=True,
        )  # , verbose=True)

    def build(self, toBuild, testList, silentInstall=True, doTrace=False):
        if not toBuild.issubset(set(testList)):
            print("TEST ERROR: Incorrect value of 'build' argument.")
            exit(1)
        for test in toBuild:
            Tools().buildCmakeTarget(
                Config.BUILD_DIR, Path(test) / "all", verbose=doTrace
            )
            Tools().buildCmakeTarget(
                Config.BUILD_DIR,
                Path(test) / "install",
                verbose=doTrace,
                silent=silentInstall,
            )

    @util.print_call
    def run(
        self,
        testParams,
        info={"tag": "", "tests": []},
        isDebug=False,
        doLogFile=False,
        doTrace=False,
        withOrigMusl=False,
        profiler=None,
    ):
        if withOrigMusl:
            # skip adlt tests for --withOrigMusl
            if testParams[0].split("/")[-1] == "adlt":
                if doTrace:
                    print(f"{testParams[0]} cannot run with --withOrigMusl flag.\n")
                return
            # run only orig tests
            testParams = (
                testParams[0].replace("/orig", "/withOrigMusl"),
                testParams[1],
            )

        Tools().runQEMU(
            testParams,
            info,
            isDebug=isDebug,
            profiler=profiler,
            doLogFile=doLogFile,
            doTrace=doTrace,
        )

    def clean(self, toClean, doTrace=False):
        for test in toClean:
            if doTrace:
                print(f"removing {test}")
            shell(
                f"rm -rf {Config.BUILD_DIR / test} {Config.INSTALL_DIR / 'qemu' / test}"
            )


class LLDTests:
    """
    LLVM LLD tests
    Note that they do not need to be configured.
    Configuration process run as part of toolchain build.
    """

    state = 0

    def build(self, doTrace=False):
        self.state = Tools().buildCmakeTarget(
            Config.MAKE_DIR, "lld-test-depends", verbose=doTrace
        )

        if self.state:
            print("lld-tests: Build lld-test-depends failed, stop build")
        return self.state

    @util.print_call
    def run(
        self,
        doTrace=False,
    ):
        if self.state:
            print("lld-tests: Build stage failed, do not run")
            return self.state
        os.environ["LIT_OPTS"] = "-vva" if doTrace else ""
        return Tools().buildCmakeTarget(Config.MAKE_DIR, "check-lld", verbose=doTrace)

    def clean(self, doTrace=False):
        test_dir = Config.MAKE_DIR / "tools" / "lld" / "test"
        if doTrace:
            print(f"Removing tests in {test_dir}")
        # Need to remove everything except lit.site.cfg.py,
        # so next time we do not need to reconfigure lld
        shell(
            f"find {test_dir} -maxdepth 1 -mindepth 1 "
            "-not -name lit.site.cfg.py -exec rm -fr {} \;"
        )

class LitTests:
    """
    lit tests
    """

    def configure(self, isDebug=False):
        if isDebug:
            buildType = "Debug"
            optFlag = "-O0"
        else:
            buildType = "Release"
            optFlag = "-O3"

        print(f"Building lit tests in {buildType} mode.")
        Tools().configureCmakeProject(
            srcDir=Config.ROOT_DIR / "lit-tests",
            buildDir=Config.BUILD_DIR / "lit-tests",
            installDir=Config.INSTALL_DIR / "lit-tests",
            envVars=f"LLVM={Config.LLVM}:SDK_ROOT={Config.SDK_ROOT}",
            buildType=buildType,
            addCFLAGS=f"{optFlag}",  # it might be enhanced
            silentConfigure=True,
        )

    @util.print_call
    def run(
        self,
        doTrace=False,
    ):
        os.environ["LIT_OPTS"] = ("-vva " if doTrace else "") + os.environ.get("LIT_OPTS", "")
        return Tools().buildCmakeTarget(Config.BUILD_DIR / "lit-tests", "check-lit", verbose=doTrace)

    def clean(self, doTrace=False):
        if doTrace:
            print(f"removing lit tests")
        shell(
            f"rm -rf {Config.BUILD_DIR / 'lit-tests'} {Config.INSTALL_DIR / 'lit-tests'}"
        )

class Tools:
    def getLLVMTool(self, tool_name):
        tool_name = Path("bin") / tool_name
        if Path(Config.LLVM / tool_name).exists():
            return Config.LLVM / tool_name
        elif Config.SDK_PARTLY:
            return Config.SDK_PARTLY / tool_name
        else:
            print(f"{Path(__file__).stem}: Error: {tool_name} not found!!!")
            exit(1)

    def configureCmakeProject(
        self,
        srcDir,
        buildDir,
        installDir,
        envVars="",
        addCFLAGS="",
        addLDFLAGS="",
        addCMAKEFLAGS="",
        toolchain="",
        buildType="Release",  # verbose=False,
        silentConfigure=True,
    ):
        toolchain_def = f"-DCMAKE_TOOLCHAIN_FILE={toolchain}" if toolchain else ""
        silentOut = Path("/dev") / "null" if silentConfigure else None
        res = shell(
            f"ADD_CFLAGS={addCFLAGS} ADD_LDFLAGS={addLDFLAGS} \
                {envVars} cmake -G Ninja -S {srcDir} -B {buildDir} \
                -DCMAKE_INSTALL_PREFIX={installDir} \
                {addCMAKEFLAGS} {toolchain_def} \
                -DCMAKE_BUILD_TYPE={buildType}",
            out=silentOut,
        )
        if res:
            print("Failed to configure CMAKE project!")
        return res

    def buildCmakeTarget(self, buildDir, buildTarget="", verbose=False, silent=False):
        buildTarget = f"--target {buildTarget}" if buildTarget else ""
        silentOut = Path("/dev") / "null" if silent else None
        cmd = f"cmake --build {buildDir} {buildTarget}" + (" -v" if verbose else "")
        if verbose:
            # print(buildTarget)
            # print(silentOut)
            print(cmd)
        res = shell(cmd, out=silentOut)
        if res:
            print("Failed to build CMAKE project!")

        return res

    def getParents(self, list, sep):
        return set([sep.join(i.split(sep)[:-1]) for i in list])

    def getBaseCommandQEMU(self, localTestPath, isDebug=False):
        options = " -L . -E -LD_LIBRARY_PATH=. "
        cmd = subprocess.getoutput("which qemu-aarch64-static ")
        if "cfi" in localTestPath:
            options += " -E UBSAN_OPTIONS=trap_on_error=0 "
        if isDebug:
            options += f"-g {Config.DEBUG_PORT} "
            print(f"DEBUG INFO: debug port: {Config.DEBUG_PORT}")
        cmd += options + " "

        return cmd

    def tryChdir(self, path, label=""):
        try:
            os.chdir(path)
        except:
            print(f"{label}: chdir failed")
            exit(0)

    def runScript(env, script, qemu, qemu_args, rpt, binary, args):
        start_time = time.time()
        ret = shell(f"pwd ; {env} ./{script} {qemu} {qemu_args} {rpt} {binary} {args}")
        execution_time = time.time() - start_time
        return (ret, execution_time)

    def build_fake_detail(detail, binary, retcode, execution_time):
        timestampstr = (datetime.datetime.now().strftime("%Y-%m-%dT%H:%M:%SZ%Z"),)
        timestr = (f"{execution_time:.4f}s",)
        if retcode == 0:
            (failures, errors) = (0, 0)
        elif retcode < 126:
            (failures, errors) = (1, 0)
        else:
            (failures, errors) = (0, 1)
        data = {
            "tests": 1,
            "failures": failures,
            "errors": errors,
            "timestamp": timestampstr,
            "time": timestr,
            "name": "AllTests",
            "disabled": 0,
            "testsuites": [
                {
                    "name": binary,
                    "tests": 1,
                    "failures": failures,
                    "disabled": 0,
                    "errors": errors,
                    "timestamp": timestampstr,
                    "time": timestr,
                    "testsuite": [
                        {
                            "name": binary,
                            "status": "RUN",
                            "result": "COMPLETED",
                            "timestamp": timestampstr,
                            "time": f"{execution_time:.4f}s",
                            "classname": binary,
                        }
                    ],
                }
            ],
        }
        with open(detail, "wt") as fDetail:
            json.dump(data, fDetail, indent=2)

    @util.print_call
    def runQEMU(
        self,
        tests,
        info={"tag": "", "tests": []},
        isDebug=False,
        profiler=["", ""],
        doLogFile=False,
        doTrace=False,
    ):
        localTestPath, testList = tests
        print(f"localTestPath:{localTestPath}")
        print(f"testList:{testList}")
        testPath = Config.INSTALL_DIR / "qemu" / localTestPath
        if doTrace:
            print(f"test path: {testPath}")
        prevDir = Path.cwd()
        self.tryChdir(testPath, localTestPath)
        currDir = Path.cwd()
        print(info["tag"])
        if doTrace:
            print(f"run QEMU from: {currDir}\n")
        cmd_base = self.getBaseCommandQEMU(localTestPath, isDebug=isDebug)
        testBinaries = util.getTestBinsSet(testList)
        allBinaries = util.getTestBinsSet(info["tests"])
        for test in testList:
            if not testBinaries or (test.test_data.binary in allBinaries):
                if doTrace:
                    print(
                        f"\n= Running subtest for {localTestPath}, command '{test.test_data.binary}'\n"
                    )

                report_options = ""
                if doLogFile:
                    report_options += (
                        f"-E GTEST_OUTPUT=json:{test.test_data.artifact_prefix}.{TestArtifact.REPORT.value}"
                        + " "
                    )
                    report_options += (
                        f"-E TEST_PREMATURE_EXIT_FILE={test.test_data.artifact_prefix}.{TestArtifact.TMP.value}"
                        + " "
                    )
                    open(
                        f"{test.test_data.artifact_prefix}.{TestArtifact.REPORT.value}",
                        "at",
                    ).close()

                testPreconds = ""
                for t in test.test_data.env:
                    testPreconds += t + " "

                testOptions = ""
                for t in test.test_data.args:
                    testOptions += t + " "

                if test.test_data.binary[0:2] == ". ":
                    # We ignore profiling for scripts
                    (retcode, execution_time) = Tools.runScript(
                        testPreconds,
                        test.test_data.binary[2:] + ".sh",
                        cmd_base,
                        test.test_data.qemu_args + " ",
                        report_options,
                        test.test_data.binary[2:],
                        testOptions,
                    )
                    detail = (
                        f"{test.test_data.artifact_prefix}.{TestArtifact.REPORT.value}"
                    )
                    try:
                        if 0 == os.stat(detail).st_size:
                            Tools.build_fake_detail(
                                detail,
                                test.test_data.binary[2:] + ".sh",
                                retcode,
                                execution_time,
                            )
                    except FileNotFoundError:
                        pass
                    continue
                else:
                    cmd = (
                        profiler[0]
                        + testPreconds
                        + cmd_base
                        + test.test_data.qemu_args
                        + " "
                        + report_options
                        + test.test_data.binary
                        + " "
                        + testOptions
                        + profiler[1]
                    )
                    if doTrace:
                        print(cmd)
                    else:
                        print(f"binary: {test.test_data.binary}")
                    if not doLogFile:
                        shell(cmd)
                        continue
                    start_time = time.time()
                    log = str()
                    stdout = str()
                    retcode = 0
                    log = f"RUN: '{cmd}'\n"
                    try:
                        with open(
                            f"{test.test_data.artifact_prefix}.{TestArtifact.ERR.value}",
                            "at",
                        ) as stdErrFile:
                            stdout = subprocess.check_output(
                                cmd, shell=True, stderr=stdErrFile
                            ).decode("utf-8", errors='backslashreplace')
                    except subprocess.CalledProcessError as e:
                        print(e.output.decode("utf-8"))
                        retcode = e.returncode
                    execution_time = time.time() - start_time

                    detail = (
                        f"{test.test_data.artifact_prefix}.{TestArtifact.REPORT.value}"
                    )
                    if 0 == os.stat(detail).st_size:
                        Tools.build_fake_detail(
                            detail, test.test_data.binary, retcode, execution_time
                        )
                    status = (
                        f"OK: exitcode {retcode}"
                        if retcode == 0
                        else f"ERROR: exitcode {retcode}"
                    )
                    execution_time_msg = f"{status}. The execution time is {execution_time:.4f} seconds.\n"
                    log = f"{log}\n{stdout}\n{execution_time_msg}\n"
                    print(log)
                    with open(
                        f"{test.test_data.artifact_prefix}.{TestArtifact.LOG.value}",
                        "at",
                    ) as testLogFile:
                        testLogFile.write(log)
            else:
                if doTrace:
                    print(
                        f"\n= Skipping subtest for {localTestPath}, command '{test.test_data.binary}'\n"
                    )
        os.chdir(prevDir)  # go back
