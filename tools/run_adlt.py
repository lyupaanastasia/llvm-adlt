import os
import argparse

from tools import shell, create_symlink
from config import Config


def parseArgs():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--inputLibs", "-i", nargs='*', help="input libs", required=True, default=None)
    parser.add_argument(
        "--outputDir", "-od", help="output directory path", required=True, default=None)
    parser.add_argument(
        "--ldFlags", "-f", help="ld flags", required=False, default=None)
    parser.add_argument(
        "--output", "-o", help="output file name", required=False, default="libadlt.so")
    parser.add_argument(
        "--debug", help="debug", action='store_true',  default=False)
    parser.add_argument(
        "--trace", help="trace", action='store_true',  default=False)
    return parser.parse_args()


def main():
    args = parseArgs()
    JOIN = os.path.join
    args.trace = True if os.environ.get('ADLT_TRACE') else False
    if args.ldFlags == None:
        args.ldFlags = os.environ.get('LD_FLAGS', '')
        if len(args.ldFlags) == 0:
            args.ldFlags = "--adlt"

    if len(args.inputLibs) == 0:
        exit(0)
    libs = ' '.join(args.inputLibs)

    if (args.debug):
        print(args.ldFlags)
        print(args.outputDir)
    os.makedirs(args.outputDir, exist_ok=True)

    ld = os.path.join(Config.LLVM, "bin", "ld.lld")
    outLib = args.output
    cmd = f"{ld} {args.ldFlags} {libs} -o {outLib}"
    if (args.trace):
        cmd += " --adlt-trace"
        print(f"ADLT COMMAND: {cmd}")
    shell(cmd)

    libs = args.inputLibs
    libs.append(outLib)
    for lib in libs:
        _, libname = os.path.split(lib)
        create_symlink(
            os.path.abspath(outLib),
            JOIN(os.getcwd(), args.outputDir, libname),
            args.debug
        )


if __name__ == "__main__":
    main()
