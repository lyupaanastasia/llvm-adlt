### Requirements
```bash
# Ubuntu (18.04 or 20.04) or Debian
sudo apt install -y git git-lfs curl wget binutils-dev ccache \
    python3 python-is-python3 python3-distutils python3-pip \
    libncurses5 libxml2 doxygen libomp5 gdb-multiarch
# Debian 12 specific
sudo apt install -y libstdc++-12-dev libpcre3
wget http://ftp.debian.org/debian/pool/main/s/swig/swig_3.0.12-2_amd64.deb
wget http://ftp.debian.org/debian/pool/main/s/swig/swig3.0_3.0.12-2_amd64.deb
sudo dpkg -i swig3.0_3.0.12-2_amd64.deb swig_3.0.12-2_amd64.deb
rm -rf ./swig*
```

### Get SDK parts
```bash
cd $HOME
# append to ~/.bashrc
export SDK_ROOT="$HOME/work/sdk"
export LLVM="$SDK_ROOT/out/llvm-install"
export CMAKE="$SDK_ROOT/prebuilts/cmake/linux-x86/bin"
export PATH="$CMAKE:$PATH" # optional: add $LLVM/bin first

# restart current shell
source ~/.bashrc

mkdir -p $SDK_ROOT && cd $SDK_ROOT
# get latest qemu
wget https://github.com/multiarch/qemu-user-static/releases/download/v7.2.0-1/qemu-aarch64-static.tar.gz && \
 tar -zxf qemu*.tar.gz && rm qemu*.tar.gz

# setup: git config --global (user.email, user.name)

# get repo-tool
curl https://storage.googleapis.com/git-repo-downloads/repo > repo && chmod a+rx ./repo

# clone SDK repos
./repo init -u https://gitee.com/urandon/manifest.git -b adlt_dev -m llvm-toolchain.xml --depth=1
./repo sync -c --retry-fetches=30 && ./repo forall -c 'git lfs pull'

# prepare
./toolchain/llvm-project/llvm-build/env_prepare.sh

# build debug PARTLY ver for aarch64
python3 ./toolchain/llvm-project/llvm-build/build.py --build-gtest-libs --enable-assertions \
	--debug --adlt-debug-build \
	--no-build-x86_64 --no-build-mipsel --no-build-arm --no-build-riscv64 --no-build-loongarch64 \
	--build-only lld,llvm-objdump,llvm-readelf,musl,compiler-rt,libcxx &> llvm.log

# alternative way: the complete toolchain (release)
python3 ./toolchain/llvm-project/llvm-build/build.py --build-gtest-libs --enable-assertions \
    --no-build-x86_64 --no-build-mipsel --no-build-arm --no-build-riscv64 --no-build-loongarch64 \
    --no-build windows,lldb-server,check-api &> llvm.log
```

### VSCode configuration
To format Python code we use the `black` tool, which can be installed via pip. \
`black` is used without arguments, all settings are default.

1. Core settings:
    ```bash
    "git.alwaysSignOff": true,
    "diffEditor.ignoreTrimWhitespace": false,

    "editor.renderFinalNewline": "on",
    "editor.trimAutoWhitespace": true,
    "editor.renderWhitespace": "trailing",

    "files.trimTrailingWhitespace": true,
    "files.insertFinalNewline": true,
    "files.trimFinalNewlines": true,

    "C_Cpp.clang_format_fallbackStyle": "LLVM",
    "C_Cpp.clang_format_style": "LLVM",
    "[python]": {
        "editor.defaultFormatter": "ms-python.black-formatter",
    }
    ```
