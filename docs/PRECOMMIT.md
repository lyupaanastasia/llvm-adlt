# Setup formatting with pre-commit hook

_In the following descruption, I assume, that the repo is in `llvm-adlt/`_

We use
- clang-format for formatting C and C+++ code. The `.clang-format`
is in `llvm-adlt/` (versioned) and based on LLVM style (but expands tabs)
- `pre-commit` repo with `black` backend for formatting python code. The configuration is in `llvm-adlt/configs/.pre-commit-config.yaml` (versioned)
- The sample pre-commit hooks are in `llvm-adlt/githooks/`. You can either copy required to `llvm-adlt/.git/hooks/pre-commit`,
or configure git to use `llvm-adlt/githooks/` for hooks. The 1st way is recommended.

## Install clang-format

```bash
$ # That installs both clang-format and git-clang-format
$ sudo apt install clang-format`
```

_If you use docker, then you need to add the above package to container, otherwise you'll need that step on every `docker run`_

## Setup clang-format (optional, not recommended)

It is not recommended to use only `clang-format` because you'll lose ability to format python code.
If you still want to use only `clang format`, then just copy its hook to .git/hooks
```bash
$ cd llvm-adlt
$ cp githooks/pre-commit.clang-format_only .git/hooks/pre-commit
```

## Install pre-commit repo and black

Install python packages
```bash
$ pip install black pre-commit
```

Find where pre-commit is (on my system, it went to ~/.local/bin) and then call `pre-commit install`. That will use the `llvm-adlt/pre-commit-config.yaml` (versioned).


```bash
$ find ~ -name pre-commit
/home/user/.local/bin/pre-commit
$ cd llvm-adlt/configs
$ pre-commit install
$ git commit -m "add pre-commit configuration"
```

The `pre-commit install`, among other things, will write `pre-commit` to `.git/hooks`.

## Install hook which uses both clang-format and black

Save the pre-commit to pre-commit.black, then copy the corresponding sample from `llvm-adlt/githooks` to `.git/hooks`
```bash
$ cd llvm-adlt
$ (cd .git/hooks && mv pre-commit pre-commit.black)
$ cp githooks/pre-commit.clang-format_plus_black .git/hooks/pre-commit
```

## Additional info about pre-commit

I used the following
[pre-commit+black](https://dev.to/emmo00/how-to-setup-black-and-pre-commit-in-python-for-auto-text-formatting-on-commit-4kka)
link when tuning formatter.
