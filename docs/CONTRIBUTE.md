## How to work with projects and publish changes

### Setup your editor first! [Link for VSCode](./SETUP_SDK.md#vscode-configuration)

### llvm-adlt (this project):
1. #### [Setup formatting with pre-commit hook](./PRECOMMIT.md)
1. #### Tune git settings for the repo
```bash
# do these 2 lines once
git remote rename origin upstream
git remote add origin git@gitee.com:<YOUR USERNAME ON GITEE>/llvm-adlt.git
# make sure all is ok
git remote -v

# create new branch
git fetch upstream master
git checkout -b <NEW BRANCH> upstream/master

# publish your changes
git fetch upstream master
git rebase upstream/master
git push origin <NEW BRANCH>
```

Optional tips (be careful):
```bash
# force delete your branch and take it from upstream
git branch -D <YOUR BRANCH>
git checkout -t upstream/<YOUR BRANCH>
git push -f origin <YOUR BRANCH>

# delete your branch from origin remotely and locally
git push origin –d <YOUR BRANCH>
git branch -D <YOUR BRANCH>
```

### llvm-project:
```bash
cd $SDK_ROOT/toolchain/llvm-project
# do these 2 lines once
git remote rename kosovpavel upstream
git remote add origin git@gitee.com:<YOUR USERNAME ON GITEE>/third_party_llvm-project.git

# llvm-project
git fetch upstream adlt_dev
git checkout -b adlt_<NEW BRANCH> upstream/adlt_dev

# publish your changes
git fetch upstream adlt_dev
git rebase upstream/adlt_dev
git push origin adlt_<NEW BRANCH>
```

## musl:
```bash
cd $SDK_ROOT/third_party/musl
git remote rename kosovpavel upstream
git remote add origin git@gitee.com:<YOUR USERNAME ON GITEE>/third_party_musl.git
# other lines are similar to the llvm-project contributing...
```
