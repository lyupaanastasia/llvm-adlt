### Start debugging examples
```bash
# Print adlt command
ADLT_TRACE=1 ./main.py -cb ...
# add the option --adlt-trace for printed lld-command

# You may re-build and debug specific test (start gdb server):
./main.py -cbd cfi/call-cast -r cfi/call-cast/adlt
# without re-buld (start gdb server):
./main.py -dr cfi/call-cast/adlt
# then connect from vscode
# (check .vscode/launch.json or docs/vscode-this/launch.json)

# FAST WAY:
# extended debugging over GDB (requires gdb-multiarch and psmisc):
./main.py -cbd tls/emu && ./gdb.py -r tls/emu/orig
./gdb.py -r tls/emu/adlt
# you will start gdb-server in QEMU, automatically connect
# and kill all pids at exit (type exit or CTRL+d)

# load gdb script to automate debugging
./gdb.py -s debug.gdb -r base/cross/orig

# add breakpoint on main() and continue
./gdb.py -r -m base/cross/orig

# rebuild lld, failed test and check results in gdb:
./main.py -cbd lld,tls/emu && ./gdb.py -r tls/emu/adlt

# run all tests from base/ folder.
./main.py -cb base -r base

# run all tests from base/ folder with --no-relax for output ADLT libs.
ADLT_NO_RELAX=1 ./main.py -cb base -r base

# run asan lit-tests from compiler-rt:
python3 tools/run_lit_sanitizers.py --test asan --llvm-project $HOME/work/sdk/toolchain/llvm-project/

# run ubsan lit-tests from compiler-rt:
python3 tools/run_lit_sanitizers.py --test ubsan --llvm-project $HOME/work/sdk/toolchain/llvm-project/
```

### Debugging notes
Helpful references about ELF:
- `$SDK_ROOT/toolchain/llvm-project/llvm/include/llvm/Object/ELFTypes.h`
- `$SDK_ROOT/third_party/musl/porting/linux/user/include/elf.h`

Debugging Toolchain using Visual Studio Code:
```bash
# go to the root of this repo: $ADLT_DEMO
cd $ADLT_DEMO

# do sym-links:
# debug this llvm-adlt repo:
ln -s $PWD/docs/vscode-this/launch.json $PWD/.vscode/launch.json

# debug LLD:
mkdir -p $SDK_ROOT/toolchain/llvm-project/lld/.vscode $PWD/.vscode
ln -s $PWD/docs/vscode-llvm/lld/launch.json $SDK_ROOT/toolchain/llvm-project/lld/.vscode/launch.json

# debug other LLVM components: E.g. llvm-strip:
mkdir -p $SDK_ROOT/toolchain/llvm-project/.vscode $PWD/.vscode
ln -s $PWD/docs/vscode-llvm/launch.json $SDK_ROOT/toolchain/llvm-project/.vscode/launch.json
```


Strip debug information for the input libraries:
```bash
find $SDK_ROOT -name *llvm-strip
ls lib* | xargs $SDK_ROOT/prebuilts/clang/ohos/linux-x86_64/clang-15.0.4/bin/llvm-strip -d
```

Base scenario in GDB:
```bash
# base scenario in GDB
b main
c       # continue

i f     # info current frame
bt      # print backtrace
i args  # print args (in function)
i loc   # print locals
x/i $pc # disasm current line
# find this line in generated dumps
# and check other regs

disas FUNC_NAME
# check the same func in original dump
# and check relocs

b *0x0000005502990e5c # bkpt at spec addr
b 2459  # bkpt at spec line
b dynlink.c:2460    # bkpt at spec file+line
c   # continue

# display variables (at each time GDB stops at a bkpt or after a step)
disp reloc_addr
disp relr[0]
disp # print all variables to be displayed
undisp 1 # undisplay variable at number in list

i b # breakpoints info
del 1   # delete breakpoint by number

# print current source code if possible:
l # or list
# type enter to print more
# or print specific source code
list obj/third_party/musl/intermidiates/linux/musl_src_ported/src/thread/pthread_create.c:314

q # quit
```

Extract dump for func in `adlt` and `orig`. E.g.:
```bash
# Note: the result of these commands will be in ./debug_logs

# find and compare "ldr     x0" instruction part in 'threadFunc' of specified lib:
./cmp.py -r "ldr\sx0" -func threadFunc -t tls/emu -fp lib/libemuTLS_aC.so
# find and compare specific symbol
./cmp.py -sym tData -t tls/emu -fp lib/libemuTLS_aC.so
# compare dumps in cmd or use VSCode
# debug received offsets in LLD


# check dynamic symbols:
./cmp.py -dyn -syms -t tls/emu -fp lib/libemuTLS_aC.so

# check all: disasm, relocs, headers, symbols etc...
./cmp.py -a -t tls/emu -fp lib/libemuTLS_aC.so
# extended mode:
./cmp.py -x -a -t tls/emu -fp lib/libemuTLS_aC.so

# check symbols:
./cmp.py -syms -t tls/emu -fp lib/libemuTLS_aC.so
# find by name in dynamic symbol table
./cmp.py -r "__emutls_v.TLS_data1" -dyn -syms -t tls/emu -fp lib/libemuTLS_aC.so
# find by index (LLD: check symIndex in RelocationScanner::processForADLT)
./cmp.py -x -r "116" -syms -t tls/emu -fp lib/libemuTLS_aC.so
# find by offset (LLD: check r->offset in RelocationScanner::processForADLT)
./cmp.py -x -r "1d64" -syms -t tls/emu -fp lib/libemuTLS_aC.so


# check section contents
./cmp.py -sec .rela.dyn -t tls/emu -fp lib/libemuTLS_aC.so
# check all section contents
./cmp.py -full -t tls/emu -fp lib/libemuTLS_aC.so

# check all disassembly
./cmp.py -fullDis -t tls/emu -fp lib/libemuTLS_aC.so

# check relocs
./cmp.py -rels -t tls/emu -fp lib/libemuTLS_aC.so
# check dynamic relocs (extended mode)
./cmp.py -x -dyn -rels -t tls/emu -fp lib/libemuTLS_aC.so


# check dynamic table
./cmp.py -dt -t tls/emu -fp lib/libemuTLS_aC.so

# check section headers
./cmp.py -sh -t tls/emu -fp lib/libemuTLS_aC.so

# check program headers
./cmp.py -ph -t tls/emu -fp lib/libemuTLS_aC.so
```


# Use `gdbserver.sh` to launch VSCODE debugging session

1. Prepare project in vscode (remote ssh, folders must include at least llvm-adlt)
2. Build tests `main.py -cbd all`
3. Launch vscode
4. launch gdbserver.sh with test binary (relative to out/qemu) and with args if necessary. For example
```
./gdbserver.sh ld-musl/dlopen/adlt/test_dlopen libfoo.so
```

5. Press \<F5\> in vscode

Explanation:

The gdbserver.sh builds the file .vscode/launch.json, inserting the binary with args into it and after that launches the gdbserver. For example, after the above call of gdbserver.sh, the .vscode/launch.json will contain the following line
```json
...
    "request": "launch",
    "program": "${workspaceFolder}/out/qemu/ld-musl/dlopen/adlt/dlopen_test_dlopen"
    "cwd": "${workspaceFolder}/out/qemu/ld-musl/dlopen/adlt"
    "miDebuggerServerAddress": ":8081",
...
```
and then the gdvserver (bult in qemu-static) will be launched, note argument at the end of line
```bash
qemu-aarch64-static -g 8081 -L . -E -LD_LIBRARY_PATH=. dlopen_test_dlopen libfoo.so
```
