# Docker as a build environment for adlt

## Steps to prepare and tune docker image

### 0 (prerequisite)

Install libvirt and docker if not yet done.

### 1. Generate docker image and docker-compose.yml

Inspect the first lines of build.sh script and change settings for variables if necessary. Most likely,
you'll want to change image name and the place where persistent volumes will be kept.   
You can change things like user, group, etc as well, but by default they will be taken from current user.

After that, run the build.sh (be patient, that step will take some time).
For example
```
chmod +x ./build.sh ; ./build.sh
$  ./build.sh
...
[+] Building 0.4s (15/15) FINISHED
...
 => => writing image ...
 => => naming to docker.io/library/adlt-builder 

FYI: To run, try ( cd /path_to_volumes/adlt-builder ; docker compose run -it --rm adlt-builder )
You can also add to your .bashrc the function
adlt-builder ()
{
    pushd /path_to_volumes/adlt-builder >/dev/null
    docker compose run -it --rm adlt-builder
    popd >/dev/null
}

$
```
As a result, you will get a docker image, the docker-compose.yml in
the current directory and the hint how to run it.


### 2. Persistent volumes

There are two persistent volumes in container:

- /home/${user} (mapped to ${volumes}/home/${user}) - that for history, startup scripts, settings, etc.
- /import/work - for repos and heavy things you want to make persistent


### 3. Run container for the first time, configure git, initialise repos

It is necessary to configure git at first run of container, otherwise the `repo init` will fail.
For example, 

```
$ ( cd /home/user/00.Docker/compose/adlt-builder ; docker compose run -it --rm adlt-builder )

user@adlt-builder:/import/work$ git config --global user.name 'Soname Name'
user@adlt-builder:/import/work$ git config --global user.email 'your.email@...'
```

After that step, you can initialize and download repositories and proceed with SDK setup and llvm-adlt build

_Note that some settings slightly differ from [sdk setup](./SETUP_SDK.md):_

- No need to install additional packages with `apt instal`
- put `export SDK_ROOT=/import/work/sdk`  into .bashrc instead of `export SDK_ROOT=${HOME}/work/sdk`
- No need to get repo tool - it is already in container


### 4. If you need root access to container files

Just do `sudo command` or `sudo su` in container.


