Docs and tests of ADLT feature for LLVM.

### General use case
Just add `--adlt` option to `ld.lld` call when build shared library.
```bash
# 1) input libs should be linked with -Wl,--emit-relocs,--no-relax
# 2) '--shared' will be applied automatically
$LLVM/bin/ld.lld --adlt -o libadlt.so liba.so libb.so
# Notes:
# 1) It is possible to use --emit-relocs to get the reloc info in the resulting library
# 2) The resulting library can be checked by existing tools as plain lib
#   examples:
$LLVM/bin/llvm-objdump -xsSdRr out/adlt.so &> adlt.so.asm
$LLVM/bin/llvm-readelf -a out/adlt.so &> readelf_adlt.so.asm
$LLVM/bin/llvm-readelf -r out/adlt.so &> readelf_relocs_adlt.so.asm
# print the content of .adlt section
$LLVM/bin/llvm-readelf --adlt-section --elf-output-style=LLVM out/adlt.so
# or
$LLVM/bin/llvm-readobj --adlt-section out/adlt.so
```


1. #### Setup SDK and Editor first
    `Read this`: [./docs/SETUP_SDK.md](./docs/SETUP_SDK.md).\
    `Alternative way`: Consider using docker container instead of local machine
        for building sdk and llvm-adlt.\
        Docker file and build  script is in `./docker/`,
        description is in [./docs/SETUP_DOCKER.md](./docs/SETUP_DOCKER.md).

1. Download this repo:
    ```bash
    ADLT_DEMO="$HOME/work/llvm-adlt"
    # optional: save this variable
    echo "export ADLT_DEMO=\"$ADLT_DEMO\"" >> ~/.bashrc
    source ~/.bashrc # restart shell

    git clone https://gitee.com/lyupaanastasia/llvm-adlt \
        --depth=1 $ADLT_DEMO && cd $ADLT_DEMO

    # Install requirements.
    pip3 install -r requirements.txt
    ```

1. If `SDK_PARTLY`: Set up variable `. rc.sdk-partly.sh /path/to/clang`, example:
    ```bash
    # do it once:
    echo 'export SDK_PARTLY=$SDK_ROOT/prebuilts/clang/ohos/linux-x86_64/clang-15.0.4' >> ~/.bashrc
    source ~/.bashrc # refresh current session
    # or do this line in every new terminal session
    . rc.sdk-partly.sh $SDK_ROOT/prebuilts/clang/ohos/linux-x86_64/clang-15.0.4
    ```
1. Run [lit tests](https://llvm.org/docs/CommandGuide/lit.html):
    CMakeLists.txt target in ./lit-tests creates two lit site configurations, orig and adlt, for running tests, and places them into the build directory.\
    In the future can be extended to include debug/release configurations as well. Example:
    ```bash
    cmake --build $SDK_ROOT/out/llvm_make --target install-FileCheck # do it once
    cmake -G Ninja -S lit-tests -B out/build/lit-tests # do it once or `python3 ./main.py -cb lit-tests`
    llvm-lit -v out/build/lit-tests/orig out/build/lit-tests/adlt             # all
    llvm-lit -v out/build/lit-tests/*                                         # all with warnings
    llvm-lit -v out/build/lit-tests/orig                                      # specific config
    llvm-lit -v out/build/lit-tests/adlt/cfi/cross-dso/dtor/cfi_dtor_test.cpp # specific test
    LIT_FILTER="cfi" llvm-lit -v out/build/lit-tests/adlt                     # filter tests by name
    llvm-lit --filter="cfi" -v out/build/lit-tests/adlt                       # filter tests by name
    LIT_FILTER="adlt.*cfi" llvm-lit -v out/build/lit-tests/*                  # filter tests and config by name
    LIT_FILTER="adlt :: cfi/cross-dso/dtor/cfi_dtor_test.cpp" llvm-lit -v out/build/lit-tests/* # filter by full config and test name
    LIT_FILTER="cfi" LIT_OPTS="-v" ninja -C out/build/lit-tests check-lit               # ninja target
    LIT_FILTER="cfi" LIT_OPTS="-v" cmake --build out/build/lit-tests --target check-lit # ninja target

    # Debug lit tests runtime. Run llvm-lit <test name> first, then:
    cd out/build/lit-tests/<config>/gwpasan/Output/double-free.c.tmp
    # Run without debugger:
    qemu-aarch64-static -L run ./a.out
    # Run with debugger with sysroot at ./run and gdb script:
    bash -c "qemu-aarch64-static -g 8081 -L run ./a.out &" &&
        gdb-multiarch ./a.out -ex="target remote localhost:8081" -ex="set sysroot run" -ex="set substitute-path ./out/lib/clangrt-aarch64-linux-ohos/toolchain $SDK_ROOT/toolchain" -x double-free.script
    ```
1. Build demo and run all tests:
    ```bash
    ./main.py -cb all -r all
    # Add the option '-d' to recompile tests in Debug-mode or debug the test.
    # If you want to debug musl, please make sure that you build sdk with the "--adlt-debug-build" flag.
    # Add the option '--withOrigMusl' to run tests with original musl. (needs builded original musl)

    # Other use cases:
    # Build specific tests. E.g. clean and build some base and cfi tests:
    ./main.py -cb base/cross,cfi/call-cast
    # The results will be located in ./out/qemu directory.

    # Run specific tests (and subtests):
    ./main.py -r base/eh_frame
    ./main.py -r base/eh_frame/adlt
    ./main.py -r base/eh_frame:eh_frame_main123

    # Build group tests:
    ./main.py -cb tls
    # or multiple groups
    ./main.py -cb tls,cfi

    # Rebuild lld, musl (with adlt) and some tests:
    ./main.py -cb lld,musl,cfi/call-cast
    # Note: files for LLD and musl will not be removed.

    # Build orig-musl:
    ./main.py -b orig-musl
    # Note: orig-musl is needed for running with '--withOrigMusl' option.

    # Add STRIPPED env variable to build and use stripped ADLT libraries:
    STRIPPED=1 ./main.py -cb lld,base/cross -r base/cross/adlt

    # Add RELR env variable to build tests with different settings for `--pack-dyn-relocs` option:
    #
    # for Android Packing + RELR:
    RELR=1 ./main.py -cb lld,base/cross -r base/cross/adlt
    # or
    RELR=android+relr ./main.py -cb lld,base/cross -r base/cross/adlt
    #
    # for RELR:
    RELR=2 ./main.py -cb lld,base/cross -r base/cross/adlt
    # or
    RELR=relr ./main.py -cb lld,base/cross -r base/cross/adlt
    #
    # for Android Packing:
    RELR=3 ./main.py -cb lld,base/cross -r base/cross/adlt
    # or
    RELR=android ./main.py -cb lld,base/cross -r base/cross/adlt

    # Optional: Use LD_FLAGS env variable to pass user options to the linker:
    LD_FLAGS="--adlt"

    # Run multiple tests. E.g., compare base/cross tests:
    ./main.py -r base/cross/orig,base/cross/adlt
    # add "--trace" to print more detailed info
    # add "--qemuLogFile" to put runtime results in log files.

    # Run only tests marked as successful:
    ./main.py -r only-ok
    # Run only tests marked as unsuccessful:
    ./main.py -r only-fail
    # (tests states are marked in TEST_LIST in tools/config.py)

    # Run only specific subtest(s) with the specified command line parameters (optional).
    # E.g., for the test ld-musl/so_mem_share/orig run
    # subtests 'so_mem_share_hello', 'so_mem_share_chain_a 0', 'so_mem_share_chain_a 2' and
    # for the test ld-musl/so_mem_share/adlt run all subtests:
    ./main.py -r ld-musl/so_mem_share/orig:so_mem_share_hello:'so_mem_share_chain_a 0':'so_mem_share_chain_a 2',ld-musl/so_mem_share/adlt
    # Note: use single quotes if subtest string contains space(s)
    # or enclose the entire run option value in quotes:
    ./main.py -r 'ld-musl/so_mem_share/orig:so_mem_share_hello:so_mem_share_chain_a 0:so_mem_share_chain_a 2,ld-musl/so_mem_share/adlt'

    # check other features by using '-h' option
    ```
1. If you want to run benchmarks, install `perf` and setup passwordless sudo access. Note, that without `perf` profiling mode will fall back to `/usr/bin/time`.
   ```bash
   $ # for passwordless sudo
   $ sudo sed -i -re 's/(^\%sudo.*)ALL/\1NOPASSWD: ALL/' /etc/sudoers
   $ sudo apt-get update
   $ # for perf
   $ apt-get install linux-tools-common -y
   $ apt-get install linux-tools-generic -y
   $ apt-get install linux-tools-$(uname -r) -y
   ```
    __Important note__: Sometimes after applying updates to the host system, you will get the message like
    ```
    WARNING: perf not found for kernel 6.8.0-47
    ```
    That happens because the kernel signature changed. To solve that on host, just add missing package
    ```bash
    sudo apt update && sudo apt install linux-tools-$(uname -r)
    ```
    For docker, you have two options
    - (better one)  - rebuild problem docker image
    - Work around, will update container if necessary on every `docker run`.
    For that, add a line to a `~/.bashrc` of _docker container_ (assumed that your `${HOME}` in container is persistent):
    ```bash
    perf --version >& /dev/null || ( sudo apt update && sudo  apt install linux-tools-$(uname -r) -y )
    ```
1. #### [Setup formatting with pre-commit hook](./docs/PRECOMMIT.md)
1. #### [Learn how to work with projects and publish changes](./docs/CONTRIBUTE.md)
1. #### [Learn debugging notes and examples](./docs/DEBUGGING.md)

### Using prebuilt toolchain
It is now possible to use prebuilt toolchain for building and running tests

To do so
1. Obtain the toolchain build with gtest libraries included (that is, built  with `--build-gtest-libs` option).
See [Setup SDK](./docs/SETUP_SDK.md).
1. Set up variables `. rc.sdk-prebuilt.sh /path/to/llvm-build-id`
1. Build and run tests `./main.py -cb all -r all`


### General rules
1. Code style format: `LLVM`. [Link to configure VSCode](./SETUP_SDK.md#vscode-configuration).
1. Merges are not allowed. Do rebases instead.

### Additional notes
1. If '`-b all`' is used, it will rebuild LLD automatically as well.
