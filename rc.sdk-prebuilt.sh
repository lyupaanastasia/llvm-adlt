#!/bin/bash

# Must be sourced
if ! ( return 0  >& /dev/null ) ; then
    echo "The script expected to be sourced"
    return 1
fi

if [[ x"$1" == x ]]; then
    echo "Usage: . ${BASH_SOURCE[0]} path_to_sdk_prebuilt" 2>& 1
    return 1
fi
export SDK_PREBUILT="$1"
export BUILD=$(basename $1)

# settings for prebuilt SDK
export SDK_ROOT=${SDK_PREBUILT}
export LLVM="${SDK_ROOT}/clang-${BUILD}"
export CMAKE_BUILD_WITH_INSTALL_RPATH=1
